
# DoAna - Website <img src='assets/images/logo-carre.png' align="right" height="100" />

<!-- badges: start -->
<!-- badges: end -->

Source code of [doana-r.com](https://doana-r.com) website for **DoAna - Statistiques Réunion** small business.


## Thanks

- [Frédéric Chiroleu](https://www.linkedin.com/in/fr%C3%A9d%C3%A9ric-chiroleu-1073aa56/) who put me in the R cauldron and gave me the initial business idea
- [Laura Hedon](https://www.laurahedon.com) for her fantastic workart
- [Marion Hallet](https://www.marionhallet.com/) who accepted to trade some portraits
- [Tom Doizy](https://www.youtube.com/watch?v=pOahn4M9eAY) for the 3D pictures and the animation
- the whole [R](https://www.r-project.org) and [RStudio](https://rstudio.com) community, and especially [Yihui Xie](https://yihui.org/), for all the wonderful tools they provide and their responsiveness 


Proofreaders:
- [my parents](http://www.doizymaquettiste.com)
- [Franky Chatenet](https://www.fc-creation.fr/)
- Hugo Yeung Shi Chung
- Christel Michel
- Ismael Houillon
- Marc Teytaud
- Cédric Durieux



## Non-exhaustive todo list

### Settings

#### Visibility (SEO)
- [x] add description field
- [ ] present key words (where? will a word cloud be useful?)
- [x] subscribe google search console
- [x] make redirection from wixsite
- [x] make redirection from doana-r.gitlab.io to doana-r.com
- [x] make redirection from doana-r.gitlab.io/initiation-r to initiation.doana-r.com
- [ ] home page redirections (doana-r.gitlab.io, doana-r.gitlab.io/, doana-r.gitlab.io/#head)
- [ ] check meta tags
- [x] add robots.txt
- [ ] add 404 error HTTP status code

#### Spying
- [x] add Plausible

#### Translation
- [ ] make it English-friendly

#### Consistency
- [ ] pass toml headings in yaml
- [ ] make variables for scss colors
- [ ] use `inherit` for colors when possible
- [ ] manage tags and categories
- [ ] remove useless author pages
- [ ] enable git info (?)

#### Sharing
- [ ] add page_sharer?
- [ ] add RSS button

#### Prettiness
- [ ] justify paragraphs with `hyphens: auto`
- [ ] customise 404 (or not)
- [ ] modify weird up-transitions when going to an anchor (#)

#### Quality
- [x] check website speed and quality 
- [ ] add alt properties for images https://sourcethemes.com/academic/docs/managing-content/#featured-image
- [ ] compress components with gzip
- [ ] add expires headers



### Home

#### Menu
- [x] right-align 
- [x] bigger font

#### Head
- [x] make doanalyzinR smaller and/or increase quality
- [x] reduce artwork to ~ 500ko
- [x] improve parallax effect for every screen sizes
- [x] ask Laura to modify image ratio

#### Services
- [ ] add text in formation part to make buttons aligned
- [ ] customise icons
- [x] add call to action buttons
- [x] define R somewhere (Marc)

#### Programmation
- [x] modify colors in the graph
- [x] add a sentence like "Amusez-vous à trier ce que vous cherchez !" (Cédric)

#### Formation
- [x] add section (list possible training programs)
- [x] CPF formanoo available

#### Contact
- [ ] customise icons
- [x] customise map (stamen style, Reunion centered, Parc à Mouton marker)

#### Footer
- [x] link to gitlab repo
- [x] add credits
- [x] add licence
- [ ] sticky back-to-top button



### Publications

#### css
- [ ] offset Type/Publication (col-md-1 class)
- [x] remove author-card (profile: false)

#### index
- [ ] redirection as for project page?
- [ ] remove search bar
- [ ] customise icons
- [x] update : chouchou + tree-grass


### About
- [x] link to CV
- [x] open CV in the browser, without the need of downloading it -> depends on the browser...
- [x] fix grey author names
- [x] modify profile picture, and the matching credit
- [ ] customise icons
- [x] add ReuSit youtube interview
- [ ] make a better video

### Projects

#### Main page
- [x] add relevant subtitle
- [x] choose a view between cards or showcase
- [x] customise card view and toolbar css


#### Project pages
- [ ] manage slugs (especially langage inconsistency between projects and projet)
- [x] rewrite initation-r index inside the website
- [ ] thank microtubule person
- [ ] link to violin microtubule plot and to https://www.data-to-viz.com/caveat/error_bar.html in barres-erreur


#### Projects ideas

- tree-grass (P)
- IPSIM-chayote (P)
- BIOPIPER (C)
- terrro (P)
- cyber-invasives presentation (P)
- coreTEP (C)
- Reunion soil mapping (C)
- bulletin avortement bovins 2019 (C)
- enquete miellées GDS (C)
- bonnes pratiques pour faire des graphiques (F)


Legend : (P) programming, (C) consulting, (F) training.



## Local updates : workflow
- periodically remove `resources` folders
- add packages dependencies in `.gitlab-ci.yml` (not anymore)
- beware of blogdown, hugo and academic updates (breaking changes)
- serve the site locally before pushing



