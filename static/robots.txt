User-agent: *
Disallow: /tag/
Disallow: /tags/
Disallow: /author/
Disallow: /authors/
Disallow: /publication/
Disallow: /publication-type/
Disallow: /publication-types/
Disallow: /categories/
Disallow: /projets/index.html


Sitemap: https://doana-r.gitlab.io/sitemap.xml
