---
title: "Un OAD pour gérer le chardon des champs"
authors:
- Marie-Hélène Robin
- Octave Lacroix
- Jean-Noël Aubertot 
- anna-doizy
date: "2022-02-01"
profile: false

# Schedule page publish date (NOT publication's date).
publishDate: ""

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: In *Phytoma* (revue française spécialisée en protection des plantes)
publication_short: In *Phytoma*

abstract: "Dans un contexte de retrait du glyphosate et de réduction du recours au labour, des outils sont nécessaires pour aider à la transition vers des solutions alternatives au contrôle chimique des adventices vivaces. Celles-ci constituent un problème majeur dans les parcelles de grandes cultures, en agriculture biologique ou conventionnelle. Pour les contrôler, une approche systémique est nécessaire.  <br>

C’est l’objectif de la tâche Modélisation du projet européen AC/DC-weeds. La modélisation qualitative multi-attributs, réalisée grâce à IPSIM (Injury Profile SIMulator), permet d’évaluer l’infestation de trois adventices vivaces : Cirsium arvense, Sonchus arvensis et Elytrigia repens. Les modèles prennent en compte l’effet du climat, du sol et des pratiques culturales, ainsi que leurs interactions sur l’infestation des adventices.  <br>

Le modèle IPSIM Cirsium dispose d’une interface disponible en ligne. Il peut être utilisé afin d’évaluer ex ante des prototypes de systèmes de culture et de simuler les infestations de Cirsium arvense dans les cultures, ou comme un diagnostic ex post afin d’aider à la conception de systèmes moins dépendants des herbicides. Enfin, le modèle peut être utilisé par les enseignants et les formateurs comme un outil pédagogique pour échanger et partager les connaissances sur la gestion agroécologique des mauvaises herbes."

# Summary. An optional shortened abstract.
summary: La modélisation qualitative permet d'évaluer l'efficacité de stratégies de gestion des adventives vivaces. Exemple avec l'outil de diagnostic IPSIM cirsium. 

tags:
featured: 

links:
- name: Phytoma
  url: http://www.phytoma-ldv.com/article-24800-Un_OAD_pour_gerer_le_chardon_des_champs
- name: Shiny app
  url: https://shiny.biosp.inrae.fr/app/ipsimcirsium
# url_code: '#'
# url_dataset: '#'
# url_poster: '#'
url_project: '/projets/ipsimcirsium/'
# url_slides: ''
# url_source: '#'
# url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
#  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

