---
title: "Qualitative modelling of fruit fly injuries on chayote in Réunion: implementation, evaluation and transfer to users"
authors:
- Jean-Philippe Deguine
- Marie-Hélène Robin
- David Camillo Corralez Munoz 
- Marie-Anne Vedy-Zecchini
- anna-doizy
- Frédéric Chiroleu
- Gauthier Quesnel 
- Isaure Paitard 
- Jean-Noël Aubertot 
date: "2021-01-01"
doi: "10.1016/j.cropro.2020.105367"
profile: false

# Schedule page publish date (NOT publication's date).
publishDate: ""

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: In *Crop Protection*
publication_short: In *Crop Protection*

abstract: "Intensive chayote cultivation in Réunion almost disappeared in the 2000s due to significant yield losses from fruit flies attacking this historically important crop. Since the late 2000s, agroecological crop protection practices have developed on the island with compelling results: the effective management of fruit fly populations, a significant reduction in pesticide use, an increase in chayote production and plantations, and the development of organic production. To assist in fruit fly management, a qualitative model which simulates fruit fly damage to chayote crops, known as IPSIM-chayote, has been designed through a collaborative effort. It is the first qualitative model developed for insect pests on a tropical crop. The IPSIM-chayote modeling platform provides satisfactory prediction results (Cohen's kappa quadratically weighted 0.79). It has a user-friendly interface and is now available free of charge online, in three languages (French, English and Spanish): https://pvbmt-apps.cirad.fr/apps/ipsim-chayote/?lang=en. The IPSIM-chayote modeling platform will be of use in many different fields: farmers use it as a tool to simulate fruit fly damage to their crops and as a decision-making tool for their agricultural practices. Transfer and training organizations (advisers, supervisors and agriculture teachers) will find it useful as a training resource in agroecological crop protection. Public authorities and local government will use it as a tool in planning and forecasting agricultural development in Réunion. Finally, researchers will find it useful as a prediction tool and resource for the exchange of information, allowing them to review scientific knowledge or identify new, relevant research areas suited to the context and challenges. IPSIM-chayote can be considered as a forum for exchange and can stimulate interactions between individuals in collaborative work. It is a flexible model, as it allows variables to be added if the context changes. IPSIM-chayote is the first qualitative model developed for crop pests in a tropical environment. It will serve as a basis for the development of other similar models simulating crop pest incidence in Réunion and the neighbouring Indian Ocean countries, thus contributing significantly to the development of agroecological crop protection."

# Summary. An optional shortened abstract.
summary: To assist in fruit fly management, a qualitative model which simulates fruit fly damage to chayote crops, known as IPSIM-chayote, has been designed through a collaborative effort.

tags:
featured: 

links:
- name: Shiny app
  url: https://pvbmt-apps.cirad.fr/apps/ipsim-chayote/
# url_code: '#'
# url_dataset: '#'
# url_poster: '#'
url_project: '/projets/ipsim-chayote/'
# url_slides: ''
# url_source: '#'
# url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
#  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

