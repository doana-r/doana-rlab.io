---
title: "Variations in mango fruit quality in response to management factors on a pre- and post-harvest continuum"
authors:
- Antoine Drouillard
- Mathieu Léchaudel
- Michel Génard
- anna-doizy
- Isabelle Grechi
date: "2023-11-08"
doi: "10.1017/S0014479723000182"
profile: false

# Schedule page publish date (NOT publication's date).
publishDate: ""

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: In *Experimental Agriculture*
publication_short: In *Experimental Agriculture*

abstract: "Fruit quality is a key factor – beginning with the producer, continuing through the supply chain, and ending with the consumer. It is described by multiple indicators and varies during the growth and ripening of the fruit. This study focused on two main aspects of Cogshall mango (Mangifera indica L.) quality: (i) the physical properties of the fruit with fresh mass (FM), pulp dry matter content (DMC), and pulp coloration; and (ii) the chemical properties with pulp sugar content and pulp acidity. These indicators were monitored on on-tree fruit, from about 60 days after bloom until full maturity. The same indicators were also monitored on fruit stored in cold storage rooms during ripening. The effects of leaf-to-fruit ratio (manageable by pruning or fruit thinning), maturity stage of fruit at harvest (manageable by harvest date), and storage temperature on the kinetics of quality traits of on-tree and stored fruit were assessed. In addition, a change-point analysis was applied to the sweetness index kinetics (used as a proxy of fruit ripening) to study fruit ripening induction. The leaf-to-fruit ratio mainly influenced fruit growth in terms of FM and pulp DMC, whereas it had less impact on the evolution of fruit chemical properties. The maturity stage of the fruit at harvest was a key factor in determining the potential quality at the ripe stage. Ripening occurs naturally at the mature green stage for on-tree fruit, but ripening at an earlier stage can be induced by harvesting the fruit. During the ripening phase, a low leaf-to-fruit ratio and a cold storage temperature tended to slow down the daily rate of sweetness increase. The use of cold temperatures during storage slowed down starch degradation and sucrose accumulation, while almost stopping the variation in fruit coloration and acidity."

# Summary. An optional shortened abstract.
summary: "This study focused on two main aspects of Cogshall mango quality: (i) the physical properties of the fruit with fresh mass, pulp dry matter content, and pulp coloration; and (ii) the chemical properties with pulp sugar content and pulp acidity."

tags:
featured: 

links:
# url_code: '#'
# url_dataset: '#'
# url_poster: '#'
# url_project: '/projets/ipsimcirsium/'
# url_slides: ''
# url_source: '#'
# url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
#  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

