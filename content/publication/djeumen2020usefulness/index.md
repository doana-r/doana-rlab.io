---
title: "A minimalistic model of vegetation physiognomies in the savanna biome"
authors:
- Ivric Valaire Yatat Djeumen
- Yves Dumont
- anna-doizy
- Pierre Couteron
date: "2021-01-15"
doi: "10.1016/j.ecolmodel.2020.109381"
profile: false

# Schedule page publish date (NOT publication's date).
publishDate: ""

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: In *Ecological Modelling*
publication_short: In *Ecological Modelling*

abstract: "We present and analyze a model aiming at recovering, as dynamical outcomes of fire-mediated tree–grass interactions, the wide range of vegetation physiognomies observable in the savanna biome along rainfall gradients at regional/continental scales. The model is based on two ordinary differential equations (ODE), for woody and grass biomass. It is parameterized from literature with respect to the African context and retains mathematical tractability, since we restricted it to the main processes, notably tree–grass asymmetric interactions (either facilitative or competitive) and the grass-fire feedback. We used a fully qualitative analysis to derive all possible long term dynamics and express them in a bifurcation diagram in relation to mean annual rainfall and fire frequency. We delineated domains of monostability (forest, grassland, savanna), of bistability (e.g. forest–grassland or forest–savanna) and even tristability. Notably, we highlighted regions in which two savanna equilibria may be jointly stable (possibly in addition to forest or grassland). We verified that common knowledge about decreasing woody biomass with increasing fire frequency is verified for all levels of rainfall, contrary to previous attempts using analogous ODE frameworks. Thus, our framework appears able to render more realistic and diversified outcomes than often thought of regarding ODE. Our model can help figure out the ongoing dynamics of savanna vegetation in large territories for which local data are sparse or absent. To explore the bifurcation diagram with different combinations of the model parameters, we have developed a user-friendly R-Shiny application freely available at : https://gitlab.com/cirad-apps/tree-grass."

# Summary. An optional shortened abstract.
summary: We present and analyze a model aiming at recovering as dynamical outcomes of tree-grass interactions the wide range of vegetation physiognomies observable in the savanna biome along rainfall gradients at regional/continental scales.

tags:
featured:  

links:
- name: Shiny app
  url: https://gitlab.com/cirad-apps/tree-grass
url_pdf: https://hal.archives-ouvertes.fr/hal-02570303/document
# url_code: '#'
# url_dataset: '#'
# url_poster: '#'
url_project: '/projets/treegrass/'
# url_slides: ''
# url_source: '#'
# url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
#  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

