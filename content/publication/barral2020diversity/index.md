---
title: "Diversity and toxigenicity of fungi that cause pineapple fruitlet core rot"
authors:
- Bastien Barral
- Marc Chillet
- anna-doizy
- Maeva Grassi
- Laetitia Ragot
- Mathieu Léchaudel
- Noel Durand
- Lindy Joy Rose
- Altus Viljoen
- Sabine Schorr-Galindo
date: "2020-05-21"
doi: "10.3390/toxins12050339"
profile: false

# Schedule page publish date (NOT publication's date).
publishDate: ""

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: In *Toxins*
publication_short: In *Toxins*

abstract: "The identity of the fungi responsible for fruitlet core rot (FCR) disease in pineapple has been the subject of investigation for some time. This study describes the diversity and toxigenic potential of fungal species causing FCR in La Reunion, an island in the Indian Ocean. One-hundred-and-fifty fungal isolates were obtained from infected and healthy fruitlets on Reunion Island and exclusively correspond to two genera of fungi: Fusarium and Talaromyces. The genus Fusarium made up 79% of the isolates, including 108 F. ananatum, 10 F. oxysporum, and one F. proliferatum. The genus Talaromyces accounted for 21% of the isolated fungi, which were all Talaromyces stollii. As the isolated fungal strains are potentially mycotoxigenic, identification and quantification of mycotoxins were carried out on naturally or artificially infected diseased fruits and under in vitro cultures of potential toxigenic isolates. Fumonisins B1 and B2 (FB1-FB2) and beauvericin (BEA) were found in infected fruitlets of pineapple and in the culture media of Fusarium species. Regarding the induction of mycotoxin in vitro, F. proliferatum produced 182 mg kg-1 of FB1 and F. oxysporum produced 192 mg kg⁻1 of BEA. These results provide a better understanding of the causal agents of FCR and their potential risk to pineapple consumers."

# Summary. An optional shortened abstract.
summary: This study describes the diversity and toxigenic potential of fungal species causing FCR in La Reunion, an island in the Indian Ocean.

tags:
featured: 

links:
# - name: Custom Link
#   url: http://example.org
url_pdf: https://www.mdpi.com/2072-6651/12/5/339/pdf
# url_code: '#'
# url_dataset: '#'
# url_poster: '#'
# url_project: ''
# url_slides: ''
# url_source: '#'
# url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
#  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

