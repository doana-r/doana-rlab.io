---
title: "Troubles neurologiques en trail : étude transversale sur 225 ultra-traileurs de La Réunion"
authors:
- Anne Perrotin
- Damien Vagner
- anna-doizy
- Laëtitia Berly
- Bruno Lemarchand
date: "2024-04-01"
profile: false

# Schedule page publish date (NOT publication's date).
publishDate: ""

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: In *Journal de Traumatologie du Sport*
publication_short: In *Journal de Traumatologie du Sport*

abstract: "Introduction. – Activité en plein essor ces dernières décennies, l'ultra-trail est une pratique
sportive aux conditions extrêmes responsables de modiﬁcations physiologiques importantes.
Les différents systèmes de l'organisme en sont impactés, dont le système nerveux périphérique.
Les atteintes neurologiques sont à ce jour peu référencées dans la littérature et leur prévalence
est probablement sous-estimée. <br><br>

Méthode. – Nous avons réalisé une étude épidémiologique transversale descriptive via un
questionnaire auto-déclaratif. Le questionnaire créé sur la plateforme Google Forms a été
transmis par mail aux principaux clubs de trail de La Réunion, puis diffusé sur internet durant
la période de février à avril 2023. Une analyse descriptive a été réalisée, puis une analyse
bivariée a été utilisée pour décrire les facteurs de risque de développer des troubles
neurologiques. <br><br>

Résultats. – Au total, 240 participants ont répondu au questionnaire et 225 ont été inclus dans
l'analyse. La prévalence des troubles neurologiques des membres inférieurs était estimée
à 49 % (n = 111). Les femmes semblaient plus à risque de développer des troubles neurolo-
giques (p = 0,028). Les syndromes canalaires représentaient 34,7 % (n = 78) et les symptômes
d'allure de polyneuropathie distale 4,4 % (n = 18) de notre échantillon. Les femmes semblaient
également plus à risque de développer des polyneuropathies distales (p = 0,015). Parmi les
participants symptomatiques, 49,5 % déclaraient l'augmentation de la distance de course
comme principal facteur déclenchant des troubles neurologiques. <br><br>

Conclusion. – La prévalence des atteintes neurologiques dans une population d'ultra-traileurs
apparaissait non négligeable et les femmes semblaient davantage concernées, notamment pour
les polyneuropathies distales. L'origine nutritionnelle ou inﬂammatoire est une hypothèse qui
reste à explorer."

# Summary. An optional shortened abstract.
summary: La prévalence des atteintes neurologiques dans une population d'ultra-traileurs apparait non négligeable et les femmes semblent davantage concernées, notamment pour les polyneuropathies distales.

tags:
featured: 

links:
- name: Article
  url: https://www.sciencedirect.com/science/article/pii/S0762915X24000366
# - name: Shiny app
#   url: https://shiny.biosp.inrae.fr/app/ipsimcirsium
# url_code: '#'
# url_dataset: '#'
# url_poster: '#'
# url_project: '/projets/ipsimcirsium/'
# url_slides: ''
# url_source: '#'
# url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
#  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

