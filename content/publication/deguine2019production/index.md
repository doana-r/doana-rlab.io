---
title: "Production durable d’extraits naturels biocides de deux Pipéracées à La Réunion"
authors:
- Jean-Philippe Deguine 
- Toulassi Atiama-Nurbel,
- C. Ajaguin Soleyen
- A. Bialecki
- Henri Beaudemoulin
- J. Carrière
- Frédéric Chiroleu,
- Anne-Marie Cortesero
- anna-doizy
- Emmanuelle Dorla
- Jacques Fillâtre
- Jean-Francois Ginglinger
- Rachel Graindorge 
- I. Grondin
- F. Lamy
- P. Laurent
- Amandine Ligonière
- P. Marchan
- G. Tostain
- and others
date: "2020-01-01"
doi: ""
profile: false

# Schedule page publish date (NOT publication's date).
publishDate: ""

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: In *Innovations agronomiques*
publication_short: In *Innovations agronomiques*

abstract: "The BIOPIPER project is a French Innovation and Partnership project that took place in Reunion Island from 2015 to 2018 with different partners of Research & Development. Its purpose was to produce natural extracts of two Piperaceae (Peperomia borbonensisand Piper borbonense) in a sustainable approach, for a use in the context of agroecological crop protection. There were 3 general objectives: (1) Synergize the skills of partners with complementary mandates on the use of endemic plants; (2) Design and implement sustainable production of both plant extracts and evaluate their efficacy as bioprotection agents; (3) Disseminate and enhance the obtained results and products. The project was structured around four actions. Action 1 made it possible todesign, develop and optimize the production of the two Piperaceae to ensure optimal production of foliar biomass. Action 2 made it possible to produce and characterize phytochemical extracts of different ecotypes of wild and cultivated plants. Action 3 measured the biocidal efficacy of extracts on various crop pests (bacteria, arthropods) of a generic nature and of economic importance in Reunion Island and at the French national level. On fruit flies, which are fearsome pests worldwide, LD 50 and LD 95 (lethal doses of 50% and 95% of the population) were measured on five species (Zeugodacus cucurbitae, Bactrocera zonata, Bactrocera dorsalis, Ceratitis quiliciiand Neoceratitis cyanescens), with essential oil-based extracts. Analysis of the LD 50 results showed differences between plants, ecotypes and types of extracts. LD 50 tests with essential oils were carried out on other economically important pests (Bemisia tabaci, Frankliniella occidentalis), a non-pest fly (Drosophila melanogaster). The essential oilof P. borbonensishas also been tested on various temperate pests (Delia radicum, Meligethes aeneus, Psylliodes chrysocephala). A biocidal effect was found on all insects tested with significant variations in sensitivity. Olfactometric tests were also carried out and revealed that the essential oil of P. borbonensisis not repellent. This result is encouraging and makes it possible to consider a targeted application of essential oil via bait in an attract & kill approach. Action 4 initiated the process of developing a regulatory folder for the commercial use of plant or extract production. Finally, the BIOPIPER project has made it possible to create an operational and original consortium in partnership in order to screen, for research or service purposes,the biocidal activity of extracts from different plants."

# Summary. An optional shortened abstract.
summary: The BIOPIPER project is a French Innovation and Partnership project that took place in Reunion Island from 2015 to 2018 with different partners of Research & Development. Its purpose was to produce natural extracts of two Piperaceae (Peperomia borbonensisand Piper borbonense) in a sustainable approach, for a use in the context of agroecological crop protection.

tags:
featured: 

links:
# - name: Custom Link
#  url: http://example.org
url_pdf: https://www6.inrae.fr/ciag/content/download/6807/49572/file/Vol79-8-Deguine/%20et/%20al.pdf
# url_code: '#'
# url_dataset: '#'
# url_poster: '#'
# url_project: ''
# url_slides: ''
# url_source: '#'
# url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
 caption: 'Credit: [**wikipedia**](https://fr.wikipedia.org/wiki/Piper_borbonense)'
 focal_point: ""
 preview_only: false

# Associated Projects (optional).
#  Associate this publication with one or more of your projects.
#  Simply enter your project's folder or file name without extension.
#  E.g. `internal-project` references `content/project/internal-project/index.md`.
#  Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#  Associate this publication with Markdown slides.
#  Simply enter your slide deck's filename without extension.
#  E.g. `slides: "example"` references `content/slides/example/index.md`.
#  Otherwise, set `slides: ""`.
slides: ""
---

