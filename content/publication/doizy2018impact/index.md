---
title: "Impact of cyber-invasive species on a large ecological network"
authors:
- anna-doizy
- Edmund Barter
- Jane Memmott
- Karen Varnham
- Thilo Gross 
date: "2018-09-05"
doi: "10.1038/s41598-018-31423-4"
profile: false

# Schedule page publish date (NOT publication's date).
publishDate: ""

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: In *Scientific reports*
publication_short: In *Scientific reports*

abstract: "As impacts of introduced species cascade through trophic levels, they can cause indirect and counter-intuitive effects. To investigate the impact of invasive species at the network scale, we use a generalized food web model, capable of propagating changes through networks with a series of ecologically realistic criteria. Using data from a small British offshore island, we quantify the impacts of four virtual invasive species (an insectivore, a herbivore, a carnivore and an omnivore whose diet is based on a rat) and explore which clusters of species react in similar ways. We find that the predictions for the impacts of invasive species are ecologically plausible, even in large networks. Species in the same taxonomic group are similarly impacted by a virtual invasive species. However, interesting differences within a given taxonomic group can occur. The results suggest that some native species may be at risk from a wider range of invasives than previously believed. The implications of these results for ecologists and land managers are discussed."

# Summary. An optional shortened abstract.
summary: To investigate the impact of invasive species at the network scale, we use a generalized food web model, capable of propagating changes through networks with a series of ecologically realistic criteria.

tags:
featured: 

links:
# - name: Custom Link
#   url: http://example.org
url_pdf: https://www.nature.com/articles/s41598-018-31423-4.pdf
# url_code: '#'
# url_dataset: '#'
# url_poster: '#'
# url_project: ''
# url_slides: ''
# url_source: '#'
# url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
#  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

