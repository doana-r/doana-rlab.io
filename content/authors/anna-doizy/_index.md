---
authors:
- anna-doizy

bio: Méthodologie scientifique et analyses de données statistiques
email: "anna@doana-r.com"

organizations:
- name: DoAna - Statistiques Réunion
  url: "/"
role: "Chercheuse, consultante et formatrice freelance <br> <div class='btn btn-outline-primary ml-onclick-form' onclick=\"ml('show', 'IT7bou', true)\">Libre comme l'R</div>"

interests:
- Aider les gens à accomplir leur volonté avec R et à avoir de la clarté dans leur problématique de recherche
- Utiliser les outils informatiques pour augmenter la validité, la reproductibilité et la transmissibilité de la science
- Crier sur ceux et celles qui veulent modifier leurs données parce que ça les arrange

education:
  courses:
  - course: Stand Up - Formation à la création et au développement d'entreprise
    institution: HEC - Paris
    year: 2021
  - course: M2 - Mathématiques pour les sciences du vivant
    institution: Université Paris Saclay
    year: 2016-2017
  - course: Diplôme d'ingénieure agronome
    institution: AgroParisTech
    year: 2013-2017

social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/anna-doizy/
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/doana-r
- icon: orcid
  icon_pack: fab
  link: https://orcid.org/0000-0002-2866-6918
- icon: cv
  icon_pack: ai
  link: "files/CV.pdf"
  
superuser: true
title: Anna Doizy
# user_groups:
# - Researchers
# - Visitors
---

Passionnée par la biologie et l'environnement, je me suis prise d'amour pour un caillou au milieu de l'Océan Indien, La Réunion.  
Je propose du conseil et des formations en méthodologie scientifique, programmation avec R, statistiques et *data science*.  

Mon travail vise à aider les scientifiques et apprenant.es scientifiques à conduire leurs projets de recherche de manière reproductible et statistiquement valide.



