---
title: Bilan d'une formation R
author: anna-doizy
date: '2024-10-21'
slug: bilan-d-une-formation-r
categories:
  - Etude de cas
tags:
  - Comprendre
  - Transmettre
  - Sens
subtitle: ''
summary: ''
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


😄 Cette photo pleine de sourires, c’était à la fin d’une session de formation d’initiation à la programmation avec R dans l’entreprise eRcane, située à Sainte-Clotilde et qui fait de la recherche et développement autour des enjeux de la canne à sucre réunionnaise.

Pour moi et les 6 participant·es (l’une d’entre elle est absente), ce n’était pas une session comme les autres.

🎉 Cette photo me permet de célébrer la restructuration de cette formation en pédagogie active et ludique.

Je te partage mon bilan :

- 🎲 j’ai adoré inventer des jeux pour expliquer des concepts de programmation complexes, aller un peu plus loin qu’une simple métaphore apportée à l’oral, faire du papier mâché et des cartes plastifiées, c’était manuel et ça m’a changé du quotidien derrière l’ordinateur.

- 📶 j’ai toujours rêvé, pendant mes parcours d’apprentissages, d’avoir une barre de progression et de savoir vers où l’enseignant nous amène et quelles sont les compétences attendues, concrètement ! Alors j’ai matérialisé des “escaliers pédagogiques” que j’affiche aux murs de la salle. Je prends le temps le matin et le soir d’avancer un pion sur son escalier pour montrer visuellement aux participant·es où iels en sont.

- ✅ j’avais des super déroulés détaillés que j’ai suivi, (presque) respectés, annotés et corrigés pour améliorer la formation. Ça m’a aidé à cadrer mes prises de paroles et celles des participant·es. À respecter les temps de pause, à donner des consignes plus claires, etc.

- 👩‍🏫 j’ai ressenti plein d’énergie en animant ces journées. Je me suis sentie dans ma bonne posture de “je transmets ce que je sais et en même temps, je dis ce que je ne sais pas et j’apporte des nuances” et j’ai essayé d’éviter la posture de “c’est moi qui sais, écoute ma divine parole”

- 🔄 j’ai appris plein de choses sur la canne à sucre et les actions d’eRcane, j’ai pu visiter les serres où les croisements de plants se font pour créer de nouvelles variétés, j’ai demandé aux participant·es de m’expliquer leurs propres jeux de données pour pouvoir les aider à mon tour dans leurs analyses

- 🌟 J’ai reçu des retours qui reflètent ce que je voulais être. Au-delà du fait que ça fait du bien à mon égo (😜), ça me permet de me dire que je suis vraiment à ma place dans mon métier et que je me sens réellement utile. Ça me donne de la confiance et de l’élan pour continuer. Et j’ai une gratitude immense pour ça.

Voilà un exemple :

> “Merci de "casser les codes" et de dispenser ta formation comme tu le fais, de façon bienveillante, ludique, intéressante, très structurée et d’être disponible quand nous en avons besoin.”

Et un autre :

> “J'ai beaucoup aimé la formation, aussi bien sur la forme que sur le fond. Sur la forme, tu nous as fait faire du code, des réflexions avec des objets physiques (l'omelette), des quizz, des jeux, des présentations formelles qui donnent du sens à tout ça, des devoirs à la maison... Et sur le fond, c'était riche, actualisé, moderne, varié et personnalisé.”

Merci 🥰

