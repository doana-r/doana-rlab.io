---
title: Les antisèches de R
author: anna-doizy
date: '2024-06-24'
slug: antiseches-r
categories:
  - Tutoriel
tags:
  - Comprendre
subtitle: ''
summary: ''
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

💡 Tu connais les antisèches de R ?


Le principe, c'est 1 ou 2 pages remplies de fonctions de R d'un package donné, rangées de manière organisée et visuelle pour  
- 😶‍🌫️ ne plus les oublier sans cesse
- 😯 en découvrir de nouvelles

C'est Posit (ex-RStudio inc.) qui a eu l'initiative, il me semble. 

Pour mes formations, j'imprime celles qui sont en lien avec le sujet et je les distribue aux apprenant.es.  
Au fur et à mesure de la formation, je leur demande de surligner les fonctions vues pour garder des repères.

Vas-y regarde par toi-même et dit m'en des nouvelles :  
- ➡️ [Base R](https://geomoer.github.io/moer-base-r/cheatsheet.html)
- ➡️ [Les packages de Posit](https://posit.co/resources/cheatsheets/) (tidyverse, shiny, quarto, etc.)
- ➡️ [En lien avec les analyses de données](https://sites.google.com/view/datascience-cheat-sheets) (pas R directement)


💬 Tu as d'autres liens pour moi ?

***

PS : depuis peu, j'ai une relieuse, ça me donne des supports hyper classes 🤩



