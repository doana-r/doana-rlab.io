---
title: "\U0001F4CA\U0001F9E0 Quand les stats et les visualisations de données rencontrent
  les émotions..."
author: anna-doizy
date: '2024-07-31'
slug: stats-visualisation-et-emotions
categories:
  - Etude de cas
tags:
  - Transmettre
subtitle: ''
summary: ''
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


Je fouinais les ressources d'une fresque du facteur humain à laquelle j'ai participé récemment...

Quand j'ai vu mes passions réunies dans une image, ça a fait 🤯

Je te partage les questionnements que j'ai eus à la découverte de cette figure et comment j'y réponds (de manière toute subjective).


## ☀️ Qu'est-ce qui fait que cette figure me parle autant ?

- 👀 elle est visuelle, elle accroche tout de suite

- 👤 on devine tout de suite le sujet : des silhouettes humaines, c'est plutôt clair

- 💡 son message est compréhensible, même par quelqu'un qui ne connait pas la notion de t-statistique ou, comme moi, les sciences cognitives

- 🙈 elle rend vi-si-ble un concept invisible (t'as déjà vu une émotion toi ?)

- 🤏 je me suis rendu compte que ce que je considérais comme un ressenti individuel est généralisable, car mesurable (ça reste des stats hein, c'est tout à fait possible que tu ressentes ta tristesse avec tes pieds, ... et ce n'est pas le cas de la moyenne de la population de l'étude)

<br>

## 🎨 Comment ces données ont-elles pu être récoltées ?

Pour simplifier, les participants ont colorié la silhouette en fonction de leurs propres ressentis physiques à la lecture des noms des émotions.

<br>

## ✅ Est-ce que cette étude est fiable ?

J'ai dû fouiller un peu plus dans l'article pour avoir des réponses. Voilà où mon attention s'est portée :

### 1️⃣ la représentativité de l'échantillonnage

- 📚 300 personnes interrogées, ça en fait un paquet et c'est cool pour avoir une bonne idée de la variabilité des réponses possibles.

- 🤷‍♀️ dont 260 femmes, alors là j'ai pas compris, clairement il y a un biais ici !

- 🇫🇮 l'expérience derrière l'image ne contient que des personnes finlandaises, mais dans une autre expérience du même article, les auteurs sont allés voir du côté des cultures asiatiques et n'ont pas vu de différences de réponses d'une culture à l'autre.

> ➡️ L'échantillonnage n'est pas parfait. Dans TOUTE étude, il y a des contraintes de finances, humaines, techniques, de temps, etc. Les auteurs font l'effort de prendre en compte certains biais d'échantillonnages, mais probablement pas tous. Il me faudrait chasser d'autres études employant un protocole similaire pour pouvoir discerner s'il y a un consensus "place des émotions dans le corps"... et je ne prendrais PAS le temps de le faire 😂



### 2️⃣ les analyses statistiques employées : 

il s'agit de modèles mixtes avec des comparaisons multiples (un test pour chacun des 50 000 pixels, ça en fait beaucoup) avec une correction de type FDR (pour ne pas surestimer les fausses découvertes). 

> ➡️ Ça, c'est mes modèles chouchous, je les connais bien ! et je suis contente de voir une correction FDR in the wild.

<br>

Mais la question la plus importante que je me pose, c'est :

👣 **Et toi ? Tu ressens des émotions dans tes pieds ?**

***

[Lien vers l'article](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3896150/) (en libre accès yay !)

