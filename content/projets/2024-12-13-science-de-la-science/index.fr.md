---
title: Découvrir la science de la science
author: anna-doizy
date: '2024-12-13'
slug: science-de-la-science
categories:
  - Brève
tags:
  - Sens
  - Transmettre
subtitle: ''
summary: "3 bonnes raisons d’aller voir la dernière vidéo de Fouloscopie..."
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


3 bonnes raisons d’aller voir la dernière vidéo de Fouloscopie :

- 🌿 il y a des MEGA beaux graphiques qui ressemblent à des plantes, c’est vraiment de l’art

- 🔭 j’ai pu prendre du recul d’une autre manière sur la progression de la science et d’où l’on vient en termes de connaissances

- ⚛️ l’interdisciplinarité et la science de la science, j’adooore


C'est ici : [Ce réseau décrypte 150 ans de découvertes scientifiques](https://www.youtube.com/watch?v=1xe3zy2mU2M)

