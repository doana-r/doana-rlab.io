---
title: Plan d'analyses statistiques
author: anna-doizy
date: '2022-10-24'
slug: plan-d-analyses-statistiques
categories:
  - Tutoriel
tags:
  - Comprendre
  - Concevoir
subtitle: ''
summary: Ce n'est pas au moment où toutes les données sont saisies qu'il faut commencer à s'inquiéter de comment on va les analyser...
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Dans le cadre des statistiques inférentielles, une analyse de données réussie ne concerne pas seulement la réponse à une question scientifique, c'est aussi et surtout toute la démarche qui se trouve derrière. La qualité de cette démarche est fondamentale pour juger de la crédibilité du résultat du projet.  

Les prérequis nécessaires (et malheureusement non suffisants) pour réussir une analyse de données statistique sont les suivants :  
☑️ Un protocole expérimental    
☑️ Un plan de gestion de données  
☑️ Des jeux de données documentés  

Si vous ne savez pas à quoi tout cela correspond et que vous envisagez d'analyser vos données selon la démarche scientifique, il est possible de vous faire accompagner par un professionnel des statistiques.

🕐 Un seul mot : **anticipez** !  
Ce n'est pas au moment où toutes les données sont saisies qu'il faut commencer à s'inquiéter de comment on va les analyser... En quoi cela fait-il partie d'une démarche expérimentale rigoureuse et reproductible ?  

**NB** : je ne parle pas du cadre des big data et de la prédiction, que je connais mal, où les données sont récupérées en masse, en vue de les analyser a posteriori, même si je soupçonne qu'il y a des parallèles intéressants à faire. Dites-moi en commentaire si vous en voyez ?

***

📜 À partir de maintenant, je m'engage avec moi-même à être beaucoup plus regardante de la qualité des données avant de choisir comment je vais travailler avec une autre personne.

Parce que ça contribuera à faire de la meilleure science.  
Parce que ça préservera ma propre santé mentale.
