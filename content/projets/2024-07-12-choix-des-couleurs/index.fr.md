---
title: Choix des couleurs
author: anna-doizy
date: '2024-07-12'
slug: choix-des-couleurs
categories:
  - Tutoriel
tags:
  - Automatiser
  - Transmettre
subtitle: ''
summary: 
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


🎨 Petite astuce pour choisir tes couleurs sans galérer et les intégrer dans tes graphiques faits avec ggplot.


🤩 J'ai découvert récemment le package [paletteer](https://github.com/EmilHvitfeldt/paletteer) via [le site de Yan Holtz](https://r-graph-gallery.com/color-palette-finder)


Dans cet [outil interactif](https://r-graph-gallery.com/color-palette-finder), tu peux filtrer les (centaines de) palettes selon :
- 🎯 une couleur cible, 
- 🤹‍♀️ le nombre de couleurs que tu souhaites, 
- 🌈 le type de palette que tu souhaites : qualitative (sans ordre), séquentielle (blanc vers bleu) ou divergente (rouge vers blanc vers bleu)

... Et tu obtiens le code pour l'intégrer dans ton graphique !

🔥 Bonnes explorations !



PS : n'oublie pas qu'une partie de la population (~ 8% des hommes et ~ 0.5% des femmes en Europe) distingue mal les couleurs 🙈 et que tu peux choisir des palettes qui prennent ça en compte. L'outil t'aide même avec ça en simulant différents types de visions !

PPS : Le reste du site vaut le détour d'ailleurs 😉

