---
title: Ce qu'il y a de nouveau dans R 4.2
author: anna-doizy
date: '2022-05-16'
slug: ce-qu-il-y-a-de-nouveau-dans-r-4-2
categories:
  - Tutoriel
tags:
  - Comprendre
  - Automatiser
subtitle: ''
summary: Voici quelques changements qui ont été apportés dans la dernière version R 4.2 et qui peuvent vous concerner.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Chaque année, l'équipe du R-project publie une nouvelle version du logiciel / langage R.

Voici quelques changements qui ont été apportés dans la dernière version R 4.2 et qui peuvent vous concerner !

Il y a, tout d'abord, de gros changements de fond pour les utilisateurs de Windows :
- Si j'ai bien compris, toute la chaîne de construction des packages (**Rtools**) a été refondée, déboguée, optimisée. Je pense qu'on ne réalise pas assez la révolution que cela représente.
- Windows 32 bits n'est plus supporté (qui utilise ça encore ?).
- Les packages ne sont plus stockés par défaut dans le même dossier qu'avant (tapez `library()` pour voir où ils sont rangés). Ils seront dans `C:\Users\username\AppData\Local` au lieu d'un obscur dossier caché et inaccessible aux utilisateurs non-admin de leur poste \o/
- L'encodage des caractères par défaut est devenu UTF-8 (encodage universel), ce qui signifie qu'il sera bien plus simple de communiquer avec les autres systèmes d'exploitation (au revoir les @~"$ à la place de tous les accents !)


Sinon, pour tout le monde, il y a diverses améliorations et débogages, dont :
- Le pipe de R-base, introduit avec R 4.0, a été amélioré. On peut désormais écrire, par exemple, `dataframe |> fonction(argument1, _, argument3)` avec `_` qui permet de signifier où l'objet `dataframe` sera inséré. Par défaut, c'est-à-dire sans `_`, il prend la place du premier argument. Attention, on ne peut utiliser `_` qu'une seule fois dans l'expression de droite.
- `format(un_vecteur_numerique, digits = 0)` renvoie une erreur. Avant le comportement changeait en fonction du système d'exploitation...
- `rbind()` nous prévient quand les longueurs des vecteurs ne sont pas multiples les uns des autres (cf. règle de R du recyclage)
- L'utilisation des opérateurs logiques `&&` et `||` renvoient un warning (et plus tard une erreur) si un des 2 côtés à une longueur supérieure à 1
- De manière similaire, si la condition dans `if()` ou `while()` est de longueur supérieure à 1, R renvoie une erreur.
- et moult autres...


Mettez à jour :

- R, 
- Rtools également si vous utilisez Windows et 
- tous vos packages ! 😉

> En utilisant de vieilles versions, vous risqueriez tomber sur des bugs totalement incompréhensibles dus à des problèmes de compatibilité.

***

Pour aller plus loin :

- [La page officielle des news](https://cran.r-project.org/doc/manuals/r-release/NEWS.html)
- [Utilisateurs de Windows](https://developer.r-project.org/Blog/public/2021/12/07/upcoming-changes-in-r-4.2-on-windows/)

