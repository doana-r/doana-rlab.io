---
title: Les modèles d'occupation
author: anna-doizy
date: '2022-04-11'
slug: modeles-d-occupation
categories:
  - Tutoriel
tags:
  - Comprendre
subtitle: ''
summary: J'ai participé à un atelier sur les modèles d'occupation. On cherche à estimer une carte d'occupation d'une espèce donnée ou bien sa dynamique d'occupation. L'enjeu principal, c'est comment interpréter les choses quand l'espèce n'est pas observée sur un site.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Le 4 et 5 avril avaient lieu les journées du [groupe de recherche EcoStat](https://gdrecostat2022.sciencesconf.org/).

Ce sont 2 jours de conférences annuels où les chercheurs du domaine présentent leurs avancées, les nouvelles méthodologies pour étudier l'écologie,...


J'ai participé à un atelier sur les modèles d'occupation. On cherche à estimer une carte d'occupation d'une espèce donnée ou bien sa dynamique d'occupation. L'enjeu principal, c'est comment interpréter les choses quand l'espèce n'est pas observée sur un site.

Comme pour un test COVID, **une absence d'observation ne signifie pas forcément l'absence de l'espèce** sur ce site. Il se peut qu'elle n'ait pas été détectée. On appelle cela un faux-négatif.

On essaie donc de modéliser la probabilité de détection de cette espèce.

Pour pallier cela, il est recommandé de **multiplier le nombre d'observations** par site (et par an dans le cas d'une dynamique).  
Et pour la fréquence des observations, il faut trouver un **compromis entre la mobilité de l'espèce** (on suppose qu'elle reste sur le site une fois qu'elle y est) **et l'indépendance temporelle** (on considère que la visite précédente n'influe pas sur la visite suivante). 

Tous les choix de protocole (taille et nombre des sites, nombre de passages, présence établie par des observations directes ou indirectes, etc) dépendent de l'écologie de l'espèce en question. C'est important de **prévoir cela en amont**.  
Par exemple, les sites ne doivent surtout pas changer d'une année à l'autre, sans quoi rien n'est interprétable. Mais aussi, **il ne faut pas développer une méthodologie trop compliquée** à mettre en place pour les humains qui récoltent les données.


***

Petit clin d'œil :
[Micro forêt de La Raffinerie](https://documentation.laraffinerie.re/index.php/La_micro_for%C3%AAt)

Plus d'information : https://gdrecostat2022.sciencesconf.org/program et https://sites.google.com/site/gdrecostat/accueil 


