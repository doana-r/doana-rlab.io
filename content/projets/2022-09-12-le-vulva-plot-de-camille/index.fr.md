---
title: Le vulva plot de Camille
author: anna-doizy
date: '2022-09-12'
slug: le-vulva-plot-de-camille
categories:
  - Brève
tags:
  - Transmettre
subtitle: ''
summary: Le vulva plot, voilà une idée originale pour produire de l'art à partir d'R !
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

**- EXCLUSIF -** Nouveau type de graphique : le vulva plot !

Voilà une idée originale pour produire de l'art à partir d'R !

Une collègue de promo, Camille Belmin, a décidé de faire un ggplot en broderie.

Je vous laisse lire par vous vous-même son cheminement et le sens qu'elle y met derrière. Je trouve ça particulièrement drôle et touchant : https://camillebelmin.github.io/post/vulva_plots/

