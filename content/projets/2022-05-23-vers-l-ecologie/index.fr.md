---
title: Vers l'écologie
author: anna-doizy
date: '2022-05-23'
lastmod: "2023-08-24"
slug: vers-l-ecologie
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Je me demande pourquoi je ne communique pas davantage sur l'écologie... Pourtant, en y regardant à deux fois, il me semble que tous mes projets DoAna sont en lien avec cette science.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Qu’est-ce que c’est que l’écologie ?  
À quoi sert cette science ?  
Comment on l'étudie ?  
Quels rôles y jouent les mystérieuses mathématiques ?

Cette vidéo présente une définition assez complète et accessible d'une facette importante de mon métier.

{{< youtube iayRpCYN7Yc >}}


<br> 

Et maintenant, je me demande pourquoi je ne communique pas davantage sur l'écologie... Pourtant, en y regardant à deux fois, il me semble que tous mes projets DoAna sont en lien avec cette science.

Peut-être parce que je la considère parfois comme un but, une direction à atteindre un peu floue et un peu idéaliste. Peut-être parce que j'imagine que les gens en ont ras-le-bol d'entendre ce mot à toutes les sauces.  
Peut-être parce que j'estime que ma valeur ajoutée en ce moment figure plutôt dans la méthodologie, les moyens, la forme que dans le fond.  
Peut-être parce que c'est plus simple de parler de choses concrètes et bien moins aisé de discuter de sens et de philosophie de vie.


***

EDIT : suite de la réflexion [ici](/projets/changement-de-civilisation/)


