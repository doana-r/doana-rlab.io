---
title: "Je reprends la base : les variables"
author: anna-doizy
date: '2024-06-12'
slug: la-base-les-variables
categories:
  - Tutoriel
tags:
  - Sens
  - Comprendre
subtitle: ''
summary: ''
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---



🤯 Prise de conscience du mois dernier : je connais et je maîtrise 1% des outils statistiques que je suis en capacité de percevoir.


🎢 Ça fait 6 ans que les statistiques font partie de mon métier et chaque jour, je prends un peu plus de recul (parfois, juste, je recule 😂).

😶‍🌫️ Et plus le temps passait, plus j'avais une angoisse qui prenait de la place à l'intérieur.
Le brouillard de "il n'y a aucune règle figée en stats" ou encore du "c'est tout au pifomètre".
Tu l'as toi aussi ce brouillard ?

😨 Regarde juste le nombre de techniques pour calculer une simple corrélation, ou le nombre de méthodes de corrections de la p-valeur dans les cas de comparaisons multiples ou le nombre de tests d'analyses de variances possibles en fonction d'un taaaaas de conditions ou encore le nombre de méthodes d'analyses factorielles.

🤏 Perso, je ne connais pas tout.  
🙈 Perso, je m'y perds souvent.  
🌐 Perso, je ne me sens PAS seule du tout à penser comme ça.  
💔 Perso, parfois, je me dis : à quoi bon ?  



⛰️ Quand je suis dans le brouillard, je cherche la montagne dans le coin et je file apprendre de nouveaux trucs pour essayer apercevoir le soleil !

📚 Je suis tombée sur le livre Modeling Mindsets de Christoph Molnar.

Je l'ai dé-vo-ré.

J'y ai trouvé ce que j'aurais voulu savoir à mon PREMIER cours de statistiques.  
La *big picture*, tu vois ce que je veux dire ? 

<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/modeling-mindsets/" target = "_blank">
    <i class="fas fa-plus"></i>
    Ici pour la big picture
  </a>
</div>
<br>

☀️ Voilà, j'ai trouvé des rayons de soleils et du haut de la montagne, j'ai vu que le monde est bien vaste.

🌱 Je connais et je maîtrise 1% des outils statistiques que je suis en capacité de percevoir. Probablement, il y en a un paquet dont je n'ai pas idée de l'existence même.

Ça m'a soulagée de clarté et ça m'a motivée pour aller explorer encore plus, sortir de mes sentiers battus habituels...

🍃 Bref : pour aller quelque part, il faut savoir d'où l'on part.  
Ça me fait du bien de reprendre la base.

Et la base des statistiques, c'est la structure des données.

🔥 Ci-dessus une infographie sur les variables, du moins, ce que j'en ai compris.

❎📦🫂 J'ajoute un focus sur les relations entre 2 variables qualitatives que j'ai mis un bout de temps à conceptualiser.


![](focus-quali-quali.png)

***

💬 Je serai hyper heureuse d'avoir ton retour !

Il y a des éléments que tu ne comprends pas ? Sur lesquels tu voudrais que je développe ? Des choses que tu n'aborderais pas de cette manière ?

***


