---
title: Modeling mindsets
author: anna-doizy
date: '2024-03-11'
slug: modeling-mindsets
categories:
  - Tutoriel
tags:
  - Comprendre
subtitle: ''
summary: Oh une PÉPITE, trouvée sur LinkedIn la semaine dernière ! Préparez-vous, je sens que je vais réutiliser cette image assez souvent.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


✨ Oh une PÉPITE, trouvée sur LinkedIn la semaine dernière ! ✨

Préparez-vous, je sens que je vais réutiliser cette image assez souvent.

✊ Pour ma part, je suis team "f**réquentiste"** et un peu "a**pprentissage non supervisé"**.

🐣 Les états d'esprits "**bayésien**" et "**inférence causale**" m'attirent particulièrement en ce moment (et je me sens débutante comme un poussin).

😯 Je n'avais jamais entendu parler de "**likelihoodism**" et du "**reinforcement learning**" ! Très curieuse d'en savoir plus !

🎽 Et toi ? Quelle team ?



***



<div class="btn-links">
  <a class="btn btn-outline-primary" href="https://leanpub.com/modeling-mindsets" target = "_blank">
    <i class="fas fa-plus"></i>
    Acheter le livre (lien non affilié)
  </a>
</div>

