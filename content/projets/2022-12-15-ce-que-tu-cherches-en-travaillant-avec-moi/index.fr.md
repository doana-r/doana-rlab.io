---
title: Ce que tu cherches en travaillant avec moi
author: anna-doizy
date: '2022-12-15'
slug: ce-que-tu-cherches-en-travaillant-avec-moi
categories:
  - Brève
tags:
  - Automatiser
  - Comprendre
  - Concevoir
  - Sens
  - Transmettre
subtitle: ''
summary: Trois raisons d'avoir envie de collaborer avec moi !
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Mes collaborateurs et collaboratrices, qui sont des chercheurs et des chercheuses (académie ou privé ou associatif), choisissent de travailler avec moi :
- ✅ quand ils ou elles veulent faire leurs analyses eux- ou elles-mêmes, mais avoir un guide pour valider leurs choix et leur ouvrir d'éventuelles nouvelles voies, parce qu'il y a toujours des possibilités d'apprendre, d'expérimenter et de grandir, en particulier dans le milieu scientifique
- 🎲 quand ils ou elles veulent apprendre et devenir autonome avec leurs données, parce que R et les statistiques sont exigeantes
- 🗝️ quand ils ou elles souhaitent déléguer une partie de leur travail sur leur question du moment, parce qu'ils ou elles n'ont pas le temps, l'énergie, la disponibilité mentale de tout faire eux- ou elles-mêmes

