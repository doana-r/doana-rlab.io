---
title: La puissance d'une communauté opensource
author: anna-doizy
date: '2024-07-16'
slug: puissance-communaute-opensource
categories:
  - Tutoriel
tags:
  - Automatiser
  - Comprendre
  - Sens
subtitle: ''
summary: Quand j'ai commencé à travailler avec R, je n'imaginais absolument pas la puissance d'une communauté opensource. Ça va faire environ une dizaine de fois maintenant...
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


🔥 Quand j'ai commencé à travailler avec R, je n'imaginais absolument pas la puissance d'une communauté opensource.

<br>

Ça va faire environ une dizaine de fois maintenant :

- 💔 que je trouve un bug dans un de mes packages préférés ou que j'ai une question le concernant
- 🔊 que je demande un avis à la communauté et/ou
- 🌐 que je contacte la personne à un autre bout du monde qui est chargée de maintenir le package en question
- 💡 que j'ai une réponse et une solution en moins de 24 heures

<br>

🤯 Je trouve ça juste... dingue

<br>


🤠 Bon mais, c'est pas tout. À ton tour !  
Comment faire pour aider les gens à t'aider ?

💌 Il s'agit d'un exercice d'empathie, mets-toi à leur place !  
Il faut que ta question soit hyper claire ☀️ et reproductible ♻️

<br>

Voici comme exemple, ce que j'ai fait la semaine dernière :

- 1️⃣ ➡️ si besoin, demander [l'avis à la communauté](https://stackoverflow.com/questions/78719144/bug-report-when-emmeans-is-used-along-with-fct-na-value-to-level)

- 2️⃣ ➡️ soumettre un [rapport de bug](https://github.com/rvlenth/emmeans/issues/500) (ou une question, feature request, update request, ...) au package concerné. Veille bien à lire les consignes aux contributeurs (c'est toi), si tu envisages de le faire !

<br>

📜 Ci-dessous, je te donne une antisèche de comment produire un Exemple Minimal Reproductible (de son petit nom).

<div class="btn-links">
  <a class="btn btn-outline-primary" href="https://www.doana-r.com/projets/aide-moi-a-t-aider/" target = "_blank">
    <i class="fas fa-plus"></i>
    Article Aide-moi à t'aider !
  </a>
</div>

<br>

Donne-moi des news si tu en fais un, je serai ravie de sentir que tu m'aides à t'aider 🤩 

***

🙏 Un é-nor-me merci à cette communauté R  
🫴 J'y contribue à mon échelle, via mes contenus en ligne et un don annuel   
👑 Ce sont les humains qui la composent qui créent, consolident, documentent, jour après jour, le logiciel majeur dont dépend mon travail... que j'adore



