---
title: Imaginer une autre histoire par rapport aux financements de la recherche
author: anna-doizy
date: '2024-03-06'
slug: imaginer-une-autre-histoire-par-rapport-aux-financements-de-la-recherche
categories:
  - Brève
tags:
  - Sens
  - Concevoir
subtitle: ''
summary: J'observe que ce qui figure dans les dossiers de financement des projets de recherche correspond rarement à ce qui se passe dans la réalité, une fois que ledit financement est décroché. J'interroge des chercheur.es pour savoir ce qu'ils et elles en pensent.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


👀 J'observe que ce qui figure dans les dossiers de financement des projets de recherche correspond rarement à ce qui se passe dans la réalité, une fois que ledit financement est décroché.

J'interroge des chercheur.es pour savoir ce qu'ils et elles en pensent.

Certain.es me disent que c'est une question de manque de compétence, ou alors d'égo, ou alors de se faire bien voir,  ou encore [remplit avec tes explications].

💬 J'ai entendu : "Pour décrocher de l'argent et avancer, des chercheur.es sont prêt.es à soumettre un dossier pour lequel ils savent exactement quelle sera la conclusion... avant même de commencer l'étude."

💬 J'ai entendu aussi : "Anna, on n'a pas le choix, il faut adapter nos idées aux attentes du financeur.es, sinon ils ne voudront jamais accepter. Par exemple, j'ai dû tourner tout mon article sur le côté insulaire dans le monde, alors que mon étude ne concerne qu'une seule île, n = 1 !"

📰 Au sujet de l'égo, je suis tombée sur une [tribune du journal Le Monde](https://www.lemonde.fr/sciences/article/2024/03/05/la-recherche-pilier-de-la-demesure-actuelle-sert-la-volonte-de-puissance_6220225_1650684.html) qui est parue hier. Ça m'a fait 🤯
Je prends le risque de mettre le texte complet ci-dessous. C'est trop important comme sujet ! (et je me sens moi-même hyper concernée...)

🤔 Je me demande à quel point ces croyances (qui peuvent être tout à fait véridiques, mais je n'ai pas pu vérifier par moi-même) font partie du problème. 
Elles accusent soit les scientifiques eux-mêmes - "bouh t'es trop nuuul.le", soit le système - "monstre immuable". 

Soit, on (se) culpabilise et on maltraite.
Soit, on se déresponsabilise sur un bourreau externe et on devient une victime.
🙈🙊🙉



## 🌱 Et si on imaginait une autre histoire ?


Ce que je constate, *et ce n'est que mon interprétation personnelle, à cet instant T*, c'est le **manque de cohérence**. 

➡️ Cohérence entre les attentes des financeurs, celles des personnes qui font la recherche, celles des personnes qui bénéficient de la recherche (les agriculteur.ices et les instituts techniques dans le domaine de l'agronomie, par exemple).  
➡️ Cohérence entre la formulation de la question de recherche et la conception du protocole expérimental (comment sont récoltées les données ? qu'est-ce qui est mesuré ? combien de répétitions ? quelle structure entre les variables ?...)  
➡️ Cohérence entre les données récoltées et le choix des modèles pour faire des analyses statistiques.  
➡️ Cohérence entre l'interprétation des résultats et la manière dont ils sont valorisés (quel choix de journal ? quelle vulgarisation et à qui ?).  
➡️ Cohérence entre une méthodologie qui prend du temps pour produire de la connaissance fiable et un monde en mouvement rapide et imprévisible.

😨 Ça me fait mal de constater tout ça.

♻️ Et en même temps, je suis heureuse de pouvoir y mettre des mots comme ceux-là.
Parce que changer d'angle de vue, voir le problème de cette manière me permet d'avoir envie de chercher des stratégies pour remettre de la cohérence.

Chercher à remettre un peu de cohérence, brique par brique, ça me donne une forme de pouvoir d'agir. Ça me permet aussi de garder beaucoup de bienveillance par rapport à ce qui se fait.

🤏 C'est pas parfait, c'est sûr. Et ça ne rendra rien parfait.
Oui, et alors ? Qu'est-ce qui est parfait ?

🐦 Telle une colibri, j'ai envie de contribuer au besoin de cohérence systémique du monde de la recherche.
Ça m'emplit de joie.


En tous les cas, ça m'intéresse de savoir ce que tu en penses toi ? Quel est ton vécu par rapport à ce sujet ? Qu'est-ce que tu ressens ? N'hésite pas à me le partager par mail ! ✍️


***


☀️ Merci à Thomas, Dani, Lucas, Marine, Clément, Alex, JD, Caroline... pour me permettre d'avoir ce genre de réflexions qui me font tant avancer !



***



## « La recherche, pilier de la démesure actuelle, sert la volonté de puissance »


François Graner  
*directeur de recherche au CNRS, laboratoire Matière et systèmes complexes, Université Paris Cité*

Directeur de recherche au CNRS, François Graner appelle, dans une tribune au « Monde », à s’interroger sur les enjeux, les motivations et les impacts de la recherche scientifique, moteur d’une croissance économique destructrice. Et prône la décroissance.


>La croissance économique se heurte aux limites physiques de la planète, et la ravage. Or la recherche est un moteur de cette croissance. Faute d’expliciter cet impact indirect et essentiel de la recherche, les débats sur son utilité se limitent généralement à ses retombées directes.
>
>Les promoteurs de la recherche soulignent pêle-mêle que la curiosité et le recours à la technique sont deux caractéristiques fondamentales de l’espèce humaine, ou que la recherche contribue au confort matériel et à la longévité. Toutes aussi disparates, les critiques de la recherche portent sur ses applications militaires ou sociétales : manipulation du vivant, poursuite sans frein du profit, dégradation du corps humain en marchandise.
>
>Il est impossible d’en tirer une analyse bénéfice-risque valide, aucune compagnie d’assurances ne peut garantir la recherche, car elle a une spécificité : ses retombées sont imprévisibles, peuvent parfois se faire sentir après plusieurs siècles, et ne peuvent pas s’anticiper par une analyse statistique du passé.
>
>Prenons l’exemple de la virologie. Séparément, deux équipes de virologues, au Japon et aux Pays-Bas, ont modifié le virus de grippe aviaire H5N1 en 2011, et l’ont rendu contaminant pour des cellules de mammifères. L’objectif annoncé, celui d’être prêt à réaliser vite un vaccin humain en cas de pandémie, était irréaliste, les virus étant trop variables pour être prévisibles (contre le Covid-19, c’est une stratégie différente qui s’est révélée pertinente).
>
> ### Des expériences d’apprenti sorcier
>
>En revanche, le risque était réel : en 2014, au vu de la fréquence des accidents de laboratoire, les Etats-Unis ont instauré un moratoire sur le financement des recherches dites « préoccupantes » en virologie ; des amendements lui ont fait perdre toute portée dès 2017. Entre-temps, des chercheurs de Caroline du Nord ont transféré leurs expériences vers Wuhan, en Chine, pour aller plus vite et à moindre coût. On ne sait toujours pas si le SARS-CoV-2 en est issu. En revanche, on sait que, pendant la pandémie de Covid-19, des fuites de laboratoire ont été enregistrées, entre autres à Taïwan en octobre et novembre 2021. En 2022, en plein cœur de Boston, des chercheurs ont construit un virus combinant la contagiosité du variant Omicron avec la létalité d’un des premiers variants. Risque de pandémie contre promesse de vaccin : quel bilan en tirer ?
>
>L’impact indirect est certain et plus grave. Plus généralement, soulignent des collectifs critiques comme Pièces et main d’œuvre ou le groupe Grothendieck, la recherche sert la compétition et la volonté de puissance. D’où ses impacts indirects sur l’état de la planète, bien plus significatifs que ses impacts directs. Elle est un pilier de la démesure actuelle, et de la quête de l’illimité, qui se manifeste entre autres par des expériences d’apprenti sorcier : transhumanisme, forçage génétique, colonisation de Mars ou interface cerveau-machine.
>
>Il s’agit bien là de toute la recherche, publique ou privée, civile ou militaire, académique ou industrielle, fondamentale ou appliquée. Toutes disciplines confondues, sciences humaines comprises : la géographie, l’archéologie, l’anthropologie, la psychologie et la sociologie ont contribué à la volonté de puissance.
>
>Grâce à la recherche, la croissance se poursuivra-t-elle tout en réduisant ses impacts sur l’énergie, les matières et l’environnement ? Cette chimère de « croissance verte » ou de « développement durable » ne repose ni sur une base théorique consensuelle ni sur des expériences généralisables. Orienter en conséquence les choix économiques et industriels relève au mieux de la naïveté, au pire de la duperie. Car les gains en efficacité que permet la recherche induisent l’augmentation des usages, et donc in fine de l’impact total : cet « effet rebond » est évident pour le numérique.
>
> ### Sortir du capitalisme néolibéral
>
>Tandis qu’améliorer le recyclage n’est pas impossible, le rêve actuel d’une « économie circulaire » (qui produirait biens et services en prélevant très peu d’énergie et nulle matière) est aussi peu compatible avec les principes de la physique que la vieille quête de la machine à mouvement perpétuel. Plus généralement, au sein d’un système complexe, une solution technique à un problème précis en engendre inéluctablement d’autres. Le dernier numéro de la revue Raison présente décrypte des fausses bonnes idées, comme la manipulation de l’atmosphère censée stabiliser le climat (la géo-ingénierie).
>
>Alors que faire ?
>
>Face à un tel problème de fond, une loi, une convention citoyenne, un comité d’éthique, un appel à la responsabilité des chercheurs (certes potentiellement utiles par ailleurs) interviennent trop peu, trop lentement, trop en aval. La priorité est de sortir du capitalisme néolibéral, et plus généralement de la volonté de puissance.
>
>Car la prudence consiste à accepter les limites, respecter l’environnement et lutter contre les inégalités sociales et géographiques. Et pour cela réduire massivement la taille et la puissance des activités humaines, de même que la consommation de ressources. Cela s’appelle la décroissance. Elle fait face à des obstacles humains et non physiques. Dès lors, elle est plus réaliste qu’une croissance verte ou infinie.
>
>**C’est seulement quand on aura changé ce socle et ces valeurs qu’on aura un cadre correct pour concevoir et refonder une recherche utile.**


François Graner, directeur de recherche CNRS au laboratoire Matière et systèmes complexes, université Paris Cité



