---
title: Penser à valoriser les données
author: anna-doizy
date: '2024-02-15'
slug: penser-valoriser-données
categories:
  - Brève
tags:
  - Concevoir
subtitle: ''
summary: J'ai besoin d'aide pour savoir ce qu'il ne faut surtout pas oublier au démarrage d'un projet de sciences participative.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---



🌱 Il y a plein de projets de sciences participatives hyper enthousiasmants autour de la conservation de la biodiversité et la sensibilisation des personnes à la nature, mais qui oublient un ÉNORME détail.

✨ Comment valoriser les données ? ✨

➡️ A qui ?  
➡️ Sous quelle forme ?  
➡️ À quels objectifs et quelle question vont répondre ces données ?  
➡️ Qui va s'en charger ?  
➡️ Quelles compétences et outils sont nécessaires ?  
➡️ Ça va durer combien de temps ?  
➡️ Est-ce qu'on automatise ?  
➡️ ...

Je pose ces questions à une personne qui veut démarrer un projet comme ça (que je trouve très pertinent !) et on me répond : "ah ben je n'y avais pas pensé"

🙈

Et ça donne quoi, quand on n'y pense pas ?

Un gros tas de données qui traînent.  
🤷️ Voilà.

Fin.  
😭

---

Je n'ai pas beaucoup d'expérience en sciences participatives et du coup, j'appelle à la communauté, c'est quoi vous vos tips pour ne pas vous retrouver juste avec un gros tas de données qui traînent ?  
A quoi il faut absolument penser ? 💡

Merciii !

