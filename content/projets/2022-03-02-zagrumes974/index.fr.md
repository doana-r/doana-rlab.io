---
title: Zagrumes974
author: anna-doizy
date: '2022-03-02'
slug: zagrumes974
categories:
  - App Shiny
tags:
  - Transmettre
subtitle: ''
summary: Application web du suivi épidémiologique des agrumes face au HLB à la Réunion.
authors: []
lastmod: '2023-06-20T16:37:52+04:00'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Je te présente ici le projet **Zagrumes974** pour valoriser les travaux de thèse de Dr. [Ismaël Houillon](https://agritrop.cirad.fr/602247/) à propos du suivi et de la surveillance d'une maladie des agrumes à La Réunion.


<iframe class="shiny-iframe" src="https://pvbmt-apps.cirad.fr/apps/zagrumes974/" frameborder="no"></iframe>


Lien vers l'application (full screen) : https://pvbmt-apps.cirad.fr/apps/zagrumes974/




