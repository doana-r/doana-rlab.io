---
title: 'Prendre soin du début de l''aventure '
author: anna-doizy
date: '2023-10-19'
slug: prendre-soin-du-debut-de-l-aventure
categories:
  - Offre
tags:
  - Comprendre
  - Concevoir
  - Sens
subtitle: ''
summary: Je suis en train de concevoir le projet qui me tient le plus à cœur depuis que j'ai découvert le milieu de la recherche expérimentale, il y a 5 ans. Il s'agit d'une formation pour clarifier son protocole expérimental en utilisant des outils de co-construction et d'intelligence collective.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

☀️ Je suis en train de concevoir le projet qui me tient le plus à cœur depuis que j'ai découvert le milieu de la recherche expérimentale, il y a 5 ans.

📜 Il s'agit d'une formation pour clarifier son protocole expérimental en utilisant des outils de co-construction et d'intelligence collective.

Au fur et à mesure des années, j'ai atteint un niveau d'exigence assez élevé (mais réaliste !) en termes :
- 📊 de démarche expérimentale (reproductibilité, qualité, tout ça tout ça)
- 🎯 et notamment de cohérence entre la question de recherche, les données récoltées et le choix des analyses réalisées.

😣 Sauf que... à mon plus grand désarroi, plus de 75% des projets que j'ai suivis de près ou de loin n'ont pas atteint ce niveau. (Tu as le droit de ne pas me croire, je ne t'en voudrais pas.)

Pourquoi ?

🙈 Je choisis éhontément de manquer de nuance ici : nous ne prenons pas assez soin du début de nos projets de recherche. (Je m'inclus parce que j'ai fait l'erreur aussi)

🤏 Les personnes qui viennent me voir pour être accompagnées ou pour collaborer avec moi arrivent bien souvent trop tard : les données sont déjà récoltées. Vite vite, il me reste - 2 semaines pour faire les analyses !

⛰️ Et là, je me sens tellement impuissante !

- ❌ Ça m'est arrivée de torturer les données pour les rendre plus... carrées.
- ❌ Ça m'est arrivée d'insister pour changer la formulation de la question de recherche.
- ❌ Ça m'est arrivé de ne rien faire de plus que ce qu'on me demandait de faire.
- ❌ Mais le pire... le pire, c'est d'oser être honnête, dire "non, je ne peux rien faire de plus pour toi" et de voir la perte d'espoir, le visage défait, même les larmes de la personne en face (heureusement, ça ne m'est arrivé que 2 fois)

Je t'avoue un truc : je souhaite tant ne plus ja-mais avoir à faire ça.

✨ Prendre soin du début du projet ne pourra pas résoudre tous les soucis, imprévus et autres manques de temps / financement, mais l'augmentation de qualité du projet et du bien-être des personnes qui le portent sera significative.

Plusieurs doctorants en fin de thèse m'ont dit : 

> "Si j'avais su tout ça, j'aurais passé 6 mois de moins à galérer, au moins."

🌱 Alors, j'ai décidé de faire une formation spéciale "questions qui fâchent avant qu'elles fâchent" pour avoir plus de clarté, plus de cohérence, plus de qualité. Je suis convaincue qu'on peut tous(tes) faire un petit pas pour s'améliorer !

La présentation ci-dessous, je l'ai commentée lors des rencontres du groupe de recherche EcoStat la semaine dernière. Une vidéo devrait sortir bientôt.

Ce projet peut voir le jour grâce :
- 🪙 au financement du groupe de recherche EcoStat
- 🎲 à une dizaine de chercheur(se)s et statisticien(ne)s qui m'ont inspiré les études de cas et les jeux de la formation
- 🫂 mes compagnons indépendant(e)s/freelance avec leur soutien à toute épreuve
- 🎓 l'accompagnement d'une formatrice de formateurs en pédagogie active


Voilà ma présentation et même un audio pour t'entrainer à tourner les slides au bon moment (et désolée pour les couacs de connexion, il y a des trous) :


<iframe class="shiny-iframe" src="https://doana-r.gitlab.io/2023-10-10-presentation-gdr-ecostat" frameborder="no"></iframe>


<audio src="prendre soin de l'aventure.mp3" controls="controls">
Your browser does not support the audio element.
</audio>

***

Pour voir toutes les présentations des rencontres du GDR Ecostat, c'est [ici](https://www.youtube.com/@gdrecostat) (je suis dans la partie 4).


