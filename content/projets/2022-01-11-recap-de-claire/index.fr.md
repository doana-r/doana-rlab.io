---
title: Récap de Claire
author: anna-doizy
date: '2022-01-11'
slug: recap-de-claire
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: J'aime bien le nouveau format d'article de Claire Della Vedova, biostatisticienne de son état. Elle y récapitule les bons plans, astuces et nouvelles fonctionnalités de R qu'elle a trouvé dans le mois.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


J'aime bien le nouveau format d'article de Claire Della Vedova, biostatisticienne de son état.

Elle y récapitule les bons plans, astuces et nouvelles fonctionnalités de R qu'elle a trouvé dans le mois.

J'apprends et je découvre toujours plein de choses en la lisant.

C'est très accessible, n'hésitez pas à jeter un coup d'œil !

En passant, merci Claire de m'avoir donné cet élan pour me mettre à écrire, moi aussi.

- https://delladata.fr/simuler-des-donnees-data-r/
- https://delladata.fr/la-recap-du-mois-de-novembre-2021/

