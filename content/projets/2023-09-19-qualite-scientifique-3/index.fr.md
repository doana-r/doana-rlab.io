---
title: Qualité scientifique - 3/4
author: anna-doizy
date: '2023-09-19'
slug: qualite-scientifique-3
categories:
  - Tutoriel
tags:
  - Comprendre
  - Concevoir
  - Sens
  - Transmettre
subtitle: ''
summary: La simplexité est l’art de rendre simples, lisibles, compréhensibles les choses complexes. Youpi, que j'aime ce mot que je ne connaissais pas.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


## 🤯 La simplexité 🤯


> La **simplexité** est l’art de rendre simples, lisibles, compréhensibles les choses complexes.

"Simplexe" est à "simple" ce que "complexe" est à "compliqué".

😊 Youpi, que j'aime ce mot que je ne connaissais pas.

***

*"Pourquoi faire simple quand on peut faire compliqué ?"* disent les Shadoks.

Et Leonardo Da Vinci de rétorquer : *"La simplicité est la sophistication suprême"*.

***

Dans mon quotidien, je rencontre ce principe, par exemple :

- ➡️ quand je construis un **protocole expérimental** : plus il est simple à comprendre, plus ce sera simple de le mettre en œuvre techniquement, de faire un suivi fiable, d'être reproductible, de faire des modèles appropriés, etc.

- ➡️ quand j'utilise mes outils de **manipulation de données** préférés avec la philosophie de programmation des packages du tidyverse (notamment `ggplot2` et `dplyr`) qui proposent des fonctions très simples et pipable (avec `%>%` ou `|>`)

- ➡️ quand j'interprète des résultats ou quand je me fais mon avis sur des opinions externes, grâce au principe scientifique fondamental qu'est le **rasoir d'Ockham ou principe de parcimonie** : "les hypothèses suffisantes les plus simples doivent être préférées"

- ➡️ quand je choisis un **modèle statistique** pour mieux comprendre un jeu de données et répondre à une question scientifique. Le **critère d'Akaike** par exemple est un critère de parcimonie qui favorise des modèles proches de la réalité (maximisation de la vraisemblance) et simples en même temps (minimisation du nombre de paramètres à estimer)


En creusant dans cette direction, je suis tombée sur un autre concept proche : le **principe KISS** !

> 😉 Keep it simple, stupid! (pleins de variantes possibles)

Le principe KISS vient du monde des développeurs informatiques. Il proscrit seulement les **complexités non indispensables**, car, paradoxalement, tenter d'utiliser des moyens simples pour résoudre un problème complexe peut conduire à une complexité encore plus grande.

Il s'agit donc de trouver le bon équilibre → la **simplexité** ! 🤯

***

Sources :

- https://fr.wikipedia.org/wiki/Simplexit%C3%A9
- https://fr.wikipedia.org/wiki/Crit%C3%A8re_d%27information_d%27Akaike
- https://fr.wikipedia.org/wiki/Rasoir_d%27Ockham
- https://fr.wikipedia.org/wiki/Principe_KISS

***

<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-1">
    <i class="fas fa-plus"></i>
    Lire Qualité 1/4
  </a>

  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-2">
    <i class="fas fa-plus"></i>
    Lire Qualité 2/4
  </a>
  
  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-4">
    <i class="fas fa-plus"></i>
    Lire Qualité 4/4
  </a>
</div>
