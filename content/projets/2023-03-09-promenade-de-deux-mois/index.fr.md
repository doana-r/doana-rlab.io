---
title: Promenade de deux mois
author: anna-doizy
date: '2023-03-09'
slug: promenade-de-deux-mois
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary:  Je m'en vais faire une balade de deux mois. Souhaite-moi un bon voyage.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


🚶️ Je m'en vais faire une balade de deux mois.

🧳 Deux mois très loin de mon territoire de cœur. À 10 000 km !  
Je vais tout simplement vadrouiller en métropole (et à Barcelone).  
J'ai plein de projets. Pro et perso se mixent allègrement. Tous sont (HYPER) enthousiasmants.

✨ J'ai posé l'intention que mon année 2023 serait une année de rencontres riches, sincères, justes, durables.  
Une année de prise de recul, de structuration, d'ouverture à d'autres possibles.  
Cette promenade est un (grand) pas vers ça.  

🙈 Alors, je t'avoue, c'est un peu inconfortable, car je n'aurai plus de visibilité pour :
- 💻 le travail à distance avec mes client(e)s actuelles, 
- 🎦 les rendez-vous en visio à caler, 
- ✍️ les moments où je vais te parler ici...

Mais bon, je prends le risque ! J'ose ! Je fonce !

😊 Cette aventure m'exalte d'avance !

Souhaite-moi un bon voyage.

PS : une promenade de 2 mois... je vais arriver à TOUT débugger après ça ! 😉  
PPS : Quelles sont tes intentions pour 2023 ? C'est en bonne direction ?
