---
title: Check_model()
author: anna-doizy
date: '2022-10-17'
lastmod: "2023-06-28"
slug: check-model
categories:
  - Tutoriel
tags:
  - Comprendre
subtitle: ''
summary: Comment faire pour vérifier que les prérequis sous-jacents des modèles paramétriques sont bien validés et comment faire pour l'expliquer aux autres.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Je me suis longtemps demandée comment faire pour vérifier simplement que les prérequis sous-jacents des modèles paramétriques que j'utilise sont bien validés et surtout comment faire pour l'expliquer aux autres.  
Par exemple, pour avoir le droit de regarder les résultats d'un modèle linéaire (fonction `lm()`), il faut que :  

➡️ les individus (les lignes du tableau) soient indépendants, c'est-à-dire qu'ils ne soient pas liés, de part le protocole ou l'échantillonnage, par une structure spatiale ou temporelle, notamment. Si jamais, ce n'est pas le cas, allez voir du côté les modèles mixtes, parce que vous avez peut-être une variable aléatoire dans le game ! 

➡️ les résidus du modèle (la différence entre les prédictions du modèle et ce que vous avez observé) doivent être normalement distribués, centrés et de variance constante. Si ce n'est pas le cas, pensez à un modèle non paramétrique ou bien une transformation de variable Y de type Box-Cox (moi, j'aime bien, mais je sais qu'il y a plusieurs écoles)

🗒️ Il y a plein de variantes pour d'autres types de modèles, par exemple, pour les glm Poisson (qui permettent de modéliser les comptages), il faudrait vérifier la (sur)dispersion, c'est-à-dire si la variance théorique et la variance observée ne sont pas différentes. Et si ce n'est le cas, il faut changer de modèle (negative binomial, ou bien zero-inflated model).

😣 Vous n'avez rien compris ? C'est un peu normal, c'est quand même hyper abstrait...  
La fonction `check_model()` dans le package [**performance**](https://www.rdocumentation.org/packages/performance/) m'aide pas mal. Elle ne dit pas (encore 😉) quoi faire si les hypothèses ne sont pas remplies, mais au moins, elle indique précisément ce qu'il faut vérifier.  
En plus, c'est joli. Moi, ça me plaît ! haha  
Elle ne fonctionne pas encore pour tous les types de modèles. Par exemple, je l'utilise pour les `lm()` et les glm mixtes du type `lme4::glmer()` (uniquement sur la partie résidus, pas sur la partie indépendance, parce que c'est à vous de le savoir !). Je n'ai pas encore testé pour les `mgcv::gam()` et je n'ai pas réussi à la faire fonctionner pour les `glm()`. Mais cela devrait venir avec le temps. [**EDIT** : oui, ça fonctionne avec les glm maintenant]
En revanche, il y a plein de fonctions intermédiaires du type `check_*()`, dans le package **performance**, à vous d'explorer.

💡 Et dites-moi si vous trouvez un `check_*()` qui révolutionne votre manière de faire des stats !! Je suis preneuse 😊

