---
title: Une intervention au collège
author: anna-doizy
date: '2023-12-12'
slug: intervention-college
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: "Aujourd'hui, je suis intervenue dans un collège pour parler de mon parcours devant quatre classes de 3ᵉ et 4ᵉ. Pour parler de la place des maths dans ma vie en tant que femme."
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🗨️ Aujourd'hui, je suis intervenue dans un collège pour parler de mon parcours devant quatre classes de 3ᵉ et 4ᵉ. Pour parler de la place des maths dans ma vie en tant que femme.

👩 Il y a beaucoup de stéréotypes de genre, qui impactent notamment notre orientation professionnelle.

🌱 En faisant ma présentation, je racontais que j'ai commencé mes études par la bio, plutôt que par les maths.

🙈 Et je me suis demandé : est-ce qu'inconsciemment, je ne me suis pas freinée ?
"Anna, tu seras avec beaucoup d'hommes, est-ce que tu vas t'en sortir ?"

❎ Quand les personnes formidables de l'association [Femmes et Mathématiques](https://femmes-et-maths.fr/) m'ont contactée pour m'interviewer, j'ai clairement pensé :
"Mais pourquoi, elles me contactent pour cette expo ? Je ne suis pas mathématicienne, moi !"

🎯 Ben, en fait, si, j'ai vu les retours extérieurs à moi, et en effet, les maths, et la science de manière plus générale, ont une place toute particulière dans ma vie. Elles contribuent à mon équilibre.

☀️ Et c'est ça que j'ai raconté aujourd'hui devant +100 collégiens.

***

Merci Véronique Slovacek-Chauveau et toutes les personnes qui ont contribué à la création et dissémination de cette exposition.

Merci Ghislaine Crabé pour l'accueil enthousiaste au collège des Aigrettes.

Merci Jocelyne Legrain de m'avoir encouragée à venir avec toi et d'avoir été un autre modèle pour les élèves (très différent du mien 😉) de femme mathématicienne.

***

PS : j'ai mis mon T-shirt préféré pour l'occasion !


<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/exposition-femmes-et-mathematiques/">
    <i class="fas fa-plus"></i>
    En savoir plus sur l'expo
  </a>
</div>

