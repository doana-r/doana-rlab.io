---
title: Lire les messages d'erreur
author: anna-doizy
date: '2023-01-19'
slug: lire-les-messages-d-erreur
categories:
  - Brève
tags:
  - Comprendre
subtitle: ''
summary: Russel Lenth, crée du CODE pour que les utilisateurs VOIENT les pièges, mais SURTOUT ne les mettent pas sous le tapis
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Emmeans, j'en parle assez souvent.

📊 C'est un package de R qui permet de construire des intervalles de confiance quand il y a plus de 2 "traitements" et de faire des tests de comparaisons multiples avec les corrections adéquates.

⚠️ J'ai déjà parlé des pièges des comparaisons multiples et ce n'est pas le sujet de ce post.

🧠 Ce que je trouve extra, c'est à quel point ces pièges sont ignorés, plus ou moins consciemment. La preuve : même le développeur principal du package, Russel Lenth, crée du CODE pour que les utilisateurs VOIENT les pièges, mais SURTOUT ne les mettent pas sous le tapis !

👀 Hey toi ! Oui, toi ! Quand tu vois un message qu'un gentil dev a pris le soin d'écrire pour t'éviter de faire n'importe quoi, tu n'enlèves PAS le message.   
Tu le lis.  

(et si tu ne le comprends pas, tu m'appelles) ← oui, je mets des parenthèses pour faire mon auto-promotion, désolée, c'est archidifficile. Essaye, toi, pour voir !


---

Source : [NEWS for the emmeans package](https://pbil.univ-lyon1.fr/CRAN/web/packages/emmeans/news/news.html?fbclid=IwAR0wLzk4XTJt2Jzdm6YnxVwBweqkdJYutcVkAC9Y091vf-mv0qy163GQcog)

