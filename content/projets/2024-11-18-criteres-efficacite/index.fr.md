---
title: Critères d'efficacité
author: anna-doizy
date: '2024-11-18'
slug: criteres-efficacite
categories:
  - Brève
tags:
  - Automatiser
subtitle: ''
summary: "Voilà un petit mémo de réflexes sympa à acquérir avec R pour être un peu plus efficace et plus reproductible dans tes analyses de données !"
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


Voilà un petit mémo de réflexes sympa à acquérir avec R pour être un peu plus efficace et plus reproductible dans tes analyses de données !

Liste non exhaustive 😜

- 💡 Tu as d’autres idées pour moi de critères à ajouter dans cette liste ?
- 🤨 C’est quel critère qui te fait le plus galérer en ce moment / que tu ne comprends pas ? Tu aurais besoin de quoi pour que ce soit plus fluide pour toi ?
- 😮 C’est quoi le petit changement de pratique qui t’a fait le plus “wow” et qui a changé ta manière de voir la programmation avec R ?

