---
title: Les femmes dans les filières mathématiques
author: anna-doizy
date: '2022-02-10'
slug: les-femmes-dans-les-filieres-mathematiques
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: La réforme du lycée de 2019 a causé quelques dégâts pour les maths.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


C'est tellement dommage, les inégalités liées à la proportion de femmes dans les filières mathématiques étaient presque enfin devenues inexistantes juste avant la réforme de 2019.

![](part-des-filles-en-terminale-S.jpg)

Sources :

- [APMEP - Réforme du lycée et mathématiques, 25 ans de recul sur les inégalités filles/garçons](https://www.apmep.fr/Reforme-du-lycee-et-mathematiques)
- [Femmes et Maths - Réforme du lycée et impact sur les mathématiques](https://femmes-et-maths.fr/2022/02/07/reforme-du-lycee-et-impact-sur-les-mathematiques-part-des-filles-et-nombre-dheures)
