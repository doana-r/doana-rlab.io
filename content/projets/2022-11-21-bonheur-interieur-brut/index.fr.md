---
title: Bonheur intérieur brut
author: anna-doizy
date: '2022-11-21'
slug: bonheur-interieur-brut
categories:
  - Etude de cas
tags:
  - Concevoir
  - Sens
  - Comprendre
  - Transmettre
subtitle: ''
summary: Avec La Raffinerie, nous avons réfléchi collectivement comment mesurer le bien-être du groupe. Comme premier test grandeur nature, nos animatrices du jour, Kat et Anne, ont décidé de proposer un jeu...
authors: []
lastmod: '2023-06-20'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

☀️ Avec le Tiers-Lieu [La Raffinerie](https://www.laraffinerie.re/), nous avons réfléchi collectivement comment mesurer le bien-être du groupe.  
Nous sommes tombés sur la notion de Bonheur Intérieur Brut ou BIB. Pour faire un parallèle avec le PIB, en version moins capitaliste.

🎲 Comme premier test grandeur nature, nos animatrices du jour, Kat et Anne, ont décidé de proposer un jeu au groupe : le jeu des ravitos (points de ravitaillements dans une course de trail, petit clin d'œil au Grand Raid - la diagonale des Fous - qui a eu lieu il y a un mois).
10 tables étaient disposées dans la cour. Chacune représentait un besoin fondamental. Chaque personne devait passer le plus rapidement possible à chaque table pour faire le point sur le remplissage de chacun de ses besoins (en prenant comme référence l'ensemble de la semaine passée).  
Une jauge très peu remplie signifie que le besoin n'est pas assez nourri et inversement.

📊 Ce graphique est la synthèse des résultats à l'échelle du groupe. Chaque point correspond à la réponse (approximative) de chacun des 18 répondants à chaque table.  
L'ordre des besoins (ils sont rangés par médiane), mais aussi, la dispersion des réponses sont intéressants à prendre en compte
Par exemple, pour la liberté : à part 3 personnes, le groupe est très homogène, rempli à 3/4 ou plus !

⚙️ Alors en tant que collectif, il serait intéressant de se demander de quelle manière réagir : prend-on plutôt soin du "groupe" et donc des besoins "bas" (comme la célébration) ou bien prend-on soin des individus les plus démunis (les points les plus à gauche) ?  
L'interprétation du BIB sera plus fine une fois que l'on aura réitéré l'expérience plusieurs fois, pour prendre en compte également l'évolution du groupe dans le temps 🙂

---

Pour aller plus loin :
- [Guide de l'indicateur trimestriel du bonheur des français](https://fr.scribd.com/document/465353829/Guide-de-LITBF) 
- [Le business du bonheur](https://www.arte.tv/fr/videos/099779-000-A/le-business-du-bonheur?fbclid=IwAR0Hxs8dr9-LEKQCi_ia7el_tF7SxcO9ryamXD2lp7H1lQhUrjIEs_aYADk) (lien expiré semblerait-il, désolée)
- [Data science - mesurer le bonheur](https://www.arte.tv/fr/videos/098713-004-A/data-science/)
- [Créez votre propre indicateur du vivre mieux](https://www.oecdbetterlifeindex.org/fr)

