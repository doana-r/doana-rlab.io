---
title: L'art du doute
author: anna-doizy
date: '2022-02-14'
slug: art-du-doute
categories:
  - Brève
tags:
  - Comprendre
  - Sens
subtitle: ''
summary: Je défends l'art du doute et j'ai deux choses à vous partager !
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

**Je défends l'art du doute** (statisticienne et zététicienne) et j'ai deux choses à vous partager :

- Dans une démarche de rationalité, le résultat d'un raisonnement pourri peut être correct, mais c'est quand même un raisonnement pourri, donc le résultat n'est pas valable au sein de cette démarche. 

- Les 2 sources d'erreurs sont : 
  1. la donnée (chiffrée ou non) en entrée 
  2. le raisonnement en lui-même.

Allez voir cette [excellente vidéo d'Hygiène Mentale](https://www.youtube.com/watch?v=VKsekCHBuHI) qui explique cela très bien.

Donc autant dire que la phrase : 

>**"On peut débattre de tout sauf des chiffres"** 

comme argument phare d'une campagne gouvernementale est l'**incarnation même de l'erreur**. C'en est presque beau. J'espère que vous êtes autant émus que moi suite à cette révélation.

***

- [Source gouvernementale](https://www.gouvernement.fr/actualite/vaccination-il-y-a-des-chiffres-qui-piquent-bien-plus-qu-une-aiguille) [**article supprimé**]
- [Vidéo de l'ARS des Pays de La Loire](https://www.youtube.com/watch?v=uUZDWW7V36A) (d'où est issue l'image ci-contre)
- Article avec une explication approfondie des chiffres en question : [CheckNews Covid-19 : est-il vrai que 80% des hospitalisés sont non vaccinés, comme le dit la campagne du gouvernement ?](https://www.liberation.fr/checknews/covid-19-est-il-vrai-que-80-des-hospitalises-sont-non-vaccines-comme-le-dit-la-campagne-du-gouvernement-20210903_LDKZGRV4J5C7DHL67YQL3IPZWA/)


