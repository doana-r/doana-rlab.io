---
title: Atelier démystifier l'entrepreneuriat
author: anna-doizy
date: '2021-12-13'
slug: demystifier-l-entrepreneuriat
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Charlotte, Jennifer, Isabelle et moi-même avons démystifié l'entrepreneuriat lors d'un atelier qui a eu lieu à La Raffineriele 19 novembre ! 
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

- C'est quoi le potentiel entrepreneurial ?  
- Quelles sont les compétences communes aux entrepreneurs ? 
- Ça s'écrit comment entrepreneuriat ? 
- Qu'est-ce qui nous fait peur quand on se lance dans le grand bain de la création d'entreprise ? 
- Où et comment demander de l'aide pour avancer ?

Charlotte, Jennifer, Isabelle et moi-même avons démystifié l'entrepreneuriat lors d'un atelier qui a eu lieu à [La Raffinerie](https://www.laraffinerie.re/) le 19 novembre ! 

J'ai adoré la préparation, les rencontres, les échanges et l'envie d'avancer des participants ! 

Merci.


