---
title: Découper les gâteaux selon Laura et Théo
author: anna-doizy
date: '2022-08-16'
slug: decouper-les-gateaux
categories:
  - Brève
tags:
  - Transmettre
subtitle: ''
summary: Je vous partage une mini-histoire écrite et illustrée par mon amie Laura Hedon. J'aime énormément son univers, alors, en plus, quand elle parle de maths, ça me fait craquer !
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Je vous partage une mini-histoire écrite et illustrée par mon amie Laura Hedon. J'aime énormément son univers, alors, en plus, quand elle parle de maths, ça me fait craquer !

J'adore les représentations visuelles de choses complexes. On peut appeler ça de la facilitation graphique. (préparez-vous, je ne suis pas près d'arrêter de vous en parler)

Dans ce cas, il s'agit de vulgariser la trigonométrie et de lui donner un usage ultra-concret. Je crois que tout le monde peut se reconnaître pour découper un gâteau ! 

![](1.png)
![](2.png)
![](3.png)
![](4.png)
![](5.png)
![](6.png)
![](7.png)
![](8.png)

Je trouve drôle qu'il y ait des chercheurs qui se penchent actuellement sur des problèmes de partage équitable de gâteau !! Allez voir cette vidéo de Science4all :

{{< youtube kefptSDi0Es >}}

Évidemment, dans ces recherches, le partage équitable, c'est un prétexte. Ce qui est vraiment important, c'est bien le gâteau 😂

