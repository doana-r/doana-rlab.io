---
title: Apprendre R avec un peu plus de fun
author: anna-doizy
date: '2024-06-03'
slug: apprendre-r-avec-un-peu-plus-de-fun
categories:
  - Offre
tags:
  - Comprendre
  - Automatiser
  - Transmettre
  - Sens
subtitle: ''
summary: Quel rapport entre une armoire à Lego et des cartes contenant les étapes pour faire une omelette ?
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🤔 Quel rapport entre une armoire à Lego et des cartes contenant les étapes pour faire une omelette ?

⬇️

<br>

<br>

<br>

⬇️

<br>

<br>

<br>

⬇️

<br>

<br>

🎲 Ce sont des jeux pédagogiques que je suis en train de concevoir pour enseigner le langage R d'une manière un peu plus fun que ce que j'ai connu durant mes études !

Je vais les utiliser pour la première fois dans ma prochaine formation de 6 jours répartis sur juin et juillet.

🧱 L'armoire à Lego va permettre de concrétiser les structures de base du langage : les objets. Ça fait 6 ans que je rêve d'en construire une (Frédéric Chiroleu 😉) ! C'est chose faite 🏗️

![](featured.jpg)

🥚 Les cartes omelette vont faire prendre conscience de l'utilité du pipe et de sa capacité à réduire la taille des scripts considérablement 🐣

![](omelette.jpg)


💬 Ça te donne envie d'apprendre en jouant ?




