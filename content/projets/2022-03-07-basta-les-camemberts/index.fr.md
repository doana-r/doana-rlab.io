---
title: Basta les camemberts !
author: anna-doizy
date: '2022-03-07'
lastmod: '2023-09-19T12:06:24+04:00'
slug: basta-les-camemberts
categories:
  - Brève
tags:
  - Transmettre
subtitle: ''
summary: "Le Schtroumpf grognon en moi me souffle souvent : j'aime pas les camemberts !"
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


Le Schtroumpf grognon en moi me souffle souvent "j'aime pas les camemberts !"

Je parle des graphiques et pas de ce met délicat qui embaume les bouches 😉 
Pourquoi ?

L'œil humain est bien meilleur à **détecter des différences entre des longueurs qu'entre des aires et/ou les angles**.

![](camembert-1.jpg)
![](camembert-2.jpg)
![](camembert-3.jpg)
![](camembert-4.jpg)
![](camembert-5.jpg)

Les **bar plots** (ou graphiques en bâtons) sont une alternative possible et recommandée aux camemberts.

Si vous voulez en savoir plus, cet article est très parlant : https://www.data-to-viz.com/caveat/pie.html

***

Source des images indisponible...
