---
title: Ma force de l'ombre
author: anna-doizy
date: '2022-03-28'
slug: ma-force-de-l-ombre
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: J'ai un secret, une super force de l'ombre qui me permet d'incarner l'entrepreneuse que je suis réellement. 
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

J'ai un secret, une super force de l'ombre qui me permet d'incarner l'entrepreneuse que je suis réellement. 

Grâce à ça, j'arrive à décortiquer ma stratégie professionnelle et personnelle.  
Je sais mieux où je veux aller.   
J'ai moins peur.   
J'ai des méthodes.  
J'ai surtout la possibilité de me créer mes propres méthodes d'entrepreneuriat. 

> "Il n'y aucune stratégie miracle. Aucune route claire et tracée."

Chaque semaine depuis un an et demi, je reçois une lettre (ça commence à faire beaucoup de lettres) et à chaque fois, elle éclaire mon métier avec un angle nouveau.

Je n'ai plus peur du marketing : l'art de créer des liens (sains) entre des gens qui ont un problème et une personne qui peut leur donner une solution.  
Je n'ai plus peur de demander de l'argent à mes clients.  
Je n'ai plus peur de perdre des clients parce que j'ai été "trop" moi-même.  
Je n'ai plus peur d'être seule dans mon travail.  
Je n'ai plus peur de demander de l'aide.  
Je n'ai plus peur de mal gérer mon temps. 
Je n'ai (presque) plus peur d'écrire en public sur les réseaux. 

En fait, je n'ai pas peur, parce que maintenant, j'aime ça !

Je ne dis pas que c'est simple. Seulement, pour passer les vitesses et pouvoir avancer, c'est quand même plus agréable de débloquer le frein à main...

Mon secret, il s'appelle [Thomas](https://thomasburbidge.com/) et je n'ai plus envie qu'il soit mon secret. 

J'ai envie de vous le partager.  
Je le connais des cours de théâtre au lycée. Depuis un peu plus de deux ans, il a décidé de donner des conseils aux freelances comme moi, comme vous peut-être. Il a un podcast et une newsletter et il propose des formations et du coaching. 

En fait, il joue le rôle d'un catalyseur (vous savez, ce qui accélère les transformations chimiques) ! Grâce à lui, j'ai pu vivre un tas de très belles transformations plus efficacement et sereinement que j'aurais pu le rêver. Des transformations de l'ombre, mais des transformations essentielles. Et je vous souhaite de tout mon cœur d'en vivre autant vous aussi !


Merci Thomas, cher ami parasocial 😉   
Merci à vous de me lire.

***

Pour commencer à entrer dans le monde de Thomas :
- [De la transparence sur la microentreprise et l'argent](https://thomasburbidge.com/bilan-2021-100-000-euros/)
- Une vidéo sur le marketing lié à la création de contenu

  {{< youtube DkdKAgZ9PWc >}}
  