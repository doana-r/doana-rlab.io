---
title: 20 fois moins de temps
author: anna-doizy
date: '2022-12-01'
slug: 20-fois-moins-de-temps
categories:
  - Etude de cas
tags:
  - Automatiser
subtitle: ''
summary: L'idée reçue que base R est plus efficace en temps de calcul que les fonctions du tidyverse persiste.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🙈 L'idée reçue que base R est plus efficace en temps de calcul que les fonctions du tidyverse persiste.  
Je vois des personnes qui le soutiennent, puis qui vérifient par eux-mêmes et se rendent compte de leur erreur. Et c'est vraiment chouette de tomber sur des publications de personnes qui se remettent joyeusement en question.
Je trouve cela particulièrement inspirant !

🕐 Dans [ce post](https://blog.rwhitedwarf.com/post/comparison_dplyr_vs_base_r/) par exemple , l'auteur Teoten réalise que son code en base R prend 2 fois plus de temps que le même code aromatisé avec une sauce de dplyr et tidyr !

🍋🦠 Pour ma part, une fois, j'ai repris un script de modélisation de la propagation d'une maladie des agrumes dans les vergers (un point sur l'image correspond à une parcelle d'agrumes) à la Réunion, qui avait été écrit en base R et avec des boucles "for".
Je me suis amusée à tout transformer avec les fonctions du tidyverse. J'ai totalement éradiqué les "for". À la fin, le script donnait exactement le même résultat et aucune ligne de code n'était pareille à l'original. Mais surtout, il mettait 20 fois moins de temps pour faire une simulation d'épidémie !  
Par exemple, 3 simulations de 100 jours prenaient environ 12 minutes sur mon ordinateur avec l'ancien script, et plus que 36 secondes avec le nouveau script 🙂

Vous avez déjà remarqué, vous, que ça allait plus vite avec dplyr ? Est-ce qu'il y a des cas particuliers où ça va plus lentement que base R ?

---

Pour info, les résultats issus de mon script de simulation d'épidémie se trouvent dans [la thèse d'Ismaël Houillon](https://agritrop.cirad.fr/602247/).  
Il a également été utilisé pour construire [cette appli](/zagrumes974), à destination des acteurs de la surveillance des maladies de plantes à la Réunion. 

