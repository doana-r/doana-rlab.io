---
title: Data storytelling
author: anna-doizy
date: '2022-01-31'
slug: data-storytelling
categories:
  - Etude de cas
tags:
  - Transmettre
subtitle: ''
summary: Me revoilà sur un de mes thèmes préférés "comment s'attacher à bien représenter ses données". C'est très délicat comme sujet. Je me creuse souvent beaucoup beaucoup la tête pour cette raison avant de publier des résultats. Et puis, bon, j'aime bien mettre les pieds dans le plat, alors je vais utiliser l'exemple de la vaccination COVID-19.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Me revoilà sur un de mes thèmes préférés : **comment s'attacher à bien représenter ses données**.


C'est très délicat comme sujet. Je me creuse souvent beaucoup beaucoup la tête pour cette raison avant de publier des résultats. Et puis, bon, j'aime bien mettre les pieds dans le plat, alors je vais utiliser l'exemple de la vaccination COVID-19.

Une des choses qui fait peur avec le vaccin, c'est que quand on regarde les chiffres, on observe une proportion croissante des personnes vaccinées en service de réanimation. Alors, on peut vite déduire que le vaccin, ça ne marche décidément pas très bien. Bon, mais si on augmente la proportion de personnes vaccinées dans la population globale, alors on l'augmente aussi dans les hôpitaux. C'est purement mathématique. En revanche, il est bien possible que le nombre total de personnes en réanimation soit plus bas que s'il n'y avait eu aucune vaccination. Ce qui est, finalement, plutôt une bonne nouvelle.

Voici la preuve en image par l'excellent "data storyteller" [John Burn-Murdoch](https://twitter.com/jburnmurdoch).

![](data-storytelling-1.jpg)
![](data-storytelling-2.jpg)


**Sources de l'image** : "Right hand scenario based on vaccination rates for people aged 50+ in England, vaccine efficacy from PHE, Right-hand scenario is hypothetical. ©FT"

***

**Mon avis** (et le vôtre aussi d'ailleurs) **sur la vaccination importe peu** ! Ce qui m'importe, c'est justement quels chiffres sont partagés au public, de manière générale. Les taux (ou proportions), font partie des **données dites "agrégées"**. Il y manque l'information du nombre d'observations et les bons moyens de comparaison (dans l'exemple : des nombres rapportés à une population constante). **Nous sommes responsables de l'interprétation de nos lecteurs quand nous leur montrons des données**. Soyons vigilants.

**PS** : Quand je serai grande, j'aimerais faire des graphiques aussi parlants et beaux que celui-ci !





