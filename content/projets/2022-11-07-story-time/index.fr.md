---
title: Story Time
author: anna-doizy
date: '2022-11-07'
slug: story-time
categories:
  - Brève
tags:
  - Sens
  - Transmettre
subtitle: ''
summary: J'ai toujours adoré les histoires. Mon parti pris, c'est de raconter une histoire la plus rationnelle possible et qui suit la démarche scientifique. Mais, quoi qu'il arrive, mon histoire ne sera, ne pourra jamais être une histoire absolument vraie.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


J'ai toujours adoré les histoires.  

Quand j'analyse des données, en fait, je cherche à les interpréter, c'est-à-dire à leur donner un sens. À raconter une histoire avec elles.
Mon parti pris, c'est de raconter une histoire la plus rationnelle possible et qui suit la démarche scientifique.  
Mais, quoi qu'il arrive (au-delà de comment les données ont été récoltés - c'est une autre histoire), je serai biaisée, je ferai des choix, consciemment ou non, je ferai des raccourcis, des omissions... Mon histoire ne sera, ne pourra jamais être une histoire absolument vraie.

⛔ **Arrêtons de croire que les scientifiques sont neutres.**   
Ce n'est qu'en agrégeant et en comparant les histoires des un(e)s et des autres (au sein de ce que l'on appelle des méta-analyses) que l'on peut trouver le sens global/généralisé le plus plausible : ça s'appelle un consensus.  
Tant que le consensus scientifique n'est pas contredit avec des preuves au moins aussi fortes que celles qui ont contribué à obtenir le consensus, alors on le considérera comme étant la "vérité".  
La Terre est ronde et gravite autour du Soleil, jusqu'à preuve du contraire.  
L'homéopathie n'est pas plus efficace qu'un placebo, jusqu'à preuve du contraire.  
La théorie de l'évolution est la théorie, à ce jour, la plus solide pour expliquer le maintien des caractéristiques d'une population au fil des générations, malgré le changement permanent. Cette explication est considérée actuellement comme étant "vraie", jusqu'à preuve du contraire.  
etc.  
etc.

💡 Le statisticien et auteur du livre "Apprentissage automatisé et interprétable", **Christoph Molnar**, s'est mis à raconter des histoires en prenant cette expression au pied de la lettre !  
Il a écrit 3 courtes nouvelles qu'il a situées au début de son ouvrage.  
Elles décrivent des dystopies qui pourraient survenir si l'on ne prend pas garde à l'interprétation des résultats issus des fameux algorithmes qui sont présents partout partout dans notre quotidien.  
Je ne pense pas que les machines devraient faire des choix moraux automatiquement à notre place. L'interprétation des données devrait être prise en charge par des humains. Un ensemble d'humains experts dans le domaine d'application des données en question (par exemple des biologistes), associés à des humains experts des pièges des données (par exemple, des biostatisticiens).

✅ Un algorithme, aussi complexe soit-il, ne reste qu'un OUTIL, parmi tant d'autres, au service des humains qui veulent trouver du sens derrière leurs tableaux de chiffres.  
Je vous présente une des 3 nouvelles ici, traduite approximativement en français.


![](extrait-1.png)
![](extrait-2.png)
![](extrait-3.png)
![](extrait-4.png)
![](extrait-5.png)


L'histoire en version originale, ainsi que les deux autres, sont [disponibles là](https://christophm.github.io/interpretable-ml-book/storytime.html). L'illustration en haut de page provient de ce lien aussi.







