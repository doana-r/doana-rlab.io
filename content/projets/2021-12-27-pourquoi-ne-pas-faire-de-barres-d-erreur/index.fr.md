---
title: Pourquoi ne pas faire de barres d'erreur
author: anna-doizy
date: '2021-12-27'
slug: pourquoi-ne-pas-faire-de-barres-d-erreur
categories:
  - Tutoriel
tags:
  - Transmettre
subtitle: ''
summary: Il y a un an et demi environ, j'écrivais un article pour montrer comment dessiner des barres d'erreur avec ggplot dans R. Je me suis rendue compte que je me suis trompée. Après vous avoir montré comment créer des barres d'erreur, j'ai maintenant envie d'expliquer pourquoi vous ne devriez PAS en faire.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


Il y a un an et demi environ, j'écrivais [un article](/projets/barres-erreur) pour montrer comment dessiner des barres d'erreur avec ggplot dans R.

Et puis, j'ai fait un virage.


Je me suis rendue compte que je me suis trompée. Après vous avoir montré comment créer des barres d'erreur, j'ai maintenant envie d'expliquer pourquoi vous ne devriez PAS en faire.

J'étais en train de me creuser la tête sur comment représenter des données le mieux possible. Alors, je suis allée m'inspirer des grands (sources en bas).

Voici mes conclusions :

- Évitez au maximum de faire des barres d'erreur. La seule exception est lorsque vous vous trouvez en présence de données déjà agrégées (l'intervalle de confiance de paramètres d'un modèle linéaire par exemple). Mais si vous connaissez les **données brutes** (à l'échelle de l'individu statistique), alors ne nous privez pas : regardez et montrez-les !

- Si vous avez plusieurs valeurs par groupe, ne les représentez pas à l'aide d'un graphique en barres (barplot). C'est une erreur très courante. Même avec les barres d'erreur, l'information est cachée. Il existe des types de graphiques bien plus appropriés comme les **diagrammes en boîtes** (boxplot) ou les **diagrammes en violon** (violin plot), combinés avec les points individuels rendus légèrement transparents.

- Une barre d'erreur peut représenter un tas d'objets statistiques différents. J'ai déjà croisé dans la littérature scientifique : l'erreur standard, 2 fois l'erreur standard, l'erreur type, 2 fois l'erreur type, l'intervalle de confiance calculé sur la distribution de Student, l'intervalle de confiance calculé par bootstrap... **Vous n'y comprenez plus rien ?** Ben, vos lecteurs sont dans le même cas !

- N'utilisez **JAMAIS** des intervalles de confiance pour **COMPARER des moyennes** entre elles. (Je vous assure que ça a été une grosse remise en question !) Si vous voulez comparer plus de deux groupes entre eux (par exemple, la hauteur d'arbres en fonction de différents modes d'arrosage), faites des **tests de comparaison multiples** (avec le package `emmeans` mettons) et pas des intervalles de confiance des moyennes ! Ce sont des objets mathématiques distincts, notamment à cause du fait qu'il faut ajuster les valeurs p des tests multiples pour corriger le seuil de significativité.

- "Les autres le font depuis toujours" n'est absolument pas une bonne raison pour ne pas changer. Trouvez un autre argument, je suis open à la critique constructive 😉

***


Sources :
- [ancien article](https://www.doana-r.com/projets/barres-erreur/)
- [erreurs courantes sur les graphiques](https://www.data-to-viz.com/caveats.html) 
- [problème de comparaison multiples](https://en.wikipedia.org/wiki/Multiple_comparisons_problem) 
- [pas d'intervalles de confiance pour comparer les moyennes](https://cran.r-project.org/web/packages/emmeans/vignettes/FAQs.html)

