---
title: Je milite pour le doute sain
author: anna-doizy
date: '2022-09-19'
slug: je-milite-pour-le-doute-sain
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: J'aimerais tant que le monde doute davantage.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Je m'inspire de la démarche rationnelle / scientifique pour développer mon schéma de croyances personnelles.  
Ce cheminement rationnel, c'est un peu ma "croyance zéro". Tout le reste part de ça.  

Je crois ce que je sais.

Évidemment, c'est la théorie et il y a mille-et-une choses que je crois, alors que je n'en sais rien du tout (foi). Et bien d'autres que je sais, mais que je ne veux pas croire (déni).

Mais en général, du moins, lorsque j'en ai conscience, j'essaie de suspendre mon jugement en attendant d'avoir des preuves. Et j'essaie encore plus de ne jamais bloquer mon jugement sur "j'y crois à 100%" ou "je n'y crois pas à 100%".   
Sinon, ça s'appelle le radicalisme / dogmatisme.  
Je pense que le radicalisme tue la bienveillance à cause de ça. On ne peut plus se mettre à la place de l'autre, écouter, réfléchir intelligemment, etc. si on est bloqué à 0 ou 100%. 

Le doute (équilibré) est sain !

J'aimerais tant que le monde doute davantage.  
Quand je dis que le milite pour de la "meilleure science", au fond, c'est ça que je veux dire : **je milite pour le doute sain**.  
Et cela touche finalement une sphère beaucoup beaucoup plus large que seul le milieu scientifique.

Et vous, ça vous parle le doute sain ?


***

J'avais déjà partagé cette image au début de mon activité. Mais je la trouve tellement parlante et d'intérêt publique que je la remets ici !

**Inspirations** : 
- Hygiène Mentale, en particulier [cette vidéo sur la pensée bayésienne](https://www.youtube.com/watch?v=x-2uVNze56s)
- Merci également à [Thomas Burbidge](https://thomasburbidge.com/) pour m'avoir amenée à cette réflexion fondamentale pour la suite de mon activité !

