---
title: Moyennes marginales
author: anna-doizy
date: '2022-10-03'
slug: moyennes-marginales
categories:
  - Tutoriel
tags:
  - Comprendre
subtitle: ''
summary: Dans son dernier article, la biostatisticienne Claire Della explique ce que sont les moyennes marginales.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Je ne suis pas la seule à me pencher sérieusement sur les comparaisons de moyennes ! 📊

Dans [son dernier article](https://delladata.fr/moyennes-marginales/?fbclid=IwAR0Yi9BVAOmNppaf3sgRMHTVdIJr7H1r9UzuxF_t3w3_PxnWwWPLF1q9HNQ), Claire explique ce qu'est :

➡️ la différence entre moyennes marginales et moyennes observées  
➡️ le lien entre la prédiction d'une moyenne marginale et son modèle paramétrique associé (ANOVA, ANCOVA par exemple)  
➡️ la grille de référence

J'ajouterai que (dans le cas d'une ANCOVA) c'est bien parce qu'une variable continue est fixée, par défaut, à sa moyenne dans la grille de référence que l'on NE PEUT PAS utiliser les résultats de la fonction `emmeans()` tels quels lorsqu'il y a des interactions dans le modèle !

MAIS, il y a des petites astuces pour explorer et interpréter correctement les moyennes marginales, même quand il y a des interactions significatives dans le modèle...  
Je vous en parlerai un autre jour !

