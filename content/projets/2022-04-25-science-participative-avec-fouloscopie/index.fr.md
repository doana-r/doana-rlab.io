---
title: Science participative avec Fouloscopie
author: anna-doizy
date: '2022-04-25'
slug: science-participative-avec-fouloscopie
categories:
  - Brève
tags:
  - Transmettre
subtitle: ''
summary: Quoi de mieux que la science participative pour étudier les foules ?
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Quoi de mieux que la **science participative** pour étudier les foules ? 

C'est le pari du chercheur Mehdi Moussaid qui nous parle dans sa chaîne YouTube **Fouloscopie** de sujets aussi variés que : 

- les niveaux de densité de foule avec des tomates, 

{{< youtube Eh7l4Gvx054 >}}

- le succès est-il dû au talent ou à la chance ? 
- Faut-il faire comme tout le monde ou chercher à innover ? 
- Qu'est-ce qu'un test psychologique fiable ? Etc.

Si on s'inscrit sur son site, on peut même participer à ses expériences sur les foules. 

Vous ne vous êtes jamais demandé si une foule joue mieux aux échecs qu'une intelligence artificielle ?  
C'est la question en suspens du moment !

***

Pour en savoir plus : https://www.youtube.com/c/Fouloscopie/videos et https://fouloscopie.com

