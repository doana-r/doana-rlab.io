---
title: La genèse du projet
author: anna-doizy
date: '2023-08-29'
slug: la-genese-du-projet
categories:
  - Offre
tags:
  - Concevoir
  - Sens
subtitle: ''
summary: Tu démarres un nouveau projet de recherche et tu es en train de créer ton protocole expérimental et de réfléchir à comment tu vas saisir tes données. Tu repenses à d'autres projets que tu as suivis de près ou de loin. Et il y a quelque chose qui te chiffonne.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🆕 La genèse du projet.

La phase sur laquelle on passe trop rapidement.

🤔 Tu démarres un nouveau projet de recherche et tu es en train de créer ton protocole expérimental et de réfléchir à comment tu vas saisir tes données. Tu repenses à d'autres projets que tu as suivis de près ou de loin. Et il y a quelque chose qui te chiffonne.

Une sorte de perte de sens générale. Tu te dis peut-être :

À quoi bon ?  
Est-ce que mon étude va servir ?  
Est-ce que je vais pouvoir répondre à mon hypothèse ? Est-ce que j'ai les moyens de récupérer toutes les données dont j'ai besoin ?  
Et puis, de toute façon, il y a des chances pour qu'elle finisse dans un tiroir ou qu'elle soit rejetée par les reviewers. Ça arrive souvent, mine de rien.

😣 Tu te dis tout ça, la motivation en berne. Un nuage de petites voix négatives dans la tête.

🙈 Aurais-tu besoin d'un regard extérieur à ton projet ?

Ce te permettrait de trouver les failles possibles de ton raisonnement, les interactions auxquelles tu n'aurais pas pensé. Ou même une manière de faire à laquelle tu n'aurais jamais pensé, parce que tu ne savais pas qu'on pouvait faire un modèle qui prend en compte certains éléments !

☀️ Un regard extérieur, c'est sain. C'est un peu comme une **"pré-review" technique et statistique** sur la démarche globale de l'étude.

Ça aide à dézoomer, à prendre du recul. À se demander pourquoi on fait ce qu'on fait.
C'est un appui pour sentir, valider que tu suis la méthode scientifique.  
Ça remet en confiance.  
Ça remotive.  
Ça donne des idées. 💡

🗝️ C'est un rôle que j'apprécie particulièrement dans les projets que j'accompagne.

Sauf que.  
J'ai régulièrement un gros problème.  
Le plus gros problème dans mon entreprise.

😨 J'arrive sur un projet déjà en cours et... qui n'est pas très droit, les données capotent un peu. Je ne me sens **pas capable de proposer une réponse à la question d'origine avec les données disponibles**.

Et je me dis à chaque fois que c'est hyper dommage, parce que...
Souvent, seulement avec le protocole en main, avant même la récolte des données, je peux dire en fait si c'est possible de répondre à la question ou pas !

Pas parce que je suis méga forte en protocole. Non. Je ne peux pas faire ça à ta place, je ne suis pas experte dans ton domaine !

🤯 Parce qu'à force, avec les dizaines (centaines ?) de projets que j'ai accompagnés depuis 5 ans, je sais comment faire pour gagner du temps, pour gagner en qualité, pour mettre toutes les chances de ton côté pour que ça fonctionne comme tu l'avais espéré !

🤝 J'accompagne, mais, moi-même, je suis accompagnée.  
Je ne te dirai jamais assez à quel point ça change ma manière de percevoir mes propres problématiques ! Et j'ai vraiment envie de t'apporter ça.

➡️ C'est trop important de **se sentir sur le bon chemin**.

***

Source de l'image : [unsplash.com](https://images.unsplash.com/photo-1581270023016-1b7ea9be8fad?ixlib=rb-4.0.3&q=85&fm=jpg&crop=entropy&cs=srgb&w=6000)

