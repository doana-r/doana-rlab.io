---
title: ggiraph pour mettre de la magie dans les ggplots
author: anna-doizy
date: '2022-03-14'
lastmod: "2023-09-19"
slug: ggiraph
categories:
  - Brève
tags:
  - Transmettre
subtitle: ''
summary: Je vous présente ma dernière trouvaille R ! Les graphiques interactifs sont des graphiques html qui interagissent automatiquement avec l'utilisateur.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


Je vous présente ma dernière trouvaille R !

C'est la girafe, vous savez cet animal qui a le plus gros coeur du monde terrestre, parce qu'il en faut de la puissance pour envoyer le sang tout en haut de long cou.

Mais bref, non, pas du tout, en fait il s'agit du package `ggiraph` pour **ggplot + interactive graph**.

Les graphiques interactifs sont en fait des graphiques html qui interagissent automatiquement avec l'utilisateur. Par exemple, passer la souris sur une partie du graphique la met en évidence et affiche un petit label pour donner davantage d'informations. C'est sacrément utile pour des graphiques complexes, ou bien pour des graphiques à valoriser pour leur donner un rendu très pro, mais encore, pour les intégrer à des sites web (en utilisant shiny par exemple 😉)  !

Je connaissais déjà `plotly` et `echarts4r`, mais malheureusement, je les trouve un peu difficiles à prendre en main et un peu bogués dès que je m'éloigne du but pour lesquels ils ont été prévus.

`ggiraph` est entièrement intégré à la logique de `ggplot2`. 

Pour l'utiliser il suffit  :
- d'ajouter `*_interactive()` à la fin des fonctions de couche `geom_*()` (par exemple `geom_point_interactive()`) et 
- d'indiquer les aesthetics supplémentaire : `tooltip` (pour les labels) et `data_id` (pour les éléments à mettre en valeur).

Pour moi, c'est un peu de la magie, mais après 2 heures de prise en main, ça marche comme je veux. Youhou !

{{< video src="CEO departures.mp4" controls="no" >}}

***

- Site du package : https://www.ardata.fr/ggiraph-book/
- Source de l'illustration : https://www.ardata.fr/ggiraph-book/examples.html
