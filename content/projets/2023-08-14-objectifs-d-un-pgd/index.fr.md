---
title: Objectifs d'un plan de gestion de données
author: anna-doizy
date: '2023-08-14'
slug: objectifs-d-un-PGD
categories:
  - Tutoriel
tags:
  - Concevoir
subtitle: ''
summary: De plus en plus souvent, lors de la phase de demande de subvention d'un projet de recherche, un document particulier est demandé, le Plan de Gestion de Données.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


## A quoi ça sert


📜 De plus en plus souvent, lors de la phase de demande de subvention d'un projet de recherche, un document particulier est demandé : le Plan de Gestion de Données ou PGD de son petit nom.

🤔 Mais qu'est-ce que c'est ?

⚙️ Un plan de gestion de données vise à créer une approche organisée, cohérente et stratégique pour la gestion des données, afin de soutenir les objectifs opérationnels du projet.

😨 Il permet d'identifier les contraintes matérielles de stockage, humaines, techniques liées aux données pour chaque étape du cycle de vie des données.

## Cycle de vie des données

Ce cycle prend en compte plusieurs phases qui peuvent être organisées ainsi :

- ✍️ **Collecte / acquisition** : Les données sont collectées à partir de différentes sources, telles que des capteurs, des formulaires en ligne, des bases de données, etc. Il peut s'agir de données structurées (dans des tableaux ou des bases de données) ou de données non structurées (texte, images, vidéos).

- 💻 **Traitement et Analyse** : Les données sont ensuite traitées et analysées pour en extraire des informations significatives. Cela peut impliquer des opérations telles que le nettoyage des données, l'agrégation, la transformation et l'application d'algorithmes d'analyse pour obtenir des insights.

- 📊 **Utilisation et publication** : Les résultats de l'analyse peuvent être diffusés sous forme de rapports, d'articles de recherche, de tableaux de bord ou d'autres présentations visuelles. Cela permet aux personnes qui n'ont pas réalisé l'expérimentation de comprendre les conclusions tirées des données.

- ♻️ **Archivage et élimination** : Au fil du temps, certaines données peuvent devenir moins pertinentes pour les décisions opérationnelles, mais elles doivent être conservées pour des raisons légales, réglementaires ou historiques. Ces données sont archivées dans des systèmes de stockage appropriés pour une conservation à long terme. Les données obsolètes, inutiles ou non conformes aux politiques de conservation sont quant à elles éliminées de manière sécurisée, conformément aux réglementations en matière de confidentialité et de protection des données.


## Vas-y !


⌛ Le PGD sert pour toute la durée du projet.

Le PGD est évolutif, il est important de le mettre à jour régulièrement !

N'attends pas qu'un formulaire obligatoire te demande de remplir un PGD pour le faire. Plus tu essaieras de te poser ces questions en amont et pendant le projet, plus simple, ce sera de le faire systématiquement ;)

☀️ Et aussi... pour un projet de recherche qui est ponctuel et concerne des données de petite échelle, pas besoin de créer une machine de guerre ! Il s'agit d'adapter les usages à ton contexte.

🌱 Alors va trouver un modèle de PGD et commence maintenant. Ce ne sera pas parfait, et ce n'est pas grave. L'objectif de la recherche, c'est d'apprendre et d'évoluer, non ?





