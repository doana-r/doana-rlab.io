---
title: Aide-moi à t'aider !
author: anna-doizy
date: '2022-08-29'
slug: aide-moi-a-t-aider
categories:
  - Tutoriel
tags:
  - Comprendre
subtitle: ''
summary: Pour aider un autre gentil être humain à m'aider, je lui fournis un exemple minimal reproductible.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Souvent, R, c'est un véritable casse-tête. 

Quand on débute, on ne comprend pas les messages d'erreur, ni la logique, on ne connaît pas les mots-clés pertinents...  
Quand on est plus expérimenté, on trouve des bogues ou bien, on ne parvient pas à faire la chose complexe qu'on a en tête.

Personnellement, je commence par chercher des réponses sur internet, il y a beaucoup beaucoup beaucoup de contenu gratuit dans des blogs (par exemple, [delladata.fr](https://delladata.fr/blog/) qui est en français), des forums ([stackoverflow](https://stackoverflow.com/search?q=%5Btag%5D+r&s=c2a65204-49d1-4166-92d3-cf4278337ffc)), sur les sites de dépôt de code libre (https://github.com/ et https://gitlab.com/), dans des livres en ligne (Va voir le [Big Book of R](https://www.bigbookofr.com/))...

Mais parfois, ce n'est pas assez, j'ai besoin d'un autre être humain et de formuler moi-même ma propre question.  

Pour aider l'autre gentil être humain à m'aider, je lui fournis un **exemple minimal reproductible** ou **ReprEx**.
Ce schéma explique ce que c'est et pourquoi c'est important.

![](featured.png)

***

Pour aller plus loin :


<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/chercher-de-l-aide-1/">
    <i class="fas fa-plus"></i>
    Chercher de l'aide (1)
  </a>
  
  <a class="btn btn-outline-primary" href="/projets/chercher-de-l-aide-2/">
    <i class="fas fa-plus"></i>
    Chercher de l'aide (2)
  </a>
</div>





