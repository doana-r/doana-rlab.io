---
title: IPSIM-cirsium
author: anna-doizy
date: 2021-11-08
lastmod: 2022-02-28
slug: ipsimcirsium
tags:
  - Transmettre
categories:
  - App Shiny
  
subtitle: ''
summary: 'Application shiny pour modéliser les dégâts du chardon commun dans les grandes cultures'


links:
- name: "App"
  url: https://shiny.biosp.inrae.fr/app/ipsimcirsium
- name: "Code"
  url: https://gitlab.com/doana-r/ipsimcirsium

---

## 11/08/2021

Je célèbre avec vous la sortie de la version bêta de IPSIM-cirsium !

IPSIM-cirsium est à la fois le nom d'un modèle créé par INRAE, le nom d'un package de R (disponible sur gitlab) et un site web à destination des agriculteurs principalement.

> Késako ?

L'INRAE a créé un modèle de prédiction des infestations de chardon commun dans les grandes cultures en Europe.
J'ai créé une interface web (à l'aide de l'outil R Shiny) qui valorise les résultats de ce modèle. Pour utiliser ce modèle dans son propre ordinateur, il suffit de télécharger le package de R.


<iframe class="shiny-iframe" src="https://shiny.biosp.inrae.fr/app_direct/ipsimcirsium/?lang=fr" frameborder="no"></iframe>


Lien vers l'application (full screen) : https://shiny.biosp.inrae.fr/app/ipsimcirsium


Vous remarquerez qu'il y a un air de famille avec [IPSIM-chayote](/projets/ipsim-chayote/) que j'ai créé pendant que je travaillais au CIRAD. En effet, c'est la suite du projet !

![](andert-1.jpg)
*Photo © Sabine Andert*

***

## Mise à jour du 28/02/2022

J'ai développé IPSIM cirsium, une application web pour comprendre et diminuer la propagation du chardon dans les champs des agriculteurs en Europe de l'Ouest, en partenariat avec [INRAE](https://www.inrae.fr/). Ce travail vient d'être publié dans la revue française Phytoma, spécialisée en protection des plantes.

L'accès à l'article est payant, mais j'ai le droit de le transmettre sur simple demande de votre part, donc n'hésitez pas 😉

![](titre-phytoma.jpg)

<div class="btn-links">
  <a class="btn btn-outline-primary" href="/publication/robin2022oad/">
    <i class="fas fa-plus"></i>
    En savoir plus
  </a>
</div>
