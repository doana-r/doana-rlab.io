---
date: "2021-02-09"
lastmod: "2021-03-15"
title: Butinage Péi
summary: Application web de sciences participatives pour explorer le butinage des abeilles à la Réunion.
slug: butinagepei

external_link: ""

image:
  caption: ""
  focal_point: Smart

links:
- name: "App"
  url: https://butinagepei.doana-r.com/
- name: "Code"
  url: https://gitlab.com/doana-r/butinagepei
# - name: "Publication"
#   url: /publication/doizy2020phylostems/

tags:
  - Transmettre
categories:
  - App Shiny

---


**Butinage Péi** est une application permettant d'explorer les plantes butinées par les abeilles sur l'île de la Réunion.
Elle est développée pour le [GDS Réunion](https://www.gds974.com/).

Ces données participatives proviennent d'un [groupe facebook](https://www.facebook.com/groups/375179256507926/?multi_permalinks=571229623569554%2C570660930293090%2C570660510293132%2C570658770293306&notif_id=1594800619596841&notif_t=group_activity&ref=notif) où ses membres peuvent apporter leurs observations d'abeilles avec les informations suivantes :

- l'espèce de plante qu'elles butinent avec une photo pour vérifier l'identification de la plante,
- le lieu et la date de l'événement.


Le développement de cette application en est encore au stade expérimental. Son utilisation et les données employées sont susceptibles de varier.





Allez la voir à cette adresse : https://butinagepei.doana-r.com/.
Son code source est [ici](https://gitlab.com/doana-r/butinagepei).


<iframe class="shiny-iframe" src="https://butinagepei.doana-r.com/" frameborder="no"></iframe>

