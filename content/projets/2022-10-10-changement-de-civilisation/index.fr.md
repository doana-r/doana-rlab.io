---
title: Changement de civilisation
author: anna-doizy
date: '2022-10-10'
slug: changement-de-civilisation
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Je suis en train de comprendre pourquoi j'ai tant de mal à parler d'écologie alors que c'est au cœur de mon métier de tous les jours.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


Je suis en train de comprendre pourquoi j'ai tant de mal à parler d'écologie alors que c'est au cœur de mon métier de tous les jours.

C'est que j'ai envie de dire des choses vraies, j'ai envie de dire des choses qui ont un réel impact. Je ne veux pas brasser du vent.  
Et dans beaucoup de discours actuels sur l'écologie, il y a (vraiment) beaucoup de vent.

En comparaison, c'est si simple de dire des choses exactes quand on parle de maths !  
C'est si simple de dire des choses qui ont de l'impact quand il s'agit de mes propres sentiments !

Mais quand il s'agit de la société entière - parce que je crois que l'écologie et la société sont intriquement liés - comment avoir un discours exact et porteur de sens ?

Ce que je ressens, c'est que toutes les solutions "magiques" et technologiques qu'on nous propose, ben, elles ne vont pas marcher à elles seules. "Sauver la planète" est fondamentalement incompatible avec la "croissance économique" et le "développement durable".

Je suis en train de faire le deuil de notre société actuelle et de notre système de croyance global - le "rêve de la société" diront les Toltèques.  
Je pèse mes mots.  

Je me demande sincèrement et profondément comment je pourrais faire pour que mon projet DoAna soit cohérent avec le changement de société que je pressens et que je veux accompagner.

Comment faire pour que mes valeurs et ma vision propres soient alignés avec les valeurs et la vision de la société à venir ?

Je n'ai pas de réponse pour le moment.

Juste un indice, un début de fil rouge peut-être : le doute sain, la bienveillance (envers les autres et envers soi-même) et une nouvelle civilisation.


***


**Inspirations** : 

- L'essai "Une autre fin du monde est possible" de Pablo Servigne, Raphaël Stevens et Gauthier Chapelle
- La conférence "Faire face dignement aux risques systémiques du XXIe siècle" d'Arthur Keller

{{< youtube A3f9Lbh5sNM >}}
