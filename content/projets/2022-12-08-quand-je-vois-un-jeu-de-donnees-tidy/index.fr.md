---
title: Quand je vois un jeu de données tidy...
author: anna-doizy
date: '2022-12-08'
slug: quand-je-vois-un-jeu-de-donnees-tidy
categories:
  - Brève
tags:
  - Concevoir
subtitle: ''
summary: 'documenté, et accompagné de son protocole expérimental'
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

La tête que je fais quand je vois un jeu de données tidy, documenté et accompagné de son protocole expérimental !

Allez, je vous promets que c'est possible 🙂

© photo de ma maman

