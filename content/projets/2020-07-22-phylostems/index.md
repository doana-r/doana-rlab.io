---
date: "2020-07-22"
title: phylostems
summary: Application web pour explorer le signal temporel à différentes échelles d'évolution dans des ensembles de séquences ADN hétérochrones.
slug: phylostems

external_link: ""

image:
  caption: ""
  focal_point: Smart

links:
- name: "App"
  url: https://pvbmt-apps.cirad.fr/apps/phylostems/
- name: "Code"
  url: https://gitlab.com/cirad-apps/phylostems
- name: "Publication"
  url: /publication/doizy2020phylostems/

tags:
  - Transmettre
categories:
  - App Shiny

---


**phylostems** est une application développée pour le CIRAD.
Elle a pour objectif d'explorer un arbre de données phylogénétiques hétérochrones (dont les branches/échantillons n'ont pas le même âge).
Elle permet de détecter la présence de signal temporel (corrélation positive entre la date d'échantillonnage et la distance génétique) à chaque noeud de l'arbre.


Allez la voir à cette adresse : https://pvbmt-apps.cirad.fr/apps/phylostems/ (*in english*).
Son code source est [ici](https://gitlab.com/cirad-apps/phylostems).



<iframe class="shiny-iframe" src="https://pvbmt-apps.cirad.fr/apps/phylostems/" frameborder="no"></iframe>


