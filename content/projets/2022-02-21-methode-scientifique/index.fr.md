---
title: Méthode scientifique
author: anna-doizy
date: '2022-02-21'
slug: methode-scientifique
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Il parait que je suis une femme de méthodes. J'adore m'organiser pour fluidifier un enchaînement d'action. Expérimenter des méthodes existantes, me les approprier, les modifier éventuellement, les transmettre.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---






Il parait que je suis une femme de méthodes. J'adore m'organiser pour fluidifier un enchaînement d'action. **Expérimenter des méthodes existantes, me les approprier, les modifier éventuellement, les transmettre**. Une part de moi est (étrangement) profondément satisfaite de ce cycle. Seulement, la méthode pour la méthode ne me conviendrait guère. **La méthode sert un objectif, une vision**. C'est un chemin. Un chemin qui n'est pas linéaire. Je souhaite simplement que les courbes de mon chemin soient belles et harmonieuses.

> "Les méthodes ne sont pas des solutions miracles et doivent être employées avec humilité. Préférons l’absence de méthode à une méthode mal employée ! Car ce sont ces mauvaises utilisations qui finissent par décrédibiliser l’ensemble du domaine."  

Jules Zimmermann, formateur, conférencier et enseignant en créativité



Pour mon métier, je vois les statistiques, la programmation, la modélisation, la CNV, la zététique, la science, la reproductibilité comme des méthodes (et j'en oublie des tas). **J'ai une formidable boîte à outil, qui s'enrichit sans cesse.** Je vous vois si effrayés ou interloqués ou légèrement taquins quand je vous présente ma belle boîte à outil 🙂 Comprenez-moi bien, je ne fais pas *des stats pour faire des stats*. Je les utilise pour aider les chercheurs à mieux comprendre le monde. **Avec de belles méthodes, nous pourrons valider de belles découvertes.**


***

- Article d'où est issue la citation : [La créativité est-elle une question de méthodes ?](https://medium.com/le-labo-des-id%C3%A9es/la-cr%C3%A9ativit%C3%A9-est-elle-une-question-de-m%C3%A9thodes-2f61d261d898)
- [Source de l'illustration](https://fr.wikipedia.org/wiki/M%C3%A9thode_scientifique)

