---
title: 2 ans
author: anna-doizy
date: '2022-04-04'
slug: 2-ans
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: ''
authors: []
lastmod: '2023-08-29T11:46:13+04:00'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Le 1er avril, DoAna a fêté ses 2 ans !

Le 1er avril, c'est la fête de l'esprit critique, parce que c'est un jour où l'on se préoccupe un peu plus de la source et de la qualité des informations que l'on capte.

Alors, ça me plaît assez de célébrer l'anniversaire de mon entreprise en même temps que l'on célèbre ce pour quoi elle est là.

Merci à tous ceux qui contribuent, de près ou de loin, à cette aventure !

***

Illustration : Hergé, évidemment.
