---
date: "2020-06-01"
lastmod: "2021-02-11"
title: Initiation à R
summary: Contenu d'une formation d'initiation à `R` et aux statistiques.
slug: "initiation-r"

external_link: "https://initiation.doana-r.com"

image:
  caption: ""
  focal_point: Smart

tags:
  - Comprendre
  - Transmettre
  - Automatiser
categories:
  - Offre
---





