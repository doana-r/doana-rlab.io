---
title: Pourquoi mes supports de formation sont libres d'accès
author: anna-doizy
date: '2023-04-05'
slug: support-formation-libre
categories:
  - Brève
tags:
  - Sens
  - Transmettre
subtitle: ''
summary: "Pour beaucoup de formateurs/formatrices, les supports sont ce qu'il y a de plus précieux et c'est très important de les garder le plus secret possible."
authors: []
lastmod: '2023-06-15T11:45:15+04:00'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

N'importe qui peut avoir accès à mes supports de formation.

🗝️ Pour beaucoup de formateurs/formatrices, les supports sont ce qu'il y a de plus précieux et c'est très important de les garder le plus secret possible. Je pense que c'est de peur que d'autres formateurs/formatrices s'en emparent et les utilisent pour leurs propres formations et qu'ils en perdent des contrats.

Après une réflexion intense, encouragée par les confinements de 2020, je suis parvenue aux conclusions suivantes, qui n'engagent que moi (et qui, en réalité, ne sont pas figées) :

- ✅ c'est important que les participant(e)s à mes formations aient un accès aux supports de la formation à laquelle ils/elles ont participé, et surtout que ces supports soient à jour.
- 🆓 c'est facile et gratuit de les mettre en ligne directement, sans avoir à créer un site avec une connexion sécurisé.
- 🔄 90% de mes connaissances de programmation avec R, informatiques et statistiques proviennent de supports open source, gratuits, accessibles sur le web. Quelle honte ce serait de ne pas rendre la pareille !
- 🎲 ma plus grande valeur ajoutée ne réside pas dans mes supports, mais dans mes qualités d'animation. Les personnes autodidactes qui veulent et peuvent se former seules ont déjà l'embarra du choix sur le net ! Celles qui viennent me voir, ce sont celles qui ont besoin d'un coup de pouce d'un humain pour les lancer dans leur apprentissage et les rassurer sur leurs capacités à comprendre ces nouvelles notions. J'utilise la pédagogie active, ludique, participative pour enseigner et mes supports ne sont qu'un outil parmi d'autres pour délivrer ma formation et lui donner une mémoire.
- 🎁 même si on réutilise (attention, c'est illégal de ne pas me citer) ou qu'on s'inspire de mes supports (allez-y !), ça me fait plaisir. Une des missions de mon entreprise DoAna, c'est de contribuer à faire de la science fiable, reproductible et éthique. Si mes supports peuvent essaimer pour aider à ça. C'est parfait !

Et toi ? Tu en penses quoi de tout ça ?

- 👩‍🏫 Si tu es dans l'animation, est-ce que ça te parle ? Quels sont tes supports et comment les amènes-tu à tes participant(e)s ?
- 👩‍🎓 Si tu as suivi des formations récemment, comment vois-tu les choses ? Tu es plutôt dans la team autodidacte ou dans la team "coup de pouce humain" ?
- 👩‍💻 Si tu veux accéder à mes supports, que tu ne sais pas trop où chercher, ou que tu ne comprends pas ma logique de rangement (qui va sûrement évoluer d'ici à quelques mois d'ailleurs), n'hésite pas à m'envoyer un petit mot.




