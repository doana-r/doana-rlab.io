---
title: Chercher de l'aide (1)
author: anna-doizy
date: '2022-01-17'
slug: chercher-de-l-aide-1
categories:
  - Tutoriel
tags:
  - Comprendre
  - Automatiser
subtitle: ''
summary: Comment demander de l'aide à Internet quand rien ne va plus ?
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


J'ouvre R. Mmmm, je sais ce que je vais faire !  
Arf, je pensais que comme ça, ça marcherait...  
Ok ok, on se calme, je recommence à 0.  
Ahhhh, mais pourquoi, POURQUOI, ça ne marche paaaaaas !  
**crise de larmes**.  
Mort apparente.  
TING, mais j'ai une idéééée !  
*"You gonna go far kid!"* Youpi 🙂

Comment demander de l'aide à Internet quand rien ne va plus ?

Je vous conseille de chercher en anglais, vous aurez davantage de résultats. Plus vous ferez de recherches, meilleur sera votre vocabulaire technique et meilleure sera la précision de vos questions.

Ajoutez "in R" à la fin ou, encore mieux, "in [nom du package] R package". Parce que "R" tout seul est difficile à gérer pour les moteurs de recherches (il faut bien UN inconvénient à ce langage).

Ça donne, par exemple :  "how to [truc de manipulation de données] in dplyr", ou "how to [truc pour faire un graphique] in ggplot".

En faisant ça, vous tomberez le plus souvent sur :

- la documentation du CRAN (qui héberge les packages officiels de R) : concerne le cas général et, parfois, légèrement obscure (vous pouvez aussi y accéder directement dans R en tapant : ?nomdelafonction)
- des forums de la communauté R dont le super [Stackoverflow](https://stackoverflow.com/questions/tagged/r) : rassurez-vous vous n'êtes pas le premier à vous être posé cette question !
- les dépôts Github des packages en question : questions plus techniques, souvent en rapport avec les bugs dans les packages. Le README (allez en bas de page ; sous les fichiers du dépôt) peut néanmoins contenir et/ou mener vers de la documentation précise et agréable à lire.

***

Inspiration : [Oscar Baruffa](https://oscarbaruffa.com/rstuff/)  
Illustration : [Allison Horst](https://github.com/allisonhorst/stats-illustrations)



<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/chercher-de-l-aide-2/">
    <i class="fas fa-plus"></i>
    Chercher de l'aide (2)
  </a>
  
  <a class="btn btn-outline-primary" href="/projets/aide-moi-a-t-aider/">
    <i class="fas fa-plus"></i>
    Aide-moi à t'aider !
  </a>
</div>


***
