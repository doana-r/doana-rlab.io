---
title: Installer et mettre à jour R
author: anna-doizy
date: '2024-05-24'
slug: installer-et-mettre-jour-r
categories:
  - Tutoriel
tags:
  - Comprendre
subtitle: ''
summary: Un tuto pour savoir 1/ comment installer R, si c'est la première fois et 2/ comment mettre à jour... tout ce qu'il faut mettre à jour.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Lors de mes formations d'initiation à R, le point le plus délicat, ce n'est pas :

- ❎ faire ses premiers pas en programmation  
- ❎ gérer les messages d'erreur  
- ❎ comprendre comment traduire ses idées dans le langage de l'ordinateur  
- ❎ faire un graphique ultra-stylé  

Non... le plus dur, c'est souvent d'installer le logiciel.  
Ou plutôt... LES logiciels ! 🙈

Ci-contre un tuto pour savoir

- 👉 comment installer R, si c'est la première fois
- 👉 comment mettre à jour... tout ce qu'il faut mettre à jour

✨ Je te souhaite l'expérience la plus fluide possible !  
👑 Accroche-toi, la première marche est un peu grande, mais ça vaut le coup


⬇️⬇️⬇️

![](1.png)
![](2-fond_R.png)
![](3-fond_Windows.png)
![](4-forme_RStudio.png)
![](5.png)
![](6-numéros_version.png)
![](7-mises_a_jour.png)
![](8-gestion_packages.png)

