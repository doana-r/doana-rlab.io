---
date: "2021-05-04"
title: Plausible analytics
summary: Quelles sont les stats de ce site ?
subtitle: Quelles sont les stats de ce site ?
slug: plausible

external_link: ""

image:
  caption: ""
  focal_point: Smart

# links:
# - name: "Code"
#   url: https://gitlab.com/cirad-apps/tree-grass
# - name: "Publication"
#   url: /publication/djeumen2020usefulness/

tags:
  - Sens
categories:
  - Open-source

---


C'est courant, lorsque l'on créé un site internet, d'aller chercher quelques statistiques sur les visites du site.
L'outil gratuit le plus en vogue c'est g**gle analytics.
Comme j'aime bien les logiciels libres/open-source, j'ai cherché une alternative.
Matomo commence a être connu, j'ai testé et ça fait très bien le taf.

Mais...

Je ne sais plus comment, [Plausible](https://plausible.io) est arrivé à ma connaissance.
Je l'aime beaucoup parce que c'est fichtrement plus simple que les 2 autres concurrents dont je viens de parler. 
Alors oui, c'est sûr, il y a plein de fonctionnalités en moins, mais personnellement, ce sont celles dont je ne me servirai pas.

Le **vrai plus**, c'est que Plausible est en conformité avec les [RGPD](https://fr.wikipedia.org/wiki/R%C3%A8glement_g%C3%A9n%C3%A9ral_sur_la_protection_des_donn%C3%A9es) (Règlement général sur la protection des données) : il respecte les données à caractère personnel selon la loi européenne. Et ça, moi, ça me tient beaucoup à coeur. Pas besoin de faire des tas de réglages compliqués pour anonymiser les adresses IP etc. (comme chacun devrait faire dans g**gle analytics et compagnie, mais évidemment ce n'est pas le cas). Tout est prêt à l'emploi, pas besoin de mettre un bandeau *cookies*. Rien. Nada.

Les deux créateurs sont basés à Londres et ils sont réactifs (oui, je suis la relou qui pose des questions 😉).
Et ça me plaît aussi, l'idée d'aider une petite équipe qui se lance.

Bref, une image vaut bien mille mots. 
Regardez plutôt ici l'ampleur de mon hégémonie dans le monde (et accessoirement la classe de cet outil) :




<iframe plausible-embed src="https://plausible.io/share/doana-r.com?auth=0QLFnPx82AHqmKcfF9W0i&embed=true&theme=dark&background=transparent" scrolling="no" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 1600px;"></iframe>

<script async src="https://plausible.io/js/embed.host.js"></script>

