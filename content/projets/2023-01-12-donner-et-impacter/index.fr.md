---
title: Donner et impacter
author: anna-doizy
date: '2023-01-12'
slug: donner-et-impacter
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: J'ai choisi de donner 1% de mon chiffre d'affaires de 2022 à R Foundation, la Société Française de Statistique et Time for the Planet.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

J'ai choisi de donner 1% de mon chiffre d'affaires de 2022 à [R Foundation](https://www.r-project.org), la [Société Française de Statistique](https://www.sfds.asso.fr/) et [Time for the Planet](https://www.time-planet.com/).

- ✅ R, c'est un logiciel open source avec une équipe et des contributeurs qui le maintiennent et le font évoluer bénévolement. C'est quand même le logiciel qui me fait vivre depuis presque 3 ans (5 ans, si on compte ma période en entreprise).
- ✅ La SFDS promeut les réflexions dans le domaine de la statistique et de la science des données. Elle a l'intention d'agir pour l'environnement pour 2023.
- ✅ Time for the Planet est un collectif d'associés encourageant la création de solutions technologiques pour l'écologie.

🔀 Je suis bien consciente que ce ne sont pas des solutions qui suffisent, mais je choisis de mettre ma petite part dans la machine. Je visualise l'argent comme un flux et que cet argent que je récupère, j'ai le choix de l'endroit où je le redirige. C'est un pouvoir très fort quand on réalise qu'on le possède bel et bien.

💶 Cette année, j'ai choisi de donner mon loyer à une propriétaire plus aimable, j'ai choisi de faire le tour de métropole pour m'agrandir la tête, j'ai choisi d'acheter mes aliments, mes loisirs, etc. le plus local possible, j'ai choisi de faire des cadeaux aux gens que j'aime (moi comprise) et aux causes qui me tiennent à cœur.

☀️ J'ai de la gratitude d'avoir ce choix en fait. Je sais que ce n'est pas possible pour beaucoup de monde. Et pour ceux qui le peuvent, j'espère que ma manière de voir les choses les inspirera.

🙏 Merci à [Thomas Burbidge](https://thomasburbidge.com/) qui a contribué à arroser cette petite graine dans ma tête ! (L'impact que tu as sur moi m'encourage à avoir confiance dans l'impact que moi-même, je pourrais avoir sur les gens qui me suivent)

