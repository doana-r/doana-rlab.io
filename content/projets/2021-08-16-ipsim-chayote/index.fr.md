---
title: IPSIM-chayote
author: anna-doizy
date: '2021-08-16'
slug: ipsim-chayote

tags:
  - Transmettre
categories:
  - App Shiny
  
subtitle: ''
summary: 'Application shiny pour modéliser les dégâts de la mouche des fruits sur le chouchou à la Réunion'
lastmod: '2021-08-16'

links:
- name: "App"
  url: https://pvbmt-apps.cirad.fr/apps/ipsim-chayote/
- name: "Code"
  url: https://gitlab.com/cirad-apps/ipsim-chayote
- name: "Publication"
  url: /publication/deguine2020ipsim/
---


Imaginez. 

Vous êtes un agriculteur à Salazie (un des trois cirques de l'île de la Réunion). Vous faites pousser du chouchou (ou chayote). Malheureusement, il y a des mouches (invasives) qui n'arrêtent pas de pondre dans vos légumes et ça ruine votre récolte. C'est bon, vous y êtes ? Et là, bim ! Voici IPSIM-chayote : le modèle scientifique qui va vous permettre de pointer les améliorations possibles dans vos pratiques d'agriculteur pour endiguer la présence de cette mouche !


Est-ce que vous allez jouer le jeu ? Il suffit de répondre au questionnaire à l'onglet "Vos données" (c'est plus sympa sur un grand écran ; vos données ne sont pas enregistrées)  :


<iframe class="shiny-iframe" src="https://pvbmt-apps.cirad.fr/apps/ipsim-chayote/" frameborder="no"></iframe>


Lien vers l'application (full screen) : https://pvbmt-apps.cirad.fr/apps/ipsim-chayote/

La publication scientifique se trouve là : https://www.sciencedirect.com/science/article/abs/pii/S0261219420303008?dgcid=coauthor


La création et la calibration du modèle sont du fait de mes co-auteurs. En revanche, j'ai participé, avec l'aide d'Isaure Paitard lorsque je travaillais encore au CIRAD, à la création de l'application web pour valoriser les résultats du modèle et le rendre accessible au plus grand nombre. Ceci a été fait grâce à l'excellent package R de RStudio : **shiny**. Il permet de développer des interfaces web (avec un côté serveur) sans autre connaissance prérequise que celle de R ! Quel progrès pour les chercheurs, la valorisation des données (bien plus sexy qu'au moyen d'un article) est à portée de main :)


> PS : Mon interface est largement améliorable (en particulier, le côté responsive pour l'adapter aux petits écrans), je suis en train de continuer mon entraînement avec le chardon commun. Affaire à suivre...


