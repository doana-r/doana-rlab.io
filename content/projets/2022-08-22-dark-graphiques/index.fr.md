---
title: Dark graphiques
author: anna-doizy
date: '2022-08-22'
slug: dark-graphiques
categories:
  - Etude de cas
  - Tutoriel
tags:
  - Transmettre
subtitle: ''
summary: Analyse de 3 enquêtes qui ont été réalisées par le GDS Reunion auprès des apiculteurs de la Réunion
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

J'ai testé les dark graphiques !

En général, les graphiques ont un fond blanc. Je pense qu'à l'origine, c'est une question d'économie d'encre pour les impressions. Et c'est devenu la tradition.

Entre nous, si les graphiques ne sont destinés qu'aux écrans, alors pourquoi se limiter ? D'autant plus que les fonds noirs sont plus reposants pour les yeux.  
Si, si, vous n'avez pas encore mis un fond noir dans RStudio ???

Pour info, il s'agit de l'analyse de 3 enquêtes qui ont été réalisées par le GDS Reunion auprès des apiculteurs de la Réunion. Si vous y connaissez un peu en abeilles, vous y trouverez des petites infos plutôt intéressantes 😉

J'ai volontairement utilisé des formats graphiques peu utilisés auprès du grand public. Je tenais particulièrement à faire découvrir d'autres types de graphiques que les graphiques en barre, les camemberts (au secours) ou les box plot. Alors, c'est totalement normal si vous mettez un peu de temps à les décrypter. Je ne suis pas désolée haha

La dernière chose que je voulais dire, c'est que dans mon activité, je distingue 2 sortes de graphiques :
- Les graphiques de travail qui sont des brouillons et permettent d'explorer les données ou les résultats des modèles stats
- Les graphiques de publication qui sont triés sur le volet, qui sont terminés, qui donnent envie qu'on les lise, qui racontent une histoire.

Il est important de se rendre compte que des dizaines de graphiques de travail et pas mal d'heures sont nécessaires à l'obtention d'un graphique de publication.  

Merci au [GDS Réunion](https://www.gds974.com/) et en particulier à Olivier Esnault et Rachel Abbas pour l'autorisation d'utilisation et de publication des données !


![](01bis-statut-reponses.png)
![](02-nourrissement.png)
![](03-traitements.png)
![](03bis-traitements.png)
![](04-colonies.png)
![](05-colonies-miellee.png)
![](06-production-vs-colonies.png)
![](07-production-miel.png)
![](08-prix-miel.png)

***

Source de "les fonds noirs sont plus reposants pour les yeux" :

- [Effects of Dark Mode Graphics on Visual Acuity and Fatigue with Virtual Reality Head-Mounted Displays](https://ieeexplore.ieee.org/abstract/document/9089640?fbclid=IwAR2tVdWzV4iixx8tGn5hn7NALEkcEsnbkkR1J_0-xPjHaew2BA2v-7R9gGg)
- [Effects of Dark Mode on Visual Fatigue and Acuity in Optical See-Through Head-Mounted Displays](https://dl.acm.org/doi/abs/10.1145/3357251.3357584?fbclid=IwAR0fTIY87vnofHFoLfyRaTr1qHx6mwznihPy4sQ6VixJOzfPBWsZatlNSpU)