---
title: Conférences enregistrées
author: anna-doizy
date: '2023-11-16'
slug: conferences-enregistrees
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Mes deux conférences d'octobre ont été enregistrées. Alors, déjà, c'est compliqué de parler devant mon écran à une assemblée réunie en présentiel... que je ne vois pas. Mais en plus...
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Mes deux conférences d'octobre ont été enregistrées.

<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/rencontrez-moi-en-octobre/">
    <i class="fas fa-chevron-right"></i>
    J'en parle ici
  </a>
</div>

<br>

Alors, déjà, c'est compliqué de parler devant mon écran à une assemblée réunie en présentiel... que je ne vois pas.

Mais en plus, se ré-entendre et se voir, et se rendre compte qu'il y a eu de gros bugs de connexion. 

Mama...

Pfff j'ai eu envie de me mettre dans une grotte.

Je suis allée dans ma grotte. 🙈

Et en même temps, je me dis que c'est peut-être un tout petit peu intéressant ce que j'ai raconté. Ça parle juste de ce qui me porte professionnellement depuis 5 ans. C'est tout 😂

Allez, je lâche un peu de perfectionnisme et me serre très fort contre ma confiance et je te les montre ces conférences !

*moi qui mets une papatte dehors*

<div class="tenor-gif-embed" data-postid="25755309" data-share-method="host" data-aspect-ratio="1.81818" data-width="100%"><a href="https://tenor.com/view/ours-blanc-meme-bonjour-polaire-polar-bear-gif-25755309">Ours Blanc Meme GIF</a>from <a href="https://tenor.com/search/ours+blanc-gifs">Ours Blanc GIFs</a></div> <script type="text/javascript" async src="https://tenor.com/embed.js"></script>

<br>

- [Prendre soin du début de l'aventure](https://www.doana-r.com/projets/prendre-soin-du-debut-de-l-aventure/) (j'ai ajouté un audio de ma présentation !)

- [Freelance et R-lady : un duo gagnant pour la démarche expérimentale](https://youtu.be/6qKE9NkN-Uc?t=438) de 7'20 à 24'00

***

**BONUS** : Retour de Mouna Belaid, organisatrice de l'événement des R-ladies dans un [article de blog de R consortium](https://www.r-consortium.org/blog/2023/11/16/highlights-from-r-ladies-paris-hybrid-meetup-empowering-community-outreach)

> Following the warm introduction to our community, Anna Doizy started her presentation virtually, through the discussion about her scientific projects and her journey as a freelancer for the past three and a half years. She shared her transition from the role of a “biostatistician” to the one of a “scientific consultant” and delved into the challenges she faced while establishing her own company. Anna’s talk also covered her valuable contributions in helping researchers to enhance their experimental approaches.
>
> Her talk was truly impressive.


