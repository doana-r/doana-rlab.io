---
title: Qualité scientifique - 1/4
author: anna-doizy
date: '2023-09-07'
slug: qualite-scientifique-1
categories:
  - Tutoriel
tags:
  - Comprendre
  - Concevoir
  - Sens
  - Transmettre
subtitle: ''
summary: C'est quoi le plus important dans la science pour toi ? Prends deux minutes pour y songer. 
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

## ✍️ La reproductibilité ✍️

> C'est quoi le plus important dans la science pour toi ?


⌛ Prends deux minutes pour y songer. Les questions qui font réfléchir au sens que l'on met dans ses actes sont puissantes.

🎓 Cette année, j'ai fait 2 sessions de formation à la reproductibilité scientifique sous R avec Rmarkdown et RStudio. Une en février et une en juillet. 18 chercheurs ont participé en tout (dont mon mentor en stat, si c'est pas extra ça !) et une des premières questions que je leur ai posées, c'est celle-là.
Le nuage de mot représente leurs réponses. La taille des mots est proportionnelle à leurs présences dans les réponses. Les couleurs sont aléatoires.

***

Je te partage ma réponse, qui n'est absolument pas la vraie unique réponse. C'est simplement ce qui important pour moi, au moment où je t'écris.

✨ Donc, pour moi, ce qui est le plus important en science, c'est d'être dans une **démarche d'amélioration continue afin de pouvoir transmettre de nouveaux éclairages sur le fonctionnement du monde qui soient précis, simple(xe)s, robustes et reproductibles**. ✨

🏗️ Mais au fond de moi, je crois que si je dois retenir UN mot de ma phrase, alors je choisirais "**reproductible**". C'est la base stable sans laquelle tout le reste ne peut reposer.

❌ Pas d'amélioration continue sans traces de ce qui a déjà été fait.   
❌ Pas de transmission adéquate sans parler de la démarche sous-jacente aux résultats.

La précision 🎯, la simplicité 🤯 et la robustesse ✊ sont des critères (parmi d'autres) pour juger de la qualité de l'étude en question.  
✍️ La reproductibilité fait intégralement partie de cette démarche qualité et aide considérablement à améliorer ces critères. Choisir d'aller dans une démarche plus reproductible me force à être plus rigoureuse. Ça ne me donne pas d'autre choix.

Et c'est comme ça que je choisis, en toute connaissance de cause, de ne pas avoir le choix !

***

Alors, c'est quoi le plus important dans la science pour toi ?


***


<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-2">
    <i class="fas fa-plus"></i>
    Lire Qualité 2/4
  </a>

  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-3">
    <i class="fas fa-plus"></i>
    Lire Qualité 3/4
  </a>
  
  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-4">
    <i class="fas fa-plus"></i>
    Lire Qualité 4/4
  </a>
</div>




