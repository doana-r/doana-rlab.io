---
title: J'ai obtenu l'agrément CIR
author: anna-doizy
date: '2023-09-28'
slug: agrement-cir
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Imagine-toi à un rassemblement officiel et un peu classe. J'ai une annonce ! 
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Imagine-toi à un rassemblement officiel et un peu classe.

Là, voi-là. Tu as bien mis ta tenue chic, j'espère ? 🤵

✨ J'ai une annonce ! ✨

***

DoAna obtient cette année l’agrément 

>**« expert scientifique ou technique effectuant des travaux de recherche et développement »** 

délivré par le Ministère de l’Enseignement Supérieur et de la Recherche. L’actuel agrément couvre les années **2023 à 2025**.

Si tes opérations sont éligibles au **Crédit Impôt Recherche** (CIR), mes prestations peuvent faire partie du dossier de déduction d'impôt de ton entreprise.

Le taux du crédit d’impôt accordé aux entreprises est de **50 % en Outre-mer et 30 % en métropole** des dépenses éligibles. 

***

En revanche, ne nous emballons pas trop vite, si tu n'as jamais fait le fameux dossier, je pense que c'est un peu administravo-tordu. Je ne suis pas compétente pour t'aider, en tous les cas. Mais, je sais qu'il y a des gens dont c'est le métier et que, si tu rentres dans les cases, ça vaut le coup de les payer, parce qu'ils te feront gagner plus d'argent qu'ils ne t'en prendront ^^


Bref, je mets un lien vers [le guide du CIR](https://www.enseignementsup-recherche.gouv.fr/sites/default/files/2023-05/guide-du-cr-dit-d-imp-t-recherche-2022-27749.pdf), pour le cas où tu te sentes concerné.e !





