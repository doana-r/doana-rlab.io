---
title: Première formation en pédagogie active
author: anna-doizy
date: '2023-03-02'
slug: premiere-formation-pedagogie-active
categories:
  - Offre
tags:
  - Automatiser
  - Sens
  - Transmettre
subtitle: ''
summary: Il est temps que je te parle de comment s'est déroulée ma première formation en pédagogie active, ludique et participative.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🎲 Il est temps que je te parle de comment s'est déroulée ma première formation en pédagogie active, ludique et participative (#palp).

💡 Bon, en fait non, je ne me sens pas très bien placée pour en parler. Alors, j'ai pris soin de recueillir les retours des 9 participant(e)s.

📜 C'est normal de faire ça. C'est d'ailleurs demandé dans le cadre de la charte qualité des organismes de formation certifiés "QUALIOPI" qui permet aux entreprises de faire financer des formations en passant par leur OPCO.  
(Note en passant, je me suis associée avec une dizaine d'autres formatrices pour créer un organisme de formation certifié avec QUALIOPI. C'est en cours et je te tiendrai au courant quand ce sera un peu plus officiel, parce que c'est un sacré projet !)

⌛ Bien sûr, je n'ai pas eu que des retours positifs comme ceux que tu peux voir ici ! En gros, il y a un consensus sur la durée trop courte qui n'a pas permis d'avoir assez de temps pour tester et appliquer l'outil en conditions réelles.

🧠 Alors, je suis en train de remanier le déroulé pour le faire passer de 1 journée à 2 et prendre en compte au mieux tous les retours très constructifs que j'ai reçus.

🎉 J'ai déjà l'accord du CIRAD (mon client) pour lancer une 2ᵉ session !  
Ce sera en mai ou juin. Affaire à suivre...

💖 Merci énormément aux participant(e)s d'avoir joué le jeu (c'est le cas de le dire), au service formation du CIRAD pour sa confiance et sa réactivité, clin d'œil à Frédéric Chiroleu pour la partie logistique.

