---
title: Formation à rmarkdown par le jeu (1)
author: anna-doizy
date: '2023-01-26'
slug: formation-rmarkdown-par-le-jeu-1
categories:
  - Offre
tags:
  - Automatiser
  - Sens
  - Transmettre
subtitle: ''
summary: Je sors ma première formation en pédagogie active le 7 février 2023. Et j'ai vraiment hâte de la tester !
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


☀️ Je sors ma première formation en pédagogie active.  
Et j'ai vraiment hâte de la tester !

C'était un sacré défi de sortir du cadre habituel que j'ai toujours connu dans mes études scientifiques :
- 🏫 le/la formatrice qui sait et qui fait défiler ses slides,
- 💻 les participants qui apprennent derrière leurs ordinateurs toute la journée.

😣 Ca fait 2 ans que je réfléchis à cette formation en particulier. Je sais qu'il y a un réel besoin. Mais j'étais bloquée pour créer mon déroulé et me sentir BIEN avec.

⚙️ Entre temps, au cours de diverses expériences (création d'une coopérative, gestion du conflit dans un collectif professionnel, démystification de l'entrepreneuriat, accompagnement sur la stratégie d'entreprise...) j'ai suivi (puis créé) des accompagnements inattendus et que j'ai adoré.  
Ils utilisaient des techniques de la pédagogie active : l'apprentissage est plus horizontal, le savoir part des participants eux-mêmes, ils sont actifs dans leur propre apprentissage, etc.

💡 J'ai eu un déclic en novembre. Tout le déroulé s'est dessiné nettement dans ma tête, d'un coup !

🎲 Dans cette formation d'une journée, j'ai prévu de faire des jeux, des quizz, des défis individuels ou en groupe. J'ai eu envie de varier au maximum les types d'apprentissage. Clairement, les participants vont se lever, bouger, dessiner, débattre en eux, créer de nouvelles idées (et coder, évidemment).

👀 Peut-être qu'il restera des places pour cette session (c'est purement en présentiel, 10 personnes maximum). Fais-moi signe si tu es intéressé(e).

**NB**: pour les personnes salariées du CIRAD, c'est pris en charge directement par le CIRAD.

