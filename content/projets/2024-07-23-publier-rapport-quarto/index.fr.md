---
title: 3 étapes ultra-simples pour mettre en ligne un rapport quarto depuis RStudio
author: anna-doizy
date: '2024-07-23'
slug: publier-rapport-quarto
categories:
  - Tutoriel
tags:
  - Automatiser
subtitle: ''
summary: Une fois que tu as ton premier rapport, ça va te mettre littéralement 5 minutes pour le publier !
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

## 😶‍🌫️ quarto, quoi c'est ?

C'est rmarkdown (si ça te parle) dans sa deuxième vie ! 

Il s'agit d'un outil indépendant, mais intégré dans RStudio pour créer des rapports, des présentations, des livres, des sites web, ... directement depuis RStudio.


## 🤔 Mais pourquoi j'irai utiliser ça ?

- ⌛ parce que c'est vachement efficace, comme TOUT est dans un seul fichier, ça va t'éviter plein de copier-coller
- ♻️ parce que ça permet d'être plus reproductible (et ça, c'est drôlement chouette, pour toi, pour tes collègues et pour la science)


Quand je dis TOUT, je parle de :

- 👉 ton texte rédigé
- 👉 ton code
- 👉 les sorties de ton code (tables, figures)
- 👉 ta bibliographie
- 👉 ... (Il y a une foultitude d'options, mais ce n'est pas l'objet de ce post.)


## Pourquoi publier ?

Si 
- ✅ je souhaite partager un fichier à plus d'une personne 
- ✅ il ne contient pas d'infos confidentielles
- ✅ j'ai envie qu'il soit disponible sur du long terme

Alors

➡️ je le mets en ligne, parce qu'il me semble que c'est moins gourmand en stockage de transmettre une url que le rapport en pièce-jointe de X mails (Dis-moi si tu as un autre avis ? Ça m'intéresse)


<br>

🤩 Bref, une fois que tu as ton premier rapport, ça va te mettre littéralement 5 minutes pour le publier en ligne et je t'ai résumé ça dans cette image.

***

📚 Plus d'info sur quarto [ici](https://quarto.org/docs/publishing/quarto-pub.html).




