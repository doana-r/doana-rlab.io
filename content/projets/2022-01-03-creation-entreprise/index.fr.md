---
title: Création d'entreprise
author: anna-doizy
date: '2022-01-03'
slug: creation-entreprise
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Je me suis envolée. Hop, je ne suis plus couvée, j'ai sorti mes ailes toutes neuves et... décollage !
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


Je me suis envolée. Hop, je ne suis plus couvée, j'ai sorti mes ailes toutes neuves et... décollage !

Me voilà officiellement depuis hier à la tête de mon entreprise.

Bizarre à dire, après avoir bénéficié d'un appui à la création d'entreprise au sein d'une "couveuse" pendant presque 2 ans !

C'est comme quand on devient majeur ou après la toute première fois qu'on a fait l'amour. Il n'y a rien qui change vraiment, mais tout est un peu différent quand même.

Ça, c'est fait.

2022, c'est bon. On peut passer à 2023 maintenant ?

***

**Crédit photo** : je crois que ça vient d'une carte postale Gélabert

