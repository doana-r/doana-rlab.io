---
title: Viens, on fait du troc ?
author: anna-doizy
date: '2023-08-02'
slug: viens-on-fait-du-troc
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Mon petit test de troc a bien fonctionné ! Je propose trois rendez-vous aléatoires par mois.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

😊 Mon petit test de troc a bien fonctionné !

🤑 J'ai rencontré [Tufan Inan](https://www.linkedin.com/in/tufan-inan/) qui souhaitait avoir des astuces de stats et de programmation pour avancer sur son projet qui concerne l'économie de l'immobilier et l'illusion monétaire.

🙈 J'ai pu l'aider malgré le fait que ce ne soit paaaaas du tout mon domaine !
C'était une chouette rencontre et j'ai l'impression que je lui ai apporté de la tranquillité pour avancer de son côté.

☀️ De mon côté, ça me donne de la confiance pour proposer trois rendez-vous aléatoires par mois. J'ai ajouté un bouton sur mon site pour prendre un rendez-vous gratuit d'une heure.

💻 Je suis disponible à d'autres moments aussi, alors n'hésite pas à me faire un petit message si tu es intéressé(e) pour travailler avec moi ou avoir des conseils.

