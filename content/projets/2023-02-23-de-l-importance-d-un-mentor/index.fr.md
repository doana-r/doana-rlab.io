---
title: De l'importance d'un mentor
author: anna-doizy
date: '2023-02-23'
slug: de-l-importance-d-un-mentor
categories:
  - Etude de cas
tags:
  - Comprendre
  - Concevoir
  - Automatiser
  - Sens
  - Transmettre
subtitle: ''
summary: Le chemin avec R, il est infini. Et plus on s'y laisse prendre, plus il devient magnifique. R à plusieurs, c'est 1000 fois plus enrichissant !
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🎓 Je me revois il y a 5 ans. Fraîchement diplômée (Ingénieure Agro / Master Maths), toute fière de pouvoir écrire sur mon CV :  
**R**

Je n’avais suivi que des TDs avec des jeux de données parfaits. Ce qui n’arrive jamais dans la vraie vie, n'est-ce pas ?
Je connaissais 5 fonctions (`read.csv()`, `plot()`, s`ummary()`, `lm()` et `anova()`, fastoche) et je me souviens que la prof nous demandait de changer les virgules en points dans les fichiers Excel avant de les importer (parce qu'elle ne savait peut-être pas ce que c'était l'aide et les arguments ?).  
Je me sentais la reine des stats (non, ça c'est faux, mais le reste, c'est vrai).

🙈 Bref. J'étais super nulle.  
Je ne savais pas encore à quel point, mais quand même un tout petit peu suffisamment pour culpabiliser à l'entretien d'embauche de chargée d'études statistiques au CIRAD à la Réunion.

💻 Le timing était bon. Mes 3 premières semaines de travail ont consisté à seconder, Frédéric Chiroleu (Fred), le statisticien de l'équipe dans sa formation *"initiation à R"*.  
Alors, je te dis tout de suite, j'ai rien secondé du tout. Je me suis tue et j'ai appris.  
Et puis j'ai continué d'apprendre pendant mes 2 ans au CIRAD.  
Et puis je continue d'apprendre tous les jours, depuis que j'ai créé mon entreprise.

🍀 Le chemin avec R, il est infini. Et plus on s'y laisse prendre, plus il devient magnifique.  
Les choses, elles deviennent moches quand on cesse de vouloir les découvrir, non ?

☀️ Mais ce que j'ai surtout envie de te dire, là, c'est que je ne serais jamais arrivée ici sans Fred, mon boss, celui qui m'a fait suffisamment confiance pour me recruter il y a 5 ans.  
Il m'a accompagné, guidée, mise en confiance.  
On a bataillé ensemble devant des messages d'erreur incompréhensibles.  
On a râlé ensemble sur des "jeux de données pourris".  
On a ri, aussi. Beaucoup. (Demandez-moi de vous raconter la chasse au trésoR pour trouver des bières cachées dans un faux plafond…)

🎉 Ce que j'ai appris de plus fort, de plus beau, c'est que R à plusieurs, c'est 1000 fois plus enrichissant !

Je ne te promets pas de chasse au trésor, mais si tu veux apprendre à t'amuser avec R et ne plus être seul(e) devant tes messages d'erreur, je serai ravie d'en discuter avec toi !

PS : la photo, c'est pendant mon talk de ce matin au CIRAD, mon ancienne entreprise et mes collaborateurs actuels (je t'en ai parlé la semaine dernière 😉) et quand j'ai parlé de cette partie de mon parcours que je n'avais jamais racontée, j'ai fait rire tout le monde ! 😅  
PPS : Fred c'est le monsieur en rouge au premier rang !  
PPPS : Merciiii Anabelle Canivarez pour m'avoir relue et donné de précieux conseils de rédaction ✍️✨ Je suis tellement ravie de trouver des occasions de travailler avec toi.
