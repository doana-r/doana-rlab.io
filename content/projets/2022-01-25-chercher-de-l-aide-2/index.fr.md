---
title: Chercher de l'aide (2)
author: anna-doizy
date: '2022-01-25'
slug: chercher-de-l-aide-2
categories:
  - Tutoriel
tags:
  - Automatiser
  - Comprendre
subtitle: ''
summary: Vous en êtes sûr. Voilà. Personne n'a JA-MAIS eu votre problème dans R. Bon, il va falloir vous-même formuler votre question.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


Vous en êtes sûr. Voilà. Personne n'a JA-MAIS eu votre problème dans R.  
Du moins, mmhmm, vous n'avez pas trouvé la solution : ni dans votre tête, ni dans la doc éclairée et éclairante de R (avec `?fonction`), ni dans StackOverFlow, ni dans Github, ni dans moindre blog suspicieux/suspect contenant des stats.

Ne mourez pas. Reprenez-vous. Séchez vos larmes.

Bon, il va falloir vous-même formuler votre question.

Mais alors, comment faire pour donner aux autres (les gentilles personnes qui répondent dans les forums, les créateurs des packages, votre statisticien(ne) préféré(e), ...) l'envie (que dis-je, le désir profond) de vous aider ?

Tadaa

Voici l'**EXEMPLE MINIMAL REPRODUCTIBLE** !

Les principes de base de l'exemple minimal reproductible (ou MRE in english) sont :
- un jeu de données minimal (et donc léger), utile pour reproduire le problème
- un code que l'on peut lancer (donc pas une capture d'écran) grâce au petit jeu de données et qui reproduit l'erreur (la vilaine)
- les informations sur votre R session, c'est-à-dire les numéros de versions de R et de vos packages (alors autant vous le dire tout de suite : sauf raison particulièrement raisonnable, METTEZ A JOUR VOS LOGICIELS, crotte de bique coeur coeur) ainsi que le système d'exploitation que vous utilisez (Windows 95, Linux Manjaro 21.0, ...)
- Si vous avez des fonctions qui utilisent l'aléatoire : fixez le pseudo-aléatoire avec `set.seed(le nombre que vous voulez)` pour permettre à tout le monde d'obtenir la même chose que vous.

Pour vous aider, vous avez même un package de R fait exprès : **reprex** (pour *reproducible r exemple*)

Ca fait peur ? C'est beaucoup de travail ?

La bonne nouvelle, c'est que dans 80% des cas, en faisant tout ça, vous allez trouver vous-même la solution à votre problème.

L'autre bonne nouvelle, c'est que le reste du temps, les gentilles personnes susmentionnées seront ravies de vous aider !

***  

Sources : 
- StackOverFlow : [How to create a Minimal, Reproducible Example](https://stackoverflow.com/help/minimal-reproducible-example?fbclid=IwAR3Wi3dtUf3dcqBAMr-MknJ4yyHFeCLcvkATs87cxzazEp96xhOLwb2zYA0) et [How to make a great R reproducible example](https://stackoverflow.com/questions/5963269/how-to-make-a-great-r-reproducible-example?fbclid=IwAR27ZkLiAWwjNY2YvXv5xz9lVUB8CPRfwy4ZKA701Lo1_ucH5KuDE25WRRQ)
- le package reprex : [Reprex do's and don'ts](https://reprex.tidyverse.org/articles/reprex-dos-and-donts.html?fbclid=IwAR3Wi3dtUf3dcqBAMr-MknJ4yyHFeCLcvkATs87cxzazEp96xhOLwb2zYA0) et [How to use reprex](https://reprex.tidyverse.org/articles/learn-reprex.html)


Illustration : [Allison Horst](https://github.com/allisonhorst/stats-illustrations)

<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/chercher-de-l-aide-1/">
    <i class="fas fa-plus"></i>
    Chercher de l'aide (1)
  </a>

  <a class="btn btn-outline-primary" href="/projets/aide-moi-a-t-aider/">
    <i class="fas fa-plus"></i>
    Aide-moi à t'aider !
  </a>
</div>

***