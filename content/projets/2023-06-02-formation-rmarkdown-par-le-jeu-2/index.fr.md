---
title: Formation à rmarkdown par le jeu (2)
author: anna-doizy
date: '2023-06-02'
slug: formation-rmarkdown-par-le-jeu-2

tags:
  - Automatiser
  - Transmettre
  - Sens
categories:
  - Offre
  
summary: Créé des rapports automatisés avec R. Je lance une 2ᵉ session de formation sur Rmarkdown les 5 et 6 juillet 2023 !
authors: []
external_link: ''
image:
  caption: ''
  focal_point: ''
  preview_only: no
url_code: ''
url_pdf: ''
url_slides: ''
url_video: ''
slides: ''
---

🗒️ Créé des rapports automatisés avec R

Je lance une 2ᵉ session de formation sur Rmarkdown les 5 et 6 juillet !

C'est une formation organisée par le CIRAD et j'en serai l'heureuse animatrice.
❌ J'ai décidé que les formations avec des apprenants et un sachant qui présente juste un diaporama, ça ne me convenait plus.


🎲 Je fais tout mon possible pour ajouter du jeu dans mes ateliers. Le principe, c'est que les participant(e)s soient acteurs et actrices de leur apprentissage et repartent avec :
- ☀️ de la confiance dans leurs compétences qu'ils/elles ont déjà
- 🗝️ des savoirs à mettre en action directement
- ➡️ un prochain petit pas pour avancer
- 🎉 du fun !


Cette formation, pour exister, a besoin d'un minimum de 8 participants dont au moins une personne extérieure au CIRAD. Le CIRAD est un organisme certifié QUALIOPI, donc la formation est finançable par une entreprise ou le pôle emploi.

✍️ Si tu connais une personne à La Réunion qui peut être intéressée par cette formation, tu peux lui transmettre l'info ?

![](reproductibilité-temoignages.png)




