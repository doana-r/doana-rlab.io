---
title: Bien lire les graphiques
author: anna-doizy
date: '2022-03-24'
slug: bien-lire-les-graphiques
categories:
  - Offre
tags:
  - Comprendre
  - Transmettre
subtitle: ''
summary: Chacun(e) peut proposer un atelier sur son activité. Pour ma part, les stats scientifiques étant un domaine légèrement technique, je suis penchée sur l'approche la plus visuelle de mon métier pour espérer transmettre son côté ludique. Devinez laquelle est-ce ?
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Avec mes collègues et ami(e)s entrepreneurses du collectif PAIRS et du collectif Libranou, nous aimons nous retrouver tous les mois pour partager nos avancées, nos réussites, nos problématiques. Chacun(e) peut proposer un atelier sur son activité pour le tester, pour attirer leur curiosité, pour partager ou transmettre quelque chose.

Pour ma part, les stats scientifiques étant un domaine légèrement technique, je suis penchée sur l'approche la plus visuelle de mon métier pour espérer transmettre son côté ludique. Devinez laquelle est-ce ?

C'est visuel, c'est parlant, ils racontent une histoire sur des données chiffrées. Il s'agit des **graphiques**, bien sûr !

Mon objectif étant de **diminuer les erreurs** (au sens large) **pour mieux comprendre le monde**, le message que je veux faire passer est : 

> Gardons l'œil affûté sur les données qui passent dans les médias (ou ailleurs). 

Je ne parle pas de questionner les chiffres en eux-mêmes dans un premier temps, mais simplement de regarder purement la forme. La **représentation visuelle des chiffres** parle beaucoup (plus que ce que l'on croit) à qui sait lire ces mises en forme.

Et comme il s'agit de rire un peu dans la vie, j'ai trouvé un tas de graphiques (réels) spécialement bien pourris, qui illustrent parfaitement mes propos 😛 

