---
title: Exposition de Femmes et Mathématiques
author: anna-doizy
date: '2022-05-12'
slug: exposition-femmes-et-mathematiques
categories:
  - Brève
tags:
  - Sens
  - Transmettre
subtitle: ''
summary: Je suis surprise d'avoir été choisie, enjouée de partager mon métier de cœur, emplie de gratitude pour cette formidable organisation.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

## Mathématiques, informatiques... avec elles !

Je suis surprise d'avoir été choisie, enjouée de partager mon métier de cœur, emplie de gratitude pour cette formidable organisation.

Voici une petite contribution pour déconstruire les stéréotypes sur les mathématiques.

Aujourd’hui, le 12 mai, c'est la journée internationale qui célèbre les femmes dans les maths (https://may12.womeninmaths.org). Et dans 2 jours, le 14 mai, c'est l'inauguration de l'exposition **"Mathématiques, informatiques... avec elles !"** à l'institut Henri Poincaré, organisée par l'association **Femmes & Mathématiques**.

20 femmes répondent à des questions comme : 

> Avez-vous eu des difficultés en tant que femme dans un monde de mathématiques/informatique ? 

> Quels conseils donneriez-vous aux jeunes ?

Cette exposition sera présentée dans plusieurs villes de France, et peut-être même un jour à la Réunion. Elle est aussi disponible en ligne ici : https://femmes-et-maths.fr/maths-info-avec-elles/ !

Vous pouvez découvrir le contenu des interviews en cliquant sur les portraits.

J'y serai présente aux alentours du 21 mai, et vous ? 😉

***

Photographie : [Marie-Pierre Dieterlé](https://mariepierredieterle.com)

