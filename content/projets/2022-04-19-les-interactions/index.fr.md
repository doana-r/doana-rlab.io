---
title: Les interactions
author: anna-doizy
date: '2022-04-19'
slug: les-interactions
categories:
  - Tutoriel
tags:
  - Comprendre
subtitle: ''
summary: Sans elles, on peut obtenir des interprétations aussi absurdes que L'humain moyen a un testicule.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---



**Définition de Wikipédia** :
> "En statistique, une **interaction** décrit une situation dans laquelle l'influence d'une variable dépend de l'état de la seconde."


Par exemple, ici, la variable `nombre de testicules` (j'aime caser des mots improbables) dépend principalement de la variable `sexe`. 

Ça parait complètement évident, dit comme ça, mais ça arrive de passer à côté **BIEN** plus souvent qu'on croit. Et quand on travaille avec des variables que l'on a moins l'habitude de manipuler que... celles présentées en exemple, alors on peut obtenir des interprétations aussi absurdes que 

> L'humain moyen a un testicule.

Au sens très large, un **biais** est "une démarche ou un procédé qui engendre des erreurs dans les résultats d'une étude" (Wikipédia encore). 

Ainsi, **la non prise en compte des interactions dans un modèle statistique est source de biais**.

Pour pas être baisé, heuuuu NON, biaisé : n'oubliez pas d'écrire vos interactions (vous savez : `~ A * B,` plutôt que `~ A + B`) avant d'interpréter les résultats de vos modèles !

***

Source de l'illustration : peut-être https://www.cafepress.co.uk/profile/jovialjim