---
title: Mon projet DoAna a quatre ans
author: anna-doizy
date: '2024-04-09'
slug: mon-projet-doana-a-quatre-ans
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Je n'ai pas du tout envie d'étaler tout ce que j'ai fait pendant ces quatre ans. J'ai plutôt envie de célébrer là où j'en suis.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

✨ Mon projet DoAna a quatre ans !

Je n'ai pas du tout envie d'étaler tout ce que j'ai fait pendant ces quatre ans. J'ai plutôt envie de célébrer là où j'en suis.

♻️ Je me sens stable.

🌱 Avec une entreprise qui tient la route, qui est cohérente avec mes valeurs, qui me permet de me sentir en sécurité financière, qui me donne de l'énergie (ou qui me soutient quand je n'en ai pas).

🗝️ DoAna apporte des outils méthodologiques et statistiques qui permettent à mon public de cœur — les personnes qui font de la recherche et qui souhaitent améliorer leur démarche scientifique — d'être plus efficace et plus confiant sur la réussite de leurs projets.

🫂 Merci à toutes les personnes qui sont avec moi, de près ou de loin.

