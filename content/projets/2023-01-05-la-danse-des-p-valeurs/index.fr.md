---
title: La danse des p-valeurs
author: anna-doizy
date: '2023-01-05'
slug: la-danse-des-p-valeurs
categories:
  - Brève
tags:
  - Comprendre
  - Transmettre
subtitle: ''
summary: Commençons 2023 en poésie. J'ai demandé à Chat GPT de faire quelques quatrains sur la "danse des p-valeurs"
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🎭 Commençons 2023 en poésie.

💻 J'ai demandé à Chat GPT (😉 [Orane Massias](https://www.linkedin.com/in/orane-massias/)) de faire quelques quatrains sur la "danse des p-valeurs" (*p-hacking* en anglais, mais c'est moins lyrique). Bon, je n'ai pas réussi à le faire bien rimer !

🙊 En tous les cas, c'est pas moi qui le dit, c'est l'IA.

***

**BONUS** : en voilà un second, qui est simplement sorti en appuyant sur re-générer :

La danse des p-valeurs, un phénomène suspect  
Où l'on teste et reteste, pour trouver l'effet  
On sélectionne les données qui vont bien  
Pour soutenir notre thèse, même si elle est fausse  
C'est une pratique douteuse, qui peut tromper  
Et faire croire à des vérités qui ne sont pas sûres  
Mais attention, car elle peut aussi nuire  
À la crédibilité de la science, et son prestige atténuer  
Alors que faire, quand les p-valeurs dansent ?  
Il faut rester vigilant, et ne pas céder  
À la tentation de tricher, ou de mentir  
Mais plutôt, respecter la vérité, et la chercher.

