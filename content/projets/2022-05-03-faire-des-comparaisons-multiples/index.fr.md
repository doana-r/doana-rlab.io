---
title: Faire des comparaisons multiples
author: anna-doizy
date: '2022-05-03'
lastmod: 2022-08-08
slug: faire-des-comparaisons-multiples
categories:
  - Tutoriel
tags:
  - Comprendre
  - Concevoir
subtitle: ''
summary: Les différents traitements que je veux comparer ont-ils un effet statistiquement différent sur la variable que je suis en train de mesurer dans telles conditions ? Quand on se pose ce genre de question, le type d'analyse statistique approprié est la comparaison multiple de moyennes.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

## Comparaisons multiples : *Big picture*

Les scientifiques peuvent se poser différents types de questions.  
L'un d'entre eux peut se formuler schématiquement ainsi : "Les différents traitements que je veux comparer (des modalités d'entretien des manguiers, la densité de plantation de plantes pollinisées, le travail du sol et la quantité d'arrosage, l'ajout d'un produit ou non dans une colonne de filtration…) ont-ils un effet statistiquement différent sur la variable que je suis en train de mesurer (le rendement de mes manguiers, le nombre de pollinisateurs piégés, la survie de mes arbres, la propriété chimique d'un sucre…) dans telles conditions (environnementales, non contrôlées) ?"

Quand on se pose ce genre de question, le type d'analyse statistique approprié est la **comparaison multiple de moyennes**.   
Autrement dit, comparer les moyennes de ma variable mesurée *à expliquer* pour chacune des modalités / traitements mis en place dans le cadre d'un protocole expérimental.

Je vous présente un petit mémo que j'ai rédigé dans le cadre d'une formation qui a eu lieu la semaine dernière à ce sujet. Quand j'ai ce type de question scientifique en tête, je veille à suivre systématiquement toutes ces étapes et dans cet ordre. Cette méthode, peut-être qu'elle vous aidera. Je ne l'ai trouvée dans aucun des cours de statistiques que j'ai reçu. 


Et, pour moi, je pense que ce sont des questions auxquelles les ordinateurs ne sont pas prêts à répondre automatiquement. J'entends par là que ces questions, ces étapes guident mon cheminement et mes choix dans mes analyses. Elles me donnent le recul approprié pour les interpréter. Et aucune, aucune, des questions scientifiques amenant à des comparaisons multiples de moyennes que je me suis posée n'a été répondue de la même manière que les autres.

En fait, ce que je vous présente ici, c'est un peu la *big picture*. Je me rends compte que je m'inspire de chacune de ces étapes pour rédiger mes publications. Par exemple, dernièrement : "C'est quoi les interactions ?".


![](faire-des-comparaisons-multiples.jpg)


## Facilitation graphique


J'ai trouvé un outil assez enthousiasmant ! 

La facilitation graphique. 

Déjà, il y a le mot graphique dedans. J'adore les graphiques.  
Sauf que plutôt que de représenter des données chiffrées (le rôle des graphiques), la facilitation graphique permet notamment de représenter des notions textuelles complexe.

Il ne s'agit pas que du côté "joli" des choses graphiques. Ce que j'aime particulièrement, c'est l'enjeu cognitif. Le cerveau traitement beeeaucoup plus facilement un dessin qu'un tableau avec des chiffres ou un gros texte bien dense. Une image (bien choisie) vaut mille mots.

Le site https://supertilt.fr/quest-ce-que-la-facilitation-graphique/ donne cette définition :

> "La facilitation graphique, c'est l’usage du visuel au service des interactions.
> Pour être plus précis, c’est associer le visuel au texte pour communiquer, convaincre, transmettre une information. Bien qu’elle puisse être très utile pour décrire les étapes de votre recette de cuisine favorite, la facilitation graphique est particulièrement adaptée aux situations professionnelles où l’information est dense et complexe."

Je vous présente ici mon premier essai, qui reprend les étapes de la comparaison multiple que je vous ai présenté il y a 3 mois.

Alors, ça vous donne davantage envie de les lire ?


![](featured.png)

