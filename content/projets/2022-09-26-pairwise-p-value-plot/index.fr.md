---
title: Pairwise p-value plot
author: anna-doizy
date: '2022-09-26'
slug: pairwise-p-value-plot
categories:
  - Tutoriel
tags:
  - Comprendre
subtitle: ''
summary: Tutoriel pour lire les graphiques issus de la fonction `pwpp()` du package **emmeans** et interpréter le résultat d'un test de comparaisons multiples.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

## Tutoriel pour lire les graphiques issus de la fonction `pwpp()` du package **emmeans**.

Je vous présente ici un type de graphique un peu particulier : le **pairwise p-value plot** !  
Il permet d'interpréter correctement le résultat d'un test de comparaisons multiples.

Ici, on veut comparer deux-à-deux (pairwise en anglais) des valeurs (une quantité quelconque : rendement, poids…) mesurées au sein de 4 traitements que j'ai appelés ici A, B, C et D, tout simplement.

Vous connaissez peut-être déjà la fonction `cld()` du package **multcomp** ?  
Elle regroupe automatiquement les modalités *non significatives entre elles* : dans cet exemple, on obtiendrait 2 groupes avec D tout seul et A, B et C ensemble.

Le `pwpp()` est une bien meilleure alternative à `cld()`, car il permet une visualisation plus globale et précise du résultat de comparaisons multiples, sans se faire avoir par le côté manichéen du seuil de 5 % !

Plus mes p-valeurs sont vers la gauche du graphique et plus je peux avoir confiance dans leur significativité 🙂   
L'interprétation peut ainsi être beaucoup plus fine.

La valeur qu'apporte `pwpp()` dans mes analyses, par rapport à `cld()`, c'est un peu comme regarder les données brutes AVANT de faire des boxplots ou autres moyennes -> **IN-DIS-PEN-SABLE** !

**PS** : Ce n'est pas moi qui le dit, c'est le créateur du [package emmeans](https://cran.r-project.org/web/packages/emmeans/vignettes/comparisons.html?fbclid=IwAR16kh98sGUrfRal-26XETrd40ECEguUBktbGH6XHFKon__jV9DsVgcs05s#CLD)

***

> ATTENTION, je ne le répéterai jamais assez - trust me : 
>
> 1. N'oubliez pas de valider les hypothèses de votre modèle paramétrique avant de regarder ses résultats !
> 1. Les comparaisons multiples se font "toutes choses égales par ailleurs", alors, sauf dans le cas où vous êtes sûrs et certains de ce que vous êtes en train de faire, ne faites PAS de tests de comparaisons multiples si vous avez des interactions statistiquement significatives dans votre modèle
> 1. Selon la complexité de votre modèle, le fait que les intervalles de confiance de vos paramètres estimés par le modèle se recoupent ou non n'est pas forcément lié au résultat des tests de comparaison multiple. En effet, ces tests utilisent une correction (adjustment) du risque alpha (rejeter H0 - les 2 moyennes sont égales - à tort) de chacun des tests pour que le risque global soit effectivement inférieur à votre alpha.
> 1. Le package emmeans est versatile. Son comportement par défaut (grille de référence, contrastes, méthode d'ajustement, ...) permet de faire en sorte que "ça marche" simplement avec un minimum de code MAIS, quand on ne sait pas trop tout ce que cela signifie, on s'expose à de très mauvaises interprétations des résultats fournis (c'est du vécu...) !

