---
title: Qualité scientifique - 4/4
author: anna-doizy
date: '2023-09-28'
slug: qualite-scientifique-4
categories:
  - Tutoriel
tags:
  - Comprendre
  - Concevoir
  - Sens
  - Transmettre
subtitle: ''
summary: En statistiques, la robustesse d'un estimateur est sa capacité à ne pas être perturbé par une modification dans une petite partie des données ou dans les paramètres du modèle. En écologie et en biologie, la résilience écologique est la capacité à retrouver un fonctionnement ou un développement normal après avoir subi une perturbation.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

## 🍀 La résilience et la robustesse ✊


📈 "En **statistiques**, la robustesse d'un estimateur est sa capacité à ne pas être perturbé par une modification dans une petite partie des données — y compris par l'introduction de données aberrantes — ou dans les paramètres du modèle choisi pour l'estimation."

🦕 "En **science de l'évolution**, la robustesse correspond à la persistance d'une certaine caractéristique d'un système biologique subissant des perturbations ou des conditions incertaines, en biologie de l'évolution."

Puis,

🌱 "En **écologie et en biologie**, la résilience écologique est la capacité d'un écosystème, d'une espèce ou d'un individu à retrouver un fonctionnement ou un développement normal après avoir subi une perturbation écologique."

🌐 "Dans le **domaine social et de la gouvernance, de la gestion des risques**, la résilience communautaire associe les approches précédentes en s'intéressant au groupe et au collectif plus qu'à l'individu isolé, pour la capacité d’un système social à « rebondir » après une perturbation."

💔 "En **psychologie**, la résilience est un phénomène consistant à pouvoir revenir d'un état de stress post-traumatique."

etc.

etc.

dit Wikipedia.


🔁 Je ne sais pas ce que tu en penses, mais j'ai bien envie de dire que la robustesse et la résilience, ces deux concepts polysémiques, sont très liées.

D'un côté, la robustesse qui est **la capacité à se maintenir dans un état stable malgré une perturbation**.

De l'autre, la résilience qui est **la capacité à revenir à un état stable (qui peut être différent de l'état initial) après une trop grande perturbation**.

Et si, pour une science de qualité, on cultivait une alternance entre ces deux compétences ?

✨ Il me semble que l'une et l'autres s'enrichissent mutuellement.

Il s'agit de cultiver la **solidité** ET l'**adaptabilité**.  
Pratiquer l'**affirmation tranquille de soi** ET l'**art de rebondir**.  
**Résister aux bugues** ET faire des **mises à jour**.  
Avoir un **protocole expérimental balisé** ET **explorer sans connaître la destination**.

🧪 En science.   
👑 Pour soi.  
🫂 Avec les autres.

***

Sources :
- https://fr.wikipedia.org/wiki/Robustesse
- https://fr.wikipedia.org/wiki/R%C3%A9silience

***

<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-1">
    <i class="fas fa-plus"></i>
    Lire Qualité 1/4
  </a>

  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-2">
    <i class="fas fa-plus"></i>
    Lire Qualité 2/4
  </a>
  
  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-3">
    <i class="fas fa-plus"></i>
    Lire Qualité 3/4
  </a>
</div>
