---
title: Je suis chercheuse
author: anna-doizy
date: '2023-02-02'
slug: je-suis-chercheuse
categories:
  - Etude de cas
tags:
  - Concevoir
  - Sens
subtitle: ''
summary: Un chercheur / une chercheuse, j'ai plutôt envie de définir ce mot comme quelqu'un qui expérimente et qui se donne le droit à l'erreur.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Je suis chercheuse.

🤯 Je vais peut-être te choquer, mais je me définis comme chercheuse alors que je n'ai pas de doctorat et que je ne travaille pas dans un centre de recherche.

📜 Un chercheur / une chercheuse, j'ai plutôt envie de définir ce mot comme quelqu'un qui expérimente et qui se donne le droit à l'erreur.

- ✅ J'ai une hypothèse,
- ✅ je crée un protocole / une méthode / une mesure,
- ✅ j'itère un nombre de fois suffisant (ce n'est jamais suffisant haha),
- ✅ j'analyse les résultats pour prendre une décision,
- ✅ je réajuste mon hypothèse de départ,
- 🔁 Et je recommence.

J'essaie d'appliquer ça partout dans ma vie, partout où il y a de la variabilité. Plus ou moins rigoureusement, selon les enjeux, bien sûr.
Pour ma communication d'entreprise, pour mes achats, pour la cuisine, pour le sport, pour le chant, pour mes prises de décision, pour mes conflits, pour mes relations amoureuses !  

😨 Sans expérimentation, je suis terrifiée par l'échec. Pourtant, j'ai besoin de certitudes, comme toi. En pratiquant la démarche expérimentale, je sais bien que je ne serai pas certaine du résultat (c'est tout l'intérêt de la science et de la recherche !). Et ça peut faire peur, ça aussi.  
La certitude n'est pourtant pas en train de disparaître.

☀️ En décalant le problème : même si je me "rate", j'ai la certitude que je suis en train d'apprendre quelque chose. Ça m'apporte une très grande force.  
Quand je parle de "chercheur" en fait, je dis tout ça. Un chercheur, pour moi, ce n'est pas un titre, c'est un mindset : l'état d'esprit avec lequel il avance.  
Quand je dis que je veux sensibiliser et travailler avec des "chercheurs", je dis que je veux travailler avec des personnes pour qui la valeur "expérimentation" est forte. Pas forcément avec des docteurs. Vraiment. 😊


---


Va voir la vidéo de **Et Tout le Monde S'en Fout** sur l'échec ! 
https://www.youtube.com/watch?v=c0IsvrSpcx4

Et si tu as encore du temps, le podcat de Thomas Burbidge avec Alexandra Martel m'a beaucoup inspirée : https://www.youtube.com/watch?v=_4ZXWOodFcE


