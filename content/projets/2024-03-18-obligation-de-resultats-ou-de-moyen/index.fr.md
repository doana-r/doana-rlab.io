---
title: Obligation de résultats ou de moyens ?
author: anna-doizy
date: '2024-03-18'
slug: obligation-de-resultats-ou-de-moyen
categories:
  - Etude de cas
tags:
  - Sens
subtitle: ''
summary: ''
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


💬 "Comment me rassurer pour savoir si j'obtiendrai bien les résultats attendus en travaillant avec toi ?"

Un futur client me pose cette question la semaine dernière, dans le cadre d'un accompagnement R et stats.
J'ai envie de partager une synthèse de ma réponse, au cas où tu te poserais la question toi aussi !

---

Mon expérience me permet d'estimer à peu près le temps à prendre pour réaliser tel ou tel type de mission. C'est pour cela que je pose beaucoup de question durant le premier rendez-vous, pour bien cerner
- 🐣 d'où tu pars,
- 🐉 tes enjeux et
- ↗️ tes attentes pour y arriver.

Seulement, dans mon activité, je ne suis pas en mesure de m'engager contractuellement sur une obligation de résultats, uniquement une obligation de moyen. 🤷‍♀️

Voilà pourquoi :

- 🧪 il est possible que les données ne permettent pas de répondre à ta question, par manque de réplicats ou par biais méthodologiques, par exemple
 
- 📊 il est possible que je ne connaisse pas de modèle stat approprié pour analyser tes données dans les règles de l'art, voire qu'il n'existe pas de méthode adaptée
 
- 🏗️ il est possible qu'un problème technique (côté data ou programmation ou logiciel) survienne et prenne du temps à résoudre
 
- 🎲 tout autre événement imprévisible
 
Je n'arrive pas à percevoir les stats comme un bouton que l'on presse et il y a un résultat qui sort, elles font entièrement partie du processus de recherche expérimentale avec essai-erreur. ♻️

Le fait que tu sois présent lors des rendez-vous me permet (au-delà du côté pédagogique) de te consulter en direct en cas de besoin de clarification sur le protocole, par exemple, et c'est comme ça que j'ai choisi de fonctionner. 🤝

- 🏗️ En cas de problème technique, j'interromps le rendez-vous en cours pour régler le problème de mon côté sans facturer ce temps-là.

- 📊 Entre 2 séances, si je sens que j'ai besoin de me former davantage ou de produire quelque chose qui ne dépend que de moi, alors je prendrai le temps de le faire sans que cela te coûte davantage (sauf exception).
C'est dans mon éthique !

- 🌱 Au fur et à mesure, j'y vois plus clair et je peux te dire avec plus de certitudes où nous allons (en termes de résultats statistiques). Et il y a une probabilité (faible, mais non nulle) que je te dise que je ne peux pas atteindre les résultats attendus.

En termes de livrable, je partage le code R et il t'appartiendra de le modifier comme tu le souhaites, sous couvert que tu cites mon travail si tu le partages ou le publies. 📜

Une chose que je peux garantir en toute confiance, c'est que tu vas apprendre des choses. 👩‍💻

---

☀️ C'est hyper important pour moi que la personne qui se demande si elle va travailler avec moi se sente libre de ne pas s'engager avec moi, si ce que je propose ne répond pas exactement à ses attentes.

🔥 Bonne nouvelle, suite à ma réponse, la personne a bien été rassurée et a choisi de continuer avec moi !


