---
title: Qualité de vie au travail m'anime
author: anna-doizy
date: '2024-06-06'
slug: qvct
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: ''
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🥺 La QVCT, jusqu'au mois dernier, je ne savais pas du tout ce que ça voulais dire. Quoi ? Encooooore un sigle ??? 

☀️ Il s'agit de la qualité de vie au travail ! Bon... j'avoue, ça me parle...

Ca m'anime... tout le temps !
- 🤹‍♀️ quand j'ai choisi de devenir entrepreneuse
- 🌱 quand j'apprends des nouveaux trucs et que ça me permet de prendre du recul et d'évoluer (tous les jours)
- ✨ quand je transmets des pépites aux autres et que je les vois briller dans leurs yeux parce que ça va vraiment leur servir (en formation, en accompagnement, auprès de mes proches, ...)
- 💁‍♀️ quand j'ai choisi de me faire accompagner moi-même par plusieurs coach et formateur.ices (en construction d'entreprise vraiment alignée sur mes valeurs, en création de formation active et ludique, ...)
- ⌛ quand je prends des rendez-vous avec moi-même pour me poser, écrire, faire une chose qui me plaît maintenant, faire le bilan, prioriser, prendre le temps de clarifier mes pensées et mes aspirations, ...




🧏 Et... justement, je fais partie d'un organisme de formation, La Puce à L'oreille (ou LPAO) qui a des formatrices vraiment compétentes pour parler de ce sujet.

🍀 Et... quelle chance ! Elles animent des ateliers GRATUITS en présentiel et en distanciel toute la semaine du 17 juin !

😊 Regarde en dessous pour les détails   
⬇️⬇️⬇️



![](programme.jpg)


>La semaine de la QVCT chez LPAO, ça donne ça ⬇️ 
>
>Ateliers, Ron kozé et Co développement, en présentiel ou à distance avec des pépites d’intervenantes ! 
>
>Pour y participer c’est très simple, cliquez sur le lien d’inscription et laissez vous guider ! 
>
>Au menu de cette semaine : 
>
> - [L'équipe apprenante, facteur de QVCT - Atelier avec Aude](https://www.helloasso.com/associations/lpao/evenements/lpao-semaine-de-la-qvct)
>
> - [Agir aujourd’hui pour améliorer le travail de demain - Ron Kozé avec Amandine](https://citedesmetiers.re/travaildedemainronkoze/) 
>
> - [Préparer son projet QVCT grâce à la carte mentale - Atelier avec Julie](https://www.helloasso.com/associations/lpao/evenements/atelier-preparer-son-projet-qvct-grace-a-la-carte-mentale)
>
> - [Mettez de la couleur dans vos interactions pour favoriser la QVCT - Atelier avec Aude](https://www.helloasso.com/associations/lpao/evenements/atelier-mettez-de-la-couleur-dans-vos-interactions-pour-favoriser-la-qvct) 
>
> - [Expérimenter sur vos problématiques QVCT - Co développement avec Aude](https://www.helloasso.com/associations/lpao/evenements/co-developpement-experimenter-sur-vos-problematiques-qvct)
>
>Hâte de vous rencontrer et d’échanger QVCT !

![](0001.jpg)
![](0002.jpg)
![](0003.jpg)
![](0004.jpg)
![](0005.jpg)
![](0006.jpg)
![](0007.jpg)
![](0008.jpg)
![](0009.jpg)
![](0010.jpg)

