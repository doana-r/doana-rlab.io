---
title: La Puce à l'Oreille passe l'audit Qualiopi
author: anna-doizy
date: '2023-06-28'
slug: audit-qualiopi
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Je suis en train de co-créer un groupe d'entrepreuneur(e)s formatrices. Nous avons passé l'audit QUALIOPI au début du mois et nous attendons avec impatience le verdict !
authors: []
lastmod: '2023-06-28T18:46:33+04:00'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

## 🦘 Le grand saut pour un label QUALIOPI

J'ai une annonce à te faire. Elle présage une évolution de mon activité.

Je suis en train de co-créer un groupe d'entrepreuneur(e)s formatrices.
Nous sommes l'Organisme de Formation La Puce à L'Oreille (LPAO, pour les intimes).

🍀 Nous avons passé l'audit QUALIOPI au début du mois et nous attendons avec impatience le verdict !

- ⚙️ QUALIOPI, c'est un audit qui prouverait que LPAO est suffisamment sérieuse (oui au féminin, c'est une puce) et structurée pour avoir des processus de qualité pour délivrer des actions de formation et des bilans de compétence.
- 🪙 QUALIOPI nous permettrait de faire financer nos formations pour des salarié(e)s (par les OPCO) ou des demandeur(se)s d'emploi. C'est un sacré atout pour nos activités !
- 🎓 QUALIOPI nécessite de se conformer à un tas de critères administratifs pour que les besoins des formés soient au mieux écoutés dans une démarche d'amélioration continue.
- ✨ QUALIOPI est l'occasion idéale pour que nous démarrions une démarche qualité en interne de notre collectif.


## ➕ Le plus de la Puce

Dans la Puce, nous formons à des choses aussi variées que le droit, la communication, la gestion financière, la gouvernance partagée, l'économie solidaire et sociale, les analyses de données et la programmation (ça, c'est moi ^^)...
C'est bizarre, non ?  
🎲 En fait, ce qui nous relie, c'est la pédagogie active, ludique et participative.

Le principe, c'est que les participant(e)s soient acteurs et actrices de leur apprentissage et repartent avec :
- ☀️ de la confiance dans leurs compétences qu'ils/elles ont déjà
- 🗝️ des savoirs à mettre en action directement
- ➡️ un prochain petit pas pour avancer
- 🎉 du fun !


## 🗺️ Vers une nouvelle aventure

Maintenant, l'objectif est de trouver les moyens les plus fluides possibles pour fonctionner ensemble, en collectif.

Souhaite-nous une belle route !

***

Merci de cheminer à mes côtés :

- [Charlotte Verdure](https://www.linkedin.com/in/charlotte-verdure-54560514b/)
- [Isabelle Epivent](https://www.linkedin.com/in/isabelle-epivent-442b7310a/)
- [Patricia Clerc](https://www.linkedin.com/in/patriciaclerc/)
- [Judith Camhi](https://www.linkedin.com/in/judith-camhi/)
- [Julie Andlauer](https://www.linkedin.com/in/julie-andlauer/)
- [Aude Harnet](https://www.linkedin.com/in/aude-harnet-003828145/)
- [Anne-Flore Baron](https://www.linkedin.com/in/anne-flore-baron-00906a112/)
- [Jennifer Pariel](https://www.linkedin.com/in/jennifer-pariel/)
- [Amandine CASTRO](https://www.linkedin.com/in/amandine-castro-111463121/)

