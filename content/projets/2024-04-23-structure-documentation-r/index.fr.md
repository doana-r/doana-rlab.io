---
title: Comment est structurée la documentation de R
author: anna-doizy
date: '2024-04-23'
slug: structure-documentation-r
categories:
  - Tutoriel
tags:
  - Comprendre
subtitle: ''
summary: Chaque fonction (et même chaque objet en réalité) a une page d'aide associée. Cette documentation est hyper utile !
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


🍀 Chaque fonction (et même chaque objet en réalité) a une page d'aide associée. Tu peux y accéder en tapant `?nom_de_la_fonction` dans la console ou directement dans l'onglet **help**, si tu utilises l'interface RStudio.

📜 D'un accord commun des développeurs, ces pages ont (presque) toutes la même structure.  
Je la présente ici et je précise comment j'utilise chaque section en fonction de mes besoins.

👀 Cette documentation est hyper utile ! Je t'encourage à y faire un tour dès que tu as un doute.

---
Pour en savoir plus :
[Comment créer la documentation de tes fonctions maison](https://r-pkgs.org/man.html)

