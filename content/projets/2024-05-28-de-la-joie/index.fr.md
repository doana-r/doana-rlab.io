---
title: De la joie
author: anna-doizy
date: '2024-05-28'
slug: de-la-joie
categories:
  - Etude de cas
tags:
  - Sens
subtitle: ''
summary: Je te partage cette photo que j'ai faite il y a un mois parce qu'elle me rappelle ma joie de ce jour.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


👩‍💻 Je te partage cette photo que j'ai faite il y a un mois parce qu'elle me rappelle ma joie de ce jour.


- 1️⃣ Joie de retrouver ma touuuute première cliente (2020) pour un accompagnement ponctuel.

- 🤝 Joie que ce soit en présentiel, alors que les autres accompagnements du début de cette année se sont principalement réalisés en visio (j'aime que ce soit en équilibre).

- 💡 Joie de voir ma cliente évoluer, se poser les bonnes questions, être de plus en plus à l'aise avec R et les stats. Je suis fière !

- ⚙️ Joie de me casser la tête avec elle sur ses problématiques. Je suis impressionnée par sa patience et sa vision à long terme dans son travail.

- 🌱 Joie d'apprendre par mes clients, sur des sujets incroyablement variés et qui me tiennent à cœur : ananas 🍍, canne à sucre 🎍, abeilles 🐝, bactéries du sol 🦠, sportifs de haut niveau (et les moins sportifs) 🎽, les animaux de compagnie (et leurs maladies) 🐾, les relations hôtes-parasites chez les insectes 🪰, ...

***

Amélie Fevrier et tous.tes les autres... Merci !



