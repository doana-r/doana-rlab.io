---
title: Une collaboration joyeuse
author: anna-doizy
date: '2023-11-21'
slug: une-collaboration-joyeuse
categories:
  - Etude de cas
tags:
  - Comprendre
  - Transmettre
subtitle: ''
summary: J'ai adoré collaborer avec Antoine Drouillard. Ce que je retiens surtout, c'est que chaque avancée, chaque échange fut amical, enrichissant et enthousiasmant.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

☀️ J'ai adoré collaborer avec Antoine Drouillard.

🗨️ Pendant sa thèse, Antoine savait déjà très bien de quoi il parlait. Mon rôle n'a pas été de faire les analyses à sa place, plutôt d'amener un espace d'expression sain, où nous avons pu partager différentes idées, discuter les méthodes et les interprétations des résultats et approfondir les modèles et la manière de coder.

✅ La matière première d'Antoine, c'était hyper propre, hyper carré. Les rares incohérences (inévitables et/ou imprévues) étaient assumées et expliquées. J'ai pu avoir une entière confiance dans les données grâce à ça.

🌱 Ce que je retiens surtout, c'est que chaque avancée, chaque échange fut amical, enrichissant et enthousiasmant.

🎉 Merci pour tout ce travail. Je suis réjouie que cela soit reconnu au travers de cette publication.


![](post-linkedin.png)



- [références de la publication en question](/publication/drouillard_lechaudel_genard_doizy_grechi_2023/)
- [lien du poste sur LinkedIn](https://www.linkedin.com/feed/update/urn:li:activity:7132705441351917569/)

