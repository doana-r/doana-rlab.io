---
title: R en peinture
author: anna-doizy
date: '2024-02-28'
slug: r-en-peinture
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Je suis en week-end avec ma maman. Elle a déniché de la peinture. La seule inspiration que je trouve, c'est R.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


Je suis en week-end avec ma maman. Elle a déniché de la peinture.

🎨 En voyant la petite palette, je me sens revenir dans le passé.
J'étais enfant et elle proposait plein d'activités de bricolage pour mon frère et moi ! J'adorais ça.

📒🖌️🥛 On s'installe : du papier à dessin, des pinceaux échevelés trouvés dans un coin, un grand verre d'eau.

🎋 Immédiatement, ma maman s'absorbe dans une représentation de bambous à la japonaise. Très classe.

🤭 Et moi... Ben la seule inspiration que je trouve, c'est R.
Avec ses petits monstres gentils inventés par Allison Horst.

R hésite et les petits monstres l'encouragent :  
- Allons explorer !  
- Allez viens... !

💡 C'est quoi, toi, tes inspirations avec un pot de peinture devant toi ?  
💡 Comment tu ferais pour motiver R à faire ses analyses ?


