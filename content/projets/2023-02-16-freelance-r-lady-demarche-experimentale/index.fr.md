---
title: 'Freelance & R-lady : un duo gagnant pour la démarche expérimentale'
author: anna-doizy
date: '2023-02-16'
slug: freelance-r-lady-demarche-experimentale
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Jeudi 23 février 2023, je donne une conférence.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

Je donne une conférence.

🚪 Voici la porte de mon bureau quand je travaillais avec Frédéric Chiroleu en tant que chargée d'études statistiques au CIRAD.

La semaine prochaine j'y retourne pour faire une animation scientifique !  
Les animations scientifiques au sein de l'unité PVBMT (peuplements végétaux et bioagresseurs en milieu tropical https://umr-pvbmt.cirad.fr/) sont un format d'exposé hebdomadaire le jeudi entre 11h et 12h, pour présenter des projets de recherche et en discuter.

📊 Je parlerai de mon expérience en tant que freelance depuis presque 3 ans, de ce que j'apporte à la science par mon travail et des écueils que j'ai vécus pour apprendre à les éviter.  

> ➡️ Venez me voir jeudi 23 février à 11h, c'est public !

En présentiel au Pôle de Protection des Plantes ou via un lien de visio que je vous communiquerai sur demande (il n'est pas encore disponible).

Thomas Tatlian, merci pour les dessins 😍

