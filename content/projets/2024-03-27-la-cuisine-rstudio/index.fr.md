---
title: La cuisine RStudio
author: anna-doizy
date: '2024-03-27'
slug: la-cuisine-rstudio
categories:
  - Tutoriel
tags:
  - Automatiser
subtitle: ''
summary: ''
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🎲 J'ai créé un petit jeu en ligne !

👩‍💻 Quand je présente le logiciel RStudio à des personnes qui ne le connaissent pas encore (si, si ça existe !), j'aime beaucoup illustrer les différentes sous-fenêtres (il y en a beaucoup) avec une analogie culinaire 🍽️

Je ne veux rien divulgâcher maintenant, alors je te laisse tester par toi-même 👇


<div class="remix-app" hash="ee29d59e30f1b749">
    <script src="https://p.interacty.me/l.js" async></script>
</div>







