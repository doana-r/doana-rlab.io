---
title: Pause
author: anna-doizy
date: '2022-08-01'
slug: pause
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: J'ai pris des vacances !
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


23/06

Je suis bien, mais je ne veux rien produire.  
Juste admirer et me sentir vivante.   
C'est une pause. C'est une pose.

***


01/08

J'ai pris des vacances ! Me voilà de retour sur les réseaux. Je suis pleine d'idées à partager sur les stats, R et l'entrepreneuriat.  
À très vite 🙂
