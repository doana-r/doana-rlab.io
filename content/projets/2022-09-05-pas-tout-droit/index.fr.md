---
title: Pas tout droit
author: anna-doizy
date: '2022-09-05'
slug: pas-tout-droit
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Pour rester authentique, j'ai aussi envie de vous montrer des choses un peu moins jolies. Un peu moins utiles. Un peu plus personnelles.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


Je me sens vulnérable de vous partager ça.

Mais pour moi, ce qui est plus important que ma propre vulnérabilité, c'est l'image que je vous envoie de moi.
Pour rester authentique, j'ai aussi envie de vous montrer des choses un peu moins jolies. Un peu moins utiles. Un peu plus personnelles.

Le chemin d'une entrepreneuse n'est pas tout droit.

J'ai eu récemment une phase de démotivation, syndrome de l'imposteur, perte de cap.  
C'est difficile de continuer quand ça arrive.

J'ai l'immense chance de savoir déjà au fond de moi que cette phase est une phase, qu'elle ne représente pas la réalité de QUI je suis.

Et si vous ressentez des choses similaires, oui, promis, il y a des choses à faire pour que ça aille mieux.

J'ai décidé de poursuivre mon chemin avec d'autres personnes dans des cas semblables au mien. Je serais plus motivée d'avancer si, dans l'idée, j'ai un ami qui m'attend à la porte de ma maison pour aller "faire un footing" [remplacez par ce que vous voulez] tous les matins !

J'ai décidé d'ajuster le cap de mon projet DoAna, pour qu'il continue de ressembler à la personne que je suis et que je deviens un peu plus chaque jour.

Alors, voilà.  
Je fais partie désormais des surfeureuses et ça me comble de joie d'avancer avec ces personnes.

***

Si jamais vous voulez voir un peu l'état d'esprit où je suis en ce moment, voici un documentaire (très court) sur le programme SURF que j'ai démarré le mois dernier :

{{< youtube YMtx1HV_huM >}}

