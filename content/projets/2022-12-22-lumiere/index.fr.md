---
title: Lumière
author: anna-doizy
date: '2022-12-22'
slug: lumiere
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: Je suis fière de travailler avec des gens qui m'inspirent, avec qui je partage des valeurs similaires. C'est ce qui me nourrit le plus au quotidien. Ce qui me fait kiffer ma vie d'indépendante.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🥰 Je suis fière de travailler avec des gens qui m'inspirent, avec qui je partage des valeurs similaires.  
C'est ce qui me nourrit le plus au quotidien. Ce qui me fait kiffer ma vie d'indépendante.

💻 On peut croire que "faire des stats", c'est austère, c'est être recluse loin du monde, derrière son écran avec des lignes de code chelou comme dans Matrix.

Alors, oui ok pour les lignes chelou (en revanche, elles donnent des graphiques trop la classe, ça vaut le coup), mais pas ok pour l'austérité ! 😉

☀️ J'avance avec la lumière des gens que j'accompagne et avec qui je collabore. C'est mon trésor.

Merci mille fois de me faire grandir.

![](expo-raffinerie.jpg)

---

© Photos par Jef (Jean-François Hirsch), de La Raffinerie, résidence d'artiste 2022

