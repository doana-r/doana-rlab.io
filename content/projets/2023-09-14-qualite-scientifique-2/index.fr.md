---
title: Qualité scientifique - 2/4
author: anna-doizy
date: '2023-09-14'
slug: qualite-scientifique-2
categories:
  - Tutoriel
tags:
  - Comprendre
  - Concevoir
  - Sens
  - Transmettre
subtitle: ''
summary: Je me suis plantée. Ça m'arrive souvent ! Mais en fait, ça me va.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


## 🎯 La précision (accuracy, in english) 🎯


🙈 Je me suis plantée.

Ça m'arrive souvent !  
Mais en fait, ça me va. Pour peu que
- 1️⃣ je m'en sois rendu compte et
- 2️⃣ j'en retire un apprentissage pour la suite. J'aime dire que "l'échec, ça sert à pas rester débile" (c.f [Et tout le monde s'en fout](https://www.youtube.com/watch?v=c0IsvrSpcx4)) !


☀️ Pour qu'un projet de recherche ne se plante pas, le résultat des expériences, et donc les données récoltées, doivent, entre autres, être précis. C'est-à-dire correspondre au réel.


## Acquisition

📊 Voilà deux sources d'erreur concernant l'acquisition des données (le gros problème) :

🔊 le **bruit** : la variabilité des données peut parfois être contrôlée en fixant certaines variables dans le protocole. Quand il y a beaucoup de bruit, alors il faut récolter davantage de données pour gagner en précision.

🤏 le **biais** : c'est le décalage entre la réalité (inconnue) et les données. Il faut concevoir un protocole qui maximise la cohérence entre la question scientifique (ce que l'on attend) et les données récoltées pour parvenir à minimiser le biais.



## Interprétation

🧠 Voilà deux sources d'erreur concernant l'interprétation des résultats face aux données (le moins gros problème) :

🔊 le **bruit** : par exemple, le choix du modèle peut dépendre du chercheur en charge des analyses, de son champ de recherche, de son expérience, de ses compétences en statistiques, si c'est un bon jour pour lui, etc. Un modèle, en adéquation avec son protocole, prévu dès la conception du projet (via un plan d'analyses statistiques) aura plus de chance d'être précis !

🤏 le **biais** : par exemple, ce sont les défauts d'interprétation liés aux croyances a priori du chercheur en charge de l'interprétation des résultats et de la rédaction. On parle souvent des biais cognitifs, il y en a plein. Je peux citer le biais de confirmation, le biais d'ancrage, le biais d'autorité, l'escalade d'engagement... Contre toute attente, ce n'est pas si mal d'en avoir parfois, parce que ça compense un peu le biais généré par les données ! Mais autant s'en passer si possible. Première étape : en prendre conscience.



> 😊 Teasing : mon activité prend un tournant parce que je m'attaque maintenant au gros problème ! Ça fait longtemps que j'ai ça en tête et c'est en train de se concrétiser !

***

✨ Je m'arrête là, je te laisse (re)découvrir cette pépite de la chaîne Hygiène Mentale qui m'a grandement inspirée pour ce post.✨

{{< youtube VKsekCHBuHI >}}


***



<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-1">
    <i class="fas fa-plus"></i>
    Lire Qualité 1/4
  </a>

  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-3">
    <i class="fas fa-plus"></i>
    Lire Qualité 3/4
  </a>
  
  <a class="btn btn-outline-primary" href="/projets/qualite-scientifique-4">
    <i class="fas fa-plus"></i>
    Lire Qualité 4/4
  </a>
</div>
