---
title: Pivoter les données
summary: "Passage du format court au format long et inversement pour avoir des données *tidy*"
date: "2020-09-29"
lastmod: "2024-07-11"
slug: pivoter

tags:
  - Concevoir
  - Comprendre
categories:
  - Tutoriel
  
output: blogdown::html_page
---



<p>Le principe de <em>tidy data</em> (une ligne = une observation, une colonne = une variable, une cellule = une valeur) paraît tellement évident que l’on peut se demander comment un jeu de données peut ne pas être <em>tidy</em>.
Malheureusement, la plupart des données que vous allez rencontrer ne seront pas <em>tidy</em>. Et ce, pour deux raisons :</p>
<ol style="list-style-type: decimal">
<li>Beaucoup de gens ne connaissent pas le principe de <em>tidy data</em> et il n’est pas évident de le réinventer soi-même à moins de travailler beaucoup de temps avec des données.</li>
<li>Les données sont souvent organisées pour faciliter d’autres utilations que leur analyse. Par exemple, pour que leur saisie soit la plus simple possible sur le terrain.</li>
</ol>
<p>Cela signifie que, très souvent, il faudra ranger les données, en amont de l’analyse.
La première étape est toujours d’identifier les variables et les observations.
Parfois, c’est évident.
Parfois, pas du tout et il faudra se creuser la tête un peu plus.<br />
La seconde étape est de résoudre ces problèmes courants :</p>
<ul>
<li>Une variable qui est répartie sur plusieurs colonnes</li>
<li>Une observation qui est répartie sur plusieurs lignes</li>
</ul>
<p>En général, un jeu de données n’aura qu’un de ces deux problèmes. Il faudrait vraiment ne pas avoir de chance pour avoir les deux !
Heureusement, les fonctions <code>pivot_longer()</code> et <code>pivot_wider()</code> du package <strong>tidyr</strong> sont là !</p>
<div id="plus-long" class="section level2">
<h2>Plus long</h2>
<p>Parfois, les valeurs d’une variable se retrouvent dans le nom des colonnes.</p>
<p>Dans la table suivante, les colonnes qui s’appelent <code>1999</code> et <code>2000</code> sont en fait les valeurs d’une variable <code>annee</code> qui n’apparaît pas.
Les valeurs qui sont contenues dans les colonnes <code>1999</code> et <code>2000</code> représentent les valeurs d’une variable <code>nombre_de_cas</code>.
Ce qui fait que chaque ligne contient <strong>deux observations</strong> au lieu d’une seule.</p>
<pre class="r"><code>jeu_court &lt;- tribble(
  ~pays, ~`1999`, ~`2000`,
  &quot;Afghanistan&quot;, 745, 2666,
  &quot;Brésil&quot;, 37737, 80488,
  &quot;Chine&quot;, 212258, 213766,
)
jeu_court
## # A tibble: 3 × 3
##   pays        `1999` `2000`
##   &lt;chr&gt;        &lt;dbl&gt;  &lt;dbl&gt;
## 1 Afghanistan    745   2666
## 2 Brésil       37737  80488
## 3 Chine       212258 213766</code></pre>
<p>Pour passer un jeu de données comme ceci en format <em>plus long</em>, il faut <em>pivoter</em> les colonnes fautives dans une nouvelle paire de variables.</p>
<p><img src="pivot-longer.png" /></p>
<p>Trois paramètres sont nécessaires pour procéder à cette opération :</p>
<ul>
<li>L’ensemble des colonnes dont les noms sont des valeurs et pas des variables. (Ici : <code>1999</code> et <code>2000</code>)</li>
<li>Le nom de la variable qui contiendra le nom de ces dernières. (Ici : <code>annee</code>)</li>
<li>Le nom de la variable qui contiendra les valeurs. (Ici : <code>nombre_de_cas</code>)</li>
</ul>
<p>On appelle la fonction <code>pivot_longer()</code> de la manière suivante :</p>
<pre class="r"><code>pivot_longer(jeu_court, c(`1999`, `2000`), names_to = &quot;annee&quot;, values_to = &quot;nombre_de_cas&quot;)
## # A tibble: 6 × 3
##   pays        annee nombre_de_cas
##   &lt;chr&gt;       &lt;chr&gt;         &lt;dbl&gt;
## 1 Afghanistan 1999            745
## 2 Afghanistan 2000           2666
## 3 Brésil      1999          37737
## 4 Brésil      2000          80488
## 5 Chine       1999         212258
## 6 Chine       2000         213766</code></pre>
<p>Pour décrire les colonnes à pivoter, on utilise la notation de <code>dplyr::select()</code>.
Dans ce cas, les deux colonnes sont listées individuellement, mais il y a bien d’autres manières de sélectionner des colonnes (allez voir l’aide de <code>select()</code>).
Notons que <code>1999</code> et <code>2000</code> ne sont pas des noms syntaxiquement valide pour des noms de colonnes (car ne commencent pas par une lettre), il faut donc les entourer de <code>`</code> (accent grave).<br />
Les variables <code>annee</code> et <code>nombre_de_cas</code> n’existent pas dans le jeu de données <code>jeu_court</code>, leur nom doit donc être entouré de guillements <code>"</code>.<br />
Dans le résultat final, les colonnes pivotées sont supprimées et de nouvelles colonnes <code>annee</code> et <code>nombre_de_cas</code> sont apparues.
Les relations entre les autres variables sont préservées.</p>
<p><code>pivot_longer()</code> rend le tableau de données plus long en augmentant le nombre de lignes et en diminuant le nombre de colonnes. Le terme “format long” n’a pas trop de sens finalement, car c’est assez relatif. Il serait plus exact de dire que le tableau A est plus long que le tableau B.</p>
<p>Il existe tout un tas de variantes pour l’utilisation de cette fonction (par exemple, plusieurs variables sont contenues dans le nom des colonnes à pivoter), n’hésitez par à regarder les exemples dans l’aide de la fonction pour se faire une meilleure idée de toutes ses potentialités !</p>
</div>
<div id="plus-court" class="section level2">
<h2>Plus court</h2>
<p><code>pivot_wider()</code> fait l’inverse de <code>pivot_longer()</code>. On l’utilise quand une obervation est répartie sur plusieurs lignes (dans le but de faire une analyse multivariée par exemple).</p>
<p>Dans la table suivante, une observation est une combinaison de pays par année, mais chacune est représentée sur deux lignes.</p>
<pre class="r"><code>jeu_long &lt;- tibble(
  pays = rep(c(&quot;Afghanistan&quot;, &quot;Brésil&quot;, &quot;Chine&quot;), each = 4),
  annee = rep(1999:2000, times = 3, each = 2),
  type = rep(c(&quot;cas&quot;, &quot;population&quot;), times = 6),
  nombre = c(745, 19987071, 2666, 20595360, 37737, 172006362, 80488, 174504898, 212258, 1272915272, 213766, 1280428583)
  )
jeu_long
## # A tibble: 12 × 4
##    pays        annee type           nombre
##    &lt;chr&gt;       &lt;int&gt; &lt;chr&gt;           &lt;dbl&gt;
##  1 Afghanistan  1999 cas               745
##  2 Afghanistan  1999 population   19987071
##  3 Afghanistan  2000 cas              2666
##  4 Afghanistan  2000 population   20595360
##  5 Brésil       1999 cas             37737
##  6 Brésil       1999 population  172006362
##  7 Brésil       2000 cas             80488
##  8 Brésil       2000 population  174504898
##  9 Chine        1999 cas            212258
## 10 Chine        1999 population 1272915272
## 11 Chine        2000 cas            213766
## 12 Chine        2000 population 1280428583</code></pre>
<p>Pour passer un jeu de données comme ceci en format <em>plus court</em>, deux arguments soont nécessaires cette fois :</p>
<ul>
<li>Le nom de la colonne qui contient le nom des futures variables. (Ici : <code>type</code>)</li>
<li>Le nom de la colonne qui contient les valeurs. (Ici : <code>nombre</code>)</li>
</ul>
<p>Ceci étant trouvé, la fonction <code>pivot_wider()</code> s’utilise ainsi :</p>
<pre class="r"><code>pivot_wider(jeu_long, names_from = type, values_from = nombre)
## # A tibble: 6 × 4
##   pays        annee    cas population
##   &lt;chr&gt;       &lt;int&gt;  &lt;dbl&gt;      &lt;dbl&gt;
## 1 Afghanistan  1999    745   19987071
## 2 Afghanistan  2000   2666   20595360
## 3 Brésil       1999  37737  172006362
## 4 Brésil       2000  80488  174504898
## 5 Chine        1999 212258 1272915272
## 6 Chine        2000 213766 1280428583</code></pre>
<p>Et visuellement, cela donne :</p>
<p><img src="pivot-wider.png" /></p>
<p>Les deux fonctions <code>pivot_wider()</code> et <code>pivot_longer()</code> sont donc complémentaires.</p>
<p><code>pivot_longer()</code> rend les tables plus étroites et plus longues, tandis que <code>pivot_wider()</code> les rend plus courtes et larges.</p>
</div>
<div id="exercices" class="section level2">
<h2>Exercices</h2>
<ol style="list-style-type: decimal">
<li>Pourquoi <code>pivot_wider()</code> et <code>pivot_longer()</code> ne sont-elles pas parfaitement symétriques ?
Condidérez attentivement l’exemple suivant :</li>
</ol>
<pre class="r"><code>stocks &lt;- tibble(
  annee = c(2015, 2015, 2016, 2016),
  semestre = c(1, 2, 1, 2),
  resultat = c(1.88, 0.59, 0.92, 0.17)
)

stocks_pluscourt &lt;- pivot_wider(stocks, names_from = annee, values_from = resultat)

pivot_longer(stocks_pluscourt, `2015`:`2016`, names_to = &quot;annee&quot;, values_to = &quot;resultat&quot;)
## # A tibble: 4 × 3
##   semestre annee resultat
##      &lt;dbl&gt; &lt;chr&gt;    &lt;dbl&gt;
## 1        1 2015      1.88
## 2        1 2016      0.92
## 3        2 2015      0.59
## 4        2 2016      0.17</code></pre>
<p>(Indice : Regardez la nature de chaque variable)</p>
<ol start="2" style="list-style-type: decimal">
<li><p>Donne deux manières de modifier le type de la variable <code>annee</code>.</p></li>
<li><p>Quelle est l’erreur dans ce code ?</p></li>
</ol>
<pre class="r"><code>pivot_longer(jeu_court, c(1999, 2000), names_to = &quot;year&quot;, values_to = &quot;cases&quot;)
## Error in `pivot_longer()`:
## ! Can&#39;t select columns past the end.
## ℹ Locations 1999 and 2000 don&#39;t exist.
## ℹ There are only 3 columns.</code></pre>
<ol start="4" style="list-style-type: decimal">
<li>Que va-t-il se passer si vous raccourcissez cette table ? Pourquoi ? Comment pourriez-vous rajouter une colonne pour identifier chaque observation de manière unique ?</li>
</ol>
<pre class="r"><code>people &lt;- tribble(
  ~name,             ~names,  ~values,
  #-----------------|--------|------
  &quot;Phillip Woods&quot;,   &quot;age&quot;,       45,
  &quot;Phillip Woods&quot;,   &quot;height&quot;,   186,
  &quot;Phillip Woods&quot;,   &quot;age&quot;,       50,
  &quot;Jessica Cordero&quot;, &quot;age&quot;,       37,
  &quot;Jessica Cordero&quot;, &quot;height&quot;,   156
)</code></pre>
<ol start="5" style="list-style-type: decimal">
<li>Ranger cette petite table de façon à obtenir une ligne par sexe. Faut-il la rendre plus longue ou plus courte ? Quelles sont les variables ?</li>
</ol>
<pre class="r"><code>preg &lt;- tribble(
  ~pregnant, ~male, ~female,
  &quot;yes&quot;,     NA,    10,
  &quot;no&quot;,      20,    12
)</code></pre>
<details>
<summary>
Indice : Obtenez cette table
</summary>
<pre><code>## # A tibble: 2 × 3
##   sexe   nombre_personnes_enceintes nombre_personnes
##   &lt;chr&gt;                       &lt;dbl&gt;            &lt;dbl&gt;
## 1 male                           NA               20
## 2 female                         10               22</code></pre>
</details>
<hr />
<p>Traduction librement interprétée issue de <em>R for data science</em> de Hadley Wickham : <a href="https://r4ds.had.co.nz/tidy-data.html">r4ds.had.co.nz/tidy-data</a>.</p>
</div>
