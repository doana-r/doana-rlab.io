---
title: Créer une fonction et faire une boucle
author: anna-doizy
date: '2024-12-02'
slug: fonction-et-boucle
categories:
  - Tutoriel
tags:
  - Automatiser
subtitle: ''
summary: ''
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🔄 Je ne fais qua-si-ment plus jamais de boucles (`for` ou `apply`) dans R !  

Et ça, c'est grâce à une propriété très intéressante de R : les **fonctions vectorisées**.

🤨 C'est quoi une fonction vectorisée ?

C'est une fonction qui prend un (ou plusieurs) vecteur de longueur n en entrée et qui produit un vecteur de longueur n en sortie.

<br>

➕ Par exemple, le  `+` : je peux additionner 2 vecteurs de longueur 3, comme `1:3 + c(5,7,9)` et R me renvoie un vecteur de longueur 3.

En back-office, le `+` agit élément par élément, d'abord `1 + 5`, puis `2 + 7`, puis `3 + 9`.

C'est une **boucle** ! Mais ça ne se voit pas !

❔ Autre exemple : j'utilise très souvent `if_else()` qui est le pendant vectorisé de la structure conditionnelle `if(...){...}else{...}`

<br>

⏩ Les fonctions vectorisées sont optimisées pour que ça aille très vite (et là c'est du boulot de développeur informatique et ça dépasse mes compétences pour plus t'expliquer le pourquoi du comment)

Une fois, j'ai repris le code d'un ami/client qui était bourré de boucles `for()`.   
J'ai tout changé, utilisé un max de fonctions vectorisées et fait seulement 1 boucle avec `lapply()` à la fin.  
Verdict : le nouveau code était **20 fois plus rapide** ! 🤯

Tu peux aller voir la [cheat sheet de dplyr](https://rstudio.github.io/cheatsheets/data-transformation.pdf) ! La deuxième page donne une liste non exhaustive de fonctions vectorisées.


<br>

🧐 En général, quand j'ai envie de *faire une boucle*, je me demande si je peux passer en premier lieu par la vectorisation.

🤷‍♀️ Mais parfois, ce n'est pas possible.

Par exemple, pour importer et nettoyer plusieurs jeux de données (qui ont le même format) d'un coup. Parce que les fonctions d’import `read_xxx()` ne sont PAS vectorisées. Elles ne peuvent renvoyer qu’un seul jeu de données à la fois.

🗺️ Alors voilà un peu mon process interne quand je décide de créer une fonction pour automatiser quelque chose.


![](featured.png)

<br>

😜 Ça te parle ?
Tu utilises des boucles pour faire quoi, toi ?


