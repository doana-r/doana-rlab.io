---
date: "2021-03-05"
title: Tree-Grass
summary: De l'utilité d'un outil R-shiny pour illustrer l'analyse théorique d'un modèle minimaliste
subtitle: De l'utilité d'un outil R-shiny pour illustrer l'analyse théorique d'un modèle minimaliste
slug: treegrass

external_link: ""

image:
  caption: ""
  focal_point: Smart

links:
- name: "Code"
  url: https://gitlab.com/cirad-apps/tree-grass
- name: "Publication"
  url: /publication/djeumen2020usefulness/

tags:
  - Transmettre
categories:
  - App Shiny
  
---


J'ai commencé à travailler avec **Yves Dumont** (du CIRAD et de l'université de Prétoria) en 2019, à distance, je ne l'avais jamais rencontré.
Et nous voilà maintenant, le 4 mars 2021, côte à côte au cours d'une animation scientifique au CIRAD (Saint-Pierre) pour raconter aux chercheurs de la Réunion la genèse d'un article que nous avons co-écrit avec Valaire Yatat et Pierre Couteron.

![](Yves-Anna.png)

C'est l'histoire d'un **modèle minimaliste de la dynamique des végétations dans les biomes de savane** en Afrique. 

Les mathématiques, les modèles, les équations différentielles, c'est un peu *aride* pour les chercheurs en écologie, ça fait un peu peur.
Peut-être même que ça mord, ces bêtes-là !

![](tree-grass-slide.png)

Alors, mon rôle a été de créer un **outil interactif** pour explorer les résultats du modèle (en jouant avec ses paramètres). 
Cet outil est un package de R, il s'installe en une commande (`remotes::install_gitlab("cirad-apps/tree-grass@0.1.0")`) et se lance avec une autre (`treegrass::run_app()`).
Il est facile à utiliser, il ne nécessite pas de compétences en programmation, il ne montre pas d'équations atroces !

![](featured.png)

Cette chose colorée s'appelle un [diagramme de bifurcation](https://fr.wikipedia.org/wiki/Th%C3%A9orie_des_bifurcations).
Le modèle permet d'exlorer la stabilité des paysages de type savane (en langage mathématique, on dit *états stationnaires stables*) en fonction de tout un tas de paramètres ayant un sens biologique, en particulier la pluviométrie moyenne et la fréquence d'incendies de savanne/forêt.
Pour certaines valeurs de ces paramètres, plusieurs types de paysages coexistent, par exemple savane herbeuse/savane arborée, ou encore forêt/prairie. Et cela donne des paysages en mosaïque :

![](forest-savanna-mosaic.png)

La publication de ce modèle et de son outil d'exploration se trouve ici : https://doi.org/10.1016/j.ecolmodel.2020.109381.


