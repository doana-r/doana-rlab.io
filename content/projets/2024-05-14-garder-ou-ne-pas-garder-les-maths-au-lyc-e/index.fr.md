---
title: Garder ou ne pas garder les maths au lycée
author: anna-doizy
date: '2024-05-14'
slug: garder-ou-ne-pas-garder-les-maths-au-lyc-e
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: ''
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

🗨️ 💬 Je discutais avec une fille qui est en troisième.
Au lycée, elle voudrait prendre l'option biologie, mais elle ne pense pas choisir l'option maths.

🤷‍♀️ Depuis la réforme du lycée, c'est possible de choisir une matière scientifique (comme la physique-chimie ou la biologie) SANS faire de maths à côté.

😣 Une amie, prof de physique au lycée, justement, me dit combien elle galère, depuis cette réforme, avec ses élèves qui n'ont pas suffisamment de notions en maths pour pouvoir suivre ses cours. Elle doit donc s'adapter et... enseigner des maths pendant ses cours !

🌱 Et moi, je travaille principalement avec des biologistes, et je vois bien qu'il est indispensable dans ce domaine d'avoir les bases (au minimum) des statistiques pour pouvoir comprendre ce qu'on fait.
Si je ne comprends pas, même à un niveau instinctif, ce qu'il se passe derrière un modèle linéaire, par exemple, alors je ne pourrais pas :

➡️ bien choisir ma question de recherche  
➡️ avoir un protocole d'échantillonnage ou expérimental adapté  
➡️ écrire le modèle correctement  
➡️ vérifier les hypothèses sous-jacentes du modèle  
➡️ interpréter les paramètres estimés, les intervalles de confiances, la table d'ANOVA, ou toute autre sortie du modèle  
➡️ raconter dans un article ma méthodologie et expliquer mes choix d'analyses (il n'y a jamais UNE bonne manière de faire ses analyses)

Et, ne te méprends pas, je n'ai pas appris tout ça au lycée !  
⚙️ Mais c'est la logique derrière tout ça que j'y ai acquise, je pense. Cet état d'esprit qui me dit que, même si c'est dur, même si je fais des erreurs, j'ai quand même envie de continuer, d'essayer, de m'accrocher.

😨 Je n'ai pas trop su que dire à cette jeune fille qui voulait arrêter les maths tout en continuant la bio.
Je lui ai conseillé de conserver les maths, en lui expliquant qu'elles ont partout.

Le fera-t-elle ?

PS : il s'agissait d'un forum Filles & Maths 974, où j'ai présenté mon parcours à une centaine de filles entre 14 et 16 ans.   
J'en ai profité pour leur faire tester un jeu de cartes que j'ai créé pour ajouter une dimension ludique à ma formation d'initiation à R : l'objectif est de ranger les étapes d'une analyse de données dans l'ordre.   
Avec un petit coup de pouce, les filles de troisième y sont arrivées ! 🎲✨

![](atelier-filles-et-maths.jpg)



***

<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/les-femmes-dans-les-filieres-mathematiques/">
    <i class="fas fa-plus"></i>
    Les chiffres de la réforme
  </a>
</div>



