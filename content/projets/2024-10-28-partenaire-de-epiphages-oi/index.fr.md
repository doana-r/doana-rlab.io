---
title: Partenaire de EPIPHAGES-OI
author: anna-doizy
date: '2024-10-28'
slug: partenaire-de-epiphages-oi
categories:
  - Etude de cas
tags:
  - Concevoir
subtitle: ''
summary: ''
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


🎉 Pour la première fois, je suis officiellement partenaire d’un projet de recherche !

🕓 Parfois, j’interviens de façon ponctuelle comme consultante, mais souvent, mes collaborations de recherche durent plusieurs mois ou années.  
Cette fois, il ne s’agit pas d’une simple prestation : je participe pleinement à un projet porté par le CIRAD, dans le cadre du plan ECOPHYTO, financé par l’Organisme Français de Biodiversité.

➡️ Le projet s’appelle EPIPHAGES-OI pour “EPIdémiosurveillance étendue du complexe d'espèces Ralstonia solanacearum et de ses auxiliaires bactérioPHAGES dans l'Océan Indien” ⬅️

🍅 Peut-être connais-tu le flétrissement bactérien ? C’est une maladie des solanacées (tomates, pommes de terre…) causée par des bactéries du sol (Ralstonia solanacearum) qui infectent les plantes par les racines et les tuent.

🦠 Ce projet vise à explorer les phages, des virus ciblant ce complexe bactérien, à La Réunion et à Mayotte. Peut-être (un gros “peut-être” !) les phages pourraient-ils un jour devenir une solution de lutte biologique.

🧪 La première phase consiste à choisir les sites d’échantillonnage des sols, de l’eau et des plantes contaminées pour y repérer les phages potentiellement intéressants.

🤔 Choisir des échantillons sur le terrain implique de faire des compromis entre l’énergie, le temps, les ressources financières disponibles, les contraintes techniques (liées à l’environnement, au matériel de mesure, à l’accès aux lieux d’intérêt, etc.) ET les contraintes liées aux statistiques. 

C'est une étape critique : s’il n’y a pas assez de données, s’il y a trop de biais, ben… on ne pourra rien conclure. Et c’est vraiment dommage !

🤹‍♀️ Mes rôles pendant ces 3 ans :

- apporter mon regard “data” pour des analyses efficaces et reproductibles
- soutenir les chercheurs du projet dans leur programmation avec R
- créer un tableau de bord dynamique (avec shiny et/ou quarto) pour partager les résultats avec la communauté.

Les autres personnes (Fernando Clavijo-Coppens, Yann Pécrix et Adrien Rieux) vont piloter, organiser, aller sur le terrain, faire un tas d’analyse au laboratoire, faire beaucoup de traitement informatique de données génétiques récoltées, rédiger des rapports, manager… et j’oublie sûrement un tas de choses !

🫠 Parfois, je me dis que faire de la recherche, c’est exercer beaucoup de métiers à la fois… 
Un peu comme l’entrepreneuriat, en fait ! 😉



***

Pour en savoir (un peu) plus : https://ecophytopic.fr/recherche-innovation/prevenir/projet-epiphages-oi
