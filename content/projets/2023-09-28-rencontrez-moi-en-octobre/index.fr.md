---
title: Rencontrez-moi en octobre
author: anna-doizy
date: '2023-09-28'
slug: rencontrez-moi-en-octobre
categories:
  - Brève
tags:
  - Sens
subtitle: ''
summary: J'interviens le mardi 10 octobre aux Rencontres annuelles du Groupe de Recherche EcoStat et le 19 octobre à une conférence organisée par les R-Ladies Paris.
authors: []
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---

J'interviens le mardi 10 octobre aux [Rencontres annuelles du Groupe de Recherche EcoStat](https://gdrecostat2023.sciencesconf.org/) et le 19 octobre à une conférence organisée par les [R-Ladies Paris](https://www.linkedin.com/in/r-ladies-paris/).

🎲 Dans la première, je vais présenter la **formation que je suis en train de créer** et qui tourne en boucle dans ma tête depuis plusieurs mois.

☀️ Dans la seconde, je parlerai de **ma vie de freelance avec R pour compagnon**. Je l'ai déjà donnée au [CIRAD](https://www.cirad.fr/) à Saint-Pierre en février 2023, cette fois ce sera derrière mon écran. 😉

![](prendre-soin-du-début-de-l'aventure.png)

![](freelance-et-rlady.png)

***

- Les inscriptions sont déjà terminées, j’ai trop tardé à communiquer, mais il y aura un replay libre d'accès !
 🙊
 
- Pour le 19 octobre :
    - [événement meetup](https://www.meetup.com/fr-FR/rladies-paris/events/296623138/)
    - [inscription via zoom](https://us06web.zoom.us/meeting/register/tZwscuCppjwrHNMCmyEXSxBF6rqDJ2Wn8z_Z#/registration)️

***

<div class="btn-links">
  <a class="btn btn-outline-primary" href="/projets/freelance-r-lady-demarche-experimentale/">
    <i class="fas fa-plus"></i>
    Mon post sur la conférence de février
  </a>
  <a class="btn btn-outline-primary" href="/projets/prendre-soin-du-debut-de-l-aventure/">
    <i class="fas fa-plus"></i>
    Présentation de la formation sur les protocoles
  </a>
  <a class="btn btn-outline-primary" href="/projets/conferences-enregistrees/">
    <i class="fas fa-plus"></i>
    Les replays !
  </a>
</div>
