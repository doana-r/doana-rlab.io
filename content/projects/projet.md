+++
# A Projects section created with the Portfolio widget.
widget = "portfolio"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = false  # This file represents a page section.
active = true  # Activate this widget? true/false

title = "Projets & pensées"
subtitle = "La production de connaissance scientifique s'axe en quatres étapes : <br>  <br>🗒️ **Concevoir** des protocoles de récolte de données efficaces, compréhensibles et beaux. <br>💡 **Comprendre** et prendre les bonnes décisions pour une analyse de données cohérente avec la question de recherche et le protocole. <br>🗝 **Transmettre** en transformant des données et les analyses en histoires à impact. <br>⚙️ **Automatiser** et optimiser des processus répétitifs en organisant mieux la gestion des données.<br>  <br>Je partage ici mon évolution, mes projets et ce qui fait sens pour moi dans mon activité. <br> <div class='btn btn-outline-primary ml-onclick-form' onclick=\"ml('show', 'IT7bou', true)\">S'abonner à ma newsletter et rester en contact</div> <br> <br>"

[content]
  # Page type to display. E.g. project.
  page_type = "projets"
  
  # Filter toolbar (optional).
  # Add or remove as many filters (`[[content.filter_button]]` instances) as you like.
  # To show all items, set `tag` to "*".
  # To filter by a specific tag, set `tag` to an existing tag name.
  # To remove toolbar, delete/comment all instances of `[[content.filter_button]]` below.
  
  # Default filter index (e.g. 0 corresponds to the first `[[filter_button]]` instance below).
  filter_default = 0
  
  [[content.filter_button]]
    name = "Tout"
    tag = "*"
  
  [[content.filter_button]]
    name = "1/ Concevoir"
    tag = "Concevoir"
  
  [[content.filter_button]]
    name = "2/ Comprendre"
    tag = "Comprendre"
    
  [[content.filter_button]]
    name = "3/ Transmettre"
    tag = "Transmettre"
    
  [[content.filter_button]]
    name = "4/ Automatiser"
    tag = "Automatiser"
    
  [[content.filter_button]]
    name = "Sens et entrepreneuriat"
    tag = "Sens"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

  # Toggle between the various page layout types.
  #   1 = List
  #   2 = Compact
  #   3 = Card
  #   5 = Showcase
  view = 3

  # For Showcase view, flip alternate rows?
  flip_alt_rows = true

+++

