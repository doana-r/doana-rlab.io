---
title: "Conseil"
subtitle: "Tu désires comprendre les entrailles de <i class='fab fa-r-project'></i> ? Tu voudrais enfin savoir quand et comment utiliser les *t-tests* et les modèles mixtes ? Que tu sois débutant ou déjà expérimenté, fais-toi accompagner pour l'ensemble ou une partie de ton projet de recherche, depuis la création du protocole de collecte des données jusqu'à la communication des résultats à tes pairs, tes partenaires et/ou au public."
widget: "blank"
headless: true
active: true
weight: 40
design:
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns: "2"
  spacing:
    # Customize the section spacing. Order is top, right, bottom, left.
    #padding: ["0px", "0px", "0px", "0px"]
---

![](/img/cheminement.png)


