+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 20 # Order that this section will appear.

title = ""
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Background image.
  image = "DoAna-Reunion.jpg"  # Name of image in `static/img/`.
#   image_darken = 0  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
#   image_size = "contain"  #  Options are `cover` (default), `contain`, or `actual` size.
#   image_position = "center"  # Options include `left`, `center` (default), or `right`.
  image_parallax = false  # Use a fun parallax-like fixed background effect? true/false

+++

