---
title: "Formation"
subtitle: "Forme-toi avec ton équipe à utiliser R, à acquérir des méthodes de travail reproductibles, à prendre soin du début de vos projets de recherche. J'utilise une pédagogie active pour encourager la coopération d'équipe en partant de votre propre expérience."
widget: "blank"
headless: true
active: true
weight: 60
design:
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns: "2"
  spacing:
    # Customize the section spacing. Order is top, right, bottom, left.
    #padding: ["0px", "0px", "0px", "0px"]
---

<div class="formation">

### Programmes existants

- Initiation à la programmation avec `R` et `RStudio` 
  <div class="btn-links">
      <a class="btn btn-outline-primary" href="files/InitiationR.png">
      <i class = "fas fa-bookmark"></i>
      Programme
      </a>
      <a class="btn btn-outline-primary" href="https://initiation.doana-r.com/">
      <i class = "fas fa-book"></i>
      Supports
      </a>
  </div>
- Reproductibilité scientifique avec `rmarkdown`  
  <div class="btn-links">
      <a class="btn btn-outline-primary" href="files/Rmarkdown.png">
      <i class = "fas fa-bookmark"></i>
      Programme
      </a>
      <a class="btn btn-outline-primary" href="https://rmarkdown.doana-r.com/">
      <i class = "fas fa-book"></i>
      Supports
      </a>
  </div>

### En cours de conception

- Prendre soin du début de ton aventure de recherche (protocole expérimental, plan de gestion de données, plan d'analyses statistiques, ...)
- Les modèles mixtes
- Passer de base R au tidyverse
- Les outils de la reproductibilité


### Ateliers collectifs thématiques

- Bien lire les graphiques

  > N'hésite pas à me proposer des thèmes que tu aimerais voir aborder !

<br>

*Prise en charge possible. Me contacter.*

</div>

