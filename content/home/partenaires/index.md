---
title: "Partenaires"
subtitle: "Voici les organismes et collectifs avec qui [DoAna](/about) a ou a eu le plaisir de travailler."

widget: "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless: true  # This file represents a page section.
active: true  # Activate this widget? true/false
weight: 65  # Order that this section will appear.

gallery_item:
- album: gallery
  image: logo-cbnm.jpg
  url: https://www.cbnm.org/
- album: gallery
  image: logo-cirad.jpg
  url: https://www.cirad.fr/
- album: gallery
  image: logo-cyroi.jpg
  url: https://cyroi.re/
- album: gallery
  image: logo-ercane.png
  url: https://www.ercane.re/
- album: gallery
  image: logo-gds.jpg
  url: https://www.gds974.com/
- album: gallery
  image: logo-inrae.png
  url: https://www.inrae.fr/
- album: gallery
  image: logo-ocea.png
  url: https://www.ocea.re
- album: gallery
  image: logo-raffinerie.jpg
  url: https://www.laraffinerie.re
- album: gallery
  image: logo-univ-reunion.png
  url: https://www.univ-reunion.fr/
- album: gallery
  image: logo-ecostat.jpg
  url: https://sites.google.com/site/gdrecostat/accueil

---

{{< gallery >}}

<!--
Libranou : manque logo
-->
