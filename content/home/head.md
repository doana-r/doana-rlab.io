---
title: ""
widget: "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless: true  # This file represents a page section.
active: true  # Activate this widget? true/false
weight: 10  # Order that this section will appear.
share: true
design:
  background:
    # Background color.
    color: "#003f5d"
    # Text color (true=light or false=dark).
    text_color_light: true
  spacing:
    # Customize the section spacing. Order is top, right, bottom, left.
    padding: ["0", "0", "10px", "0"]
  columns: "1"
---


<img src="/img/do-analyz-in-r.gif" alt="Anna Doizy R - Do Analyz in Reunion" style="width: 50%;margin: auto;">


<!-- Cal floating-popup embed code begins -->
<script type="text/javascript">
(function (C, A, L) { let p = function (a, ar) { a.q.push(ar); }; let d = C.document; C.Cal = C.Cal || function () { let cal = C.Cal; let ar = arguments; if (!cal.loaded) { cal.ns = {}; cal.q = cal.q || []; d.head.appendChild(d.createElement("script")).src = A; cal.loaded = true; } if (ar[0] === L) { const api = function () { p(api, arguments); }; const namespace = ar[1]; api.q = api.q || []; typeof namespace === "string" ? (cal.ns[namespace] = api) && p(api, ar) : p(cal, ar); return; } p(cal, ar); }; })(window, "https://app.cal.com/embed/embed.js", "init");
Cal("init", {origin:"https://app.cal.com"});

Cal("floatingButton", {"calLink":"doana-r/viens-on-fait-du-troc","config":{"layout":"month_view"},"buttonText":"On fait du troc ?","buttonPosition":"bottom-left","buttonColor":"#003f5d","buttonTextColor":"#effff7"});
Cal("ui", {"styles":{"branding":{"brandColor":"#000000"}},"hideEventTypeDetails":false,"layout":"month_view"});
</script>
<!-- Cal floating-popup embed code ends -->
