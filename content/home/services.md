---
widget: featurette
headless: true
active: true
weight: 30
title: Services
subtitle: >-
  [DoAna - Statistiques Réunion](/) est une entreprise indépendante basée à la
  Réunion. Elle propose du conseil et des formations en **méthodologie scientifique** et en **analyses de
  données** (expérimentales, observationnelles, simulées...). <br> <br> 
  La **validité scientifique**, la **reproductibilité** et la **transmission** sont ses maîtres-mots.
feature:
  - icon: clipboard-check
    icon_pack: fas
    name: Conseil
    description: >-
      Fais-toi accompagner dans la conception, la production et l'automatisation de tes analyses de données et veille à la **validité** de ta méthode scientifique.
      <br>
      <div class="btn-links">
      <a class="btn btn-outline-primary" href="#conseil">
      <i class = "fas fa-chevron-circle-down"></i>
      Je veux devenir autonome
      </a>
      </div>
  - icon: handshake
    icon_pack: fas
    name: Collaboration de recherche
    description: >-
      **Associons-nous** sur un projet de recherche, à partir de sa conception, et mettons en commun nos compétences complémentaires.
      <br>
      <div class="btn-links">
      <a class="btn btn-outline-primary" href="#collaboration">
      <i class = "fas fa-chevron-circle-down"></i>
      Je veux collaborer
      </a>
      </div>
  - icon: graduation-cap
    icon_pack: fas
    name: |
      Formations
    description: >-
      (Re)découvre avec ton équipe le langage de programmation [<i class="fab fa-r-project"></i>](https://www.r-project.org/) et le bonheur simple de **prendre soin d'un projet de recherche dès sa conception**.
      <br>
      <div class="btn-links">
      <a class="btn btn-outline-primary" href="#formation">
      <i class = "fas fa-chevron-circle-down"></i>
      Je veux apprendre
      </a>
      </div>
---
