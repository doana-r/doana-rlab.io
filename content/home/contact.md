---
# Contact widget.
widget: "contact"  # See https://sourcethemes.com/academic/docs/page-builder/
headless: true  # This file represents a page section.
active: true  # Activate this widget? true/false
weight: 70  # Order that this section will appear.

title: "Contact"
subtitle: "Voulez-vous en savoir plus, vous tenir au courant ou demander un rendez-vous ? <br> <br> <a class='btn btn-outline-primary' href='/projects/'>Voir mes projets</a>"

# Automatically link email and phone?
autolink: true

# Email form provider
#   0: Disable email form
#   1: Netlify (requires that the site is hosted by Netlify)
#   2: formspree.io
email_form: 0

design:
  spacing:
    padding: ["110px", "0", "28em", "0"]
    
# leaflet : view-source:https://leafletjs.com/examples/quick-start/example-popups.html
# stamen : view-source:http://maps.stamen.com/test/leaflet.html
# miniglobe : https://github.com/chriswhong/leaflet-globeminimap

# <link rel="stylesheet" href="https://unpkg.com/leaflet@latest/dist/leaflet.css" />
# <script src="https://unpkg.com/leaflet@latest/dist/leaflet.js"></script>
---

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
  integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
  crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
  integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
  crossorigin=""></script>
  

<script type="text/javascript" src="https://stamen-maps.a.ssl.fastly.net/js/tile.stamen.js"></script>



<div id="mapFoulard" class="map"></div>

<script type="text/javascript">

  var map = new L.Map("mapFoulard", {
    center: new L.LatLng(-21.1, 55.52),
    zoom: 9, 
    maxZoom: 12,
    tileSize: 512
    });
    
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors' }).addTo(map);

	L.marker([-21.22, 55.32]).addTo(map);
	
  setTimeout(() => {
    map.invalidateSize()
    }, 100)
  
</script>

